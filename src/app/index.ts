import { App } from './App';
import { AppData } from './core/appData/AppData';
import { Authentication } from './core/authentication/Authentication';
import { Comms } from './core/comms/Comms';
import { Database } from './core/database/Database';
import { Device } from './core/device/Device';
import { F7 } from './core/framework/F7';
import { Framework7PageEvents } from './core/framework/Framework7PageEvents';
import { Listeners } from './core/listener/Listeners';
import { Logger } from './core/logger/Logger';
import { HandlebarUtils } from './core/utils/HandlebarUtils';

	// Start with logger so we get messags
	Logger.init();

	// Listeners next so we can link events
	Listeners.init();

	// Framework init for Dom functions and router
	F7.init();

	// Start the device / cordova stuff
	Device.init();

	// Initialise data
	Database.init();

	// Initialise comms
	Comms.init();

	// Initialise data and caches
	AppData.init();

	// UI Manipulation
	HandlebarUtils.registerHelpers();

	// Start app and page controls
	App.init();

	// Bind events to page controllers
	Framework7PageEvents.init();

	// Start the user login
	Authentication.init();




