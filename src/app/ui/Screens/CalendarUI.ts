import { Dom7Instance } from 'dom7';
import Handlebars from 'handlebars';
import _ from 'lodash';
import moment from 'moment';

import { AppData } from '../../core/appData/AppData';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';
import { Logger } from '../../core/logger/Logger';
import { IReminder } from '../../reminder/reminder/IReminder';
import { ReminderDisplay } from '../../reminder/reminder/ReminderDisplay';
import { BasicScreenInterface } from './BasicScreenInterface';

/**
 * The app calendar UI controller class
 * @class
 * @implements {BasicScreenInterface}
 */

export class CalendarUI implements BasicScreenInterface {

	/** @var {Dom7Instance} backButton Reference to the back button element */
	private backButton : Dom7Instance;

	/** @var {HandlebarsTemplateDelegate<any>} parsedTemplate Reference to the list template */
	private parsedTemplate : HandlebarsTemplateDelegate<any>;

	/**
	 * @constructor
	 */
	constructor() {
		this.parseTemplate();
	}

	/**
	 * Initialise the page
	 * @method onPageInit
	 * @public
	 */
	public onPageInit = (): void => {
		this.registerDomElements();
		this.initPageDates();
		this.initUi();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageIn
	 * @public
	 */
	public onBeforePageIn = (): void => {
		Listeners.deviceListener.deviceBack.subscribe(this.onBack);
	}


	/**
	 * Handle any tasks before the page transitions in
	 * @method onAfterPageIn
	 * @public
	 */
	public onAfterPageIn = (): void => {
		//
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageOut
	 * @public
	 */
	public onBeforePageOut = (): void => {
		Listeners.deviceListener.deviceBack.unsubscribe(this.onBack);

	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onPageReinit
	 * @public
	 */
	public onPageReinit = (): void => {
		//
	}

	/**
	 * Initialise the dom element references
	 * @method registerDomElements
	 * @private
	 */
	private registerDomElements = (): void => {
		this.backButton = F7.framework7App.dom('#calendar-back-link');
	}

	/**
	 * Initialise the UI components.
	 * @method initUi
	 * @private
	 */
	private initUi = (): void => {
		this.backButton.off('click', undefined).on('click', this.onBack);
		this.initTemplate();
		this.refreshCalendarList();
	}

	/**
	 * Initialise the local template then parse if for handlebars
	 * @method parseTemplate
	 * @private
	 * @returns {Promise<string>} Returns a promise containing the template.
	 */
	public parseTemplate = async (): Promise<void> => {
		try {
			const template = await this.initTemplate();
			this.parsedTemplate = Handlebars.compile(template);
		}
		catch (error) {
			Logger.error('CalendarUi.initPage : Error loading template %s', error.message);
		}
	}

	/**
	 * Initialise the calendar item template for parsing
	 * @method initTemplate
	 * @private
	 * @async
	 * @returns {Promise<string>} Returns a promise containing the template.
	 */
	private initTemplate = (): Promise<string> => {
		return new Promise((resolve, reject): any => {
			return F7.framework7App.app.request.get('pages/templates/calendarListItem.html', (templateData: string) => {
				resolve(templateData);
			}),
			((error: Error) => {
				reject(error.message);
			});
		});
	}

	/**
	 * Initialise the dates on the calendar page.
	 * @method initPageDates
	 * @private
	 */
	private initPageDates = (): void => {
		for (let month = 1; month <= 12; month ++) {
			const html = F7.framework7App.dom('#reminder-calendar-month-title-' + month).html() + ' ' + moment().format('YYYY');
			F7.framework7App.dom('#reminder-calendar-month-title-' + month).html(html);
		}
	}

	/**
	 * Refresh the reminder list on screen.
	 * @method refreshCalendarList
	 * @private
	 * @async
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private refreshCalendarList = (): Promise<void> => {
		let reminderTotal = 0;
		F7.framework7App.dom('.reminder-counter-bubble').html('0');
		F7.framework7App.dom('.reminder-counter-bubble').removeClass('with-reminder');
		F7.framework7App.dom('.reminder-content-calendar-month').html('');
		const reminders: IReminder[] = _.sortBy(AppData.reminders.getActiveReminders(), ['reminder_date'], ['asc']);
		for (const reminder of reminders) {
			if (reminder) {
				const displayMonth = moment(reminder.reminder_date).format('M');
				const currentCalCount = parseInt(F7.framework7App.dom('#reminder-bubble-calendar-' + displayMonth).html()) + 1;
				reminderTotal++;
				F7.framework7App.dom('#reminder-bubble-calendar-' + displayMonth).addClass('with-reminder');
				F7.framework7App.dom('#reminder-bubble-calendar-' + displayMonth).html(currentCalCount.toString());
				const reminderDisplay: ReminderDisplay = new ReminderDisplay(reminder);
				F7.framework7App.dom('#reminder-calendar-' + displayMonth).append(this.parsedTemplate(reminderDisplay));
				F7.framework7App.dom('#reminder-calendar-month-title-' + displayMonth).html(reminderDisplay.month + ' ' + reminderDisplay.year);
				F7.framework7App.dom('#reminder-calendar-month-title-' + displayMonth).addClass('text-color-primary');
			}
		}
		if (reminderTotal > 0) {
			F7.framework7App.dom('#reminder-total').html('<span class="badge color-red">' + reminderTotal + '</span>');
		} else {
			F7.framework7App.dom('#reminder-total').html('');
		}
		return;
	}

	/**
	 * Handle the back button event
	 * @method onBack
	 * @private
	 */
	private onBack = (): Promise<void> => {
		F7.framework7App.app.views.main.router.back('/', {animate: true});
		return;
	}
}
