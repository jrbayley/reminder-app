import { Dom7Instance } from 'dom7';
import Handlebars from 'handlebars';

import { IMqttMessage } from '../../core/comms/mqtt/interfaces/IMqttMessage';
import { IMqttPayload } from '../../core/comms/mqtt/interfaces/IMqttPayload';
import { IMqttUserAndHost } from '../../core/comms/mqtt/interfaces/IMqttUserAndHost';
import { MqttMessageType } from '../../core/comms/mqtt/interfaces/MqttMessageType';
import { MqttMessageUtils } from '../../core/comms/mqtt/MqttMessageUtils';
import { MqttPayloadUtils } from '../../core/comms/mqtt/MqttPayloadUtils';
import { Configuration } from '../../core/config/Configuration';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';
import { Logger } from '../../core/logger/Logger';
import { BasicScreenInterface } from './BasicScreenInterface';

/**
 * The app online users UI controller class
 * @class
 * @implements {BasicScreenInterface}
 */

export class ChatWithUI implements BasicScreenInterface {

	/** @var {Dom7Instance} backButton Reference to the back button element */
	private backButton : Dom7Instance;

	/** @var {Dom7Instance} messageToSend Reference to the send message element */
	private messageToSend : Dom7Instance;

	/** @var {Dom7Instance} userToChatWith Reference to the user display span element */
	private userToChatWith : Dom7Instance;

	/** @var {Dom7Instance} messageDisplayWindow Reference to the message display div element */
	private messageDisplayWindow : Dom7Instance;


	/** @var {Dom7Instance} sendButton Reference to the send button element */
	private sendButton : Dom7Instance;

	/** @var {HandlebarsTemplateDelegate<any>} sendMessageTemplate Reference to the send message template */
	private sendMessageTemplate : HandlebarsTemplateDelegate<any>;

	/** @var {HandlebarsTemplateDelegate<any>} receivedMessageTemplate Reference to the send message template */
	private receivedMessageTemplate : HandlebarsTemplateDelegate<any>;

	/** @var {IMqttUserAndHost} recipient The user and host to message */
	private recipient : IMqttUserAndHost;

	/**
	 * @constructor
	 */
	constructor() {
		Listeners.deviceListener.deviceBack.subscribe(this.onBack);
		this.parseTemplate();
	}

	/**
	 * Initialise the reminder page class
	 * @method onPageInitWithItem
	 * @public
	 * @param {string} userAndHost The userAndHost to message to.
	 */
	public onPageInitWithItem = (userAndHost: string): void => {
		Logger.debug('ChatWithUI: Chat with user %s', userAndHost);
		this.recipient = MqttMessageUtils.separateUserAndHost(userAndHost);
		this.registerDomElements();
		this.initFormControls();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageIn
	 * @public
	 */
	public onBeforePageIn = (): void => {
		Listeners.deviceListener.deviceBack.subscribe(this.onBack);
		Listeners.mqttListener.mqttMessageReceived.subscribe(this.onMessageReceived);
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onAfterPageIn
	 * @public
	 */
	public onAfterPageIn = (): void => {
		Listeners.mqttListener.mqttPresenceSend.publish();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageOut
	 * @public
	 */
	public onBeforePageOut = (): void => {
		Listeners.deviceListener.deviceBack.unsubscribe(this.onBack);
		Listeners.mqttListener.mqttMessageReceived.unsubscribe(this.onMessageReceived);
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onPageReinit
	 * @public
	 */
	public onPageReinit = (): void => {
		//
	}

	/**
	 * Handle any form initialisation
	 * @method initFormControls
	 * @public
	 */
	public initFormControls = (): void => {
		this.initUi();
	}

	/**
	 * Initialise the dom element references
	 * @method registerDomElements
	 * @private
	 */
	private registerDomElements = (): void => {
		this.backButton = F7.framework7App.dom('#messages-back-link');
		this.userToChatWith = F7.framework7App.dom('#userToChatWith');
		this.messageToSend = F7.framework7App.dom('#messageToSend');
		this.sendButton = F7.framework7App.dom('#sendMessageButton');
		this.messageDisplayWindow = F7.framework7App.dom('#messageDisplayWindow');
	}

	/**
	 * Initialise the UI components.
	 * @method initUi
	 * @private
	 */
	private initUi = (): void => {
		this.userToChatWith.html(this.recipient.user);
		this.backButton.off('click', undefined).on('click', this.onBack);
		this.sendButton.off('click', undefined).on('click', this.sendMessage);
		Listeners.mqttListener.mqttPresenceReceived.publish(undefined);
	}

	/**
	 * Initialise the local template then parse if for handlebars
	 * @method parseTemplate
	 * @private
	 * @returns {Promise<string>} Returns a promise containing the template.
	 */
	private parseTemplate = async (): Promise<void> => {
		try {
			const sentMessageTemplate = await this.initSendMessageTemplate();
			this.sendMessageTemplate = Handlebars.compile(sentMessageTemplate);
			const receivedMessageTemplate = await this.initReceivedMessageTemplate();
			this.receivedMessageTemplate = Handlebars.compile(receivedMessageTemplate);
		}
		catch (error) {
			Logger.error('MessagesUi.initPage : Error loading template %s', error.message);
		}
	}

	private initSendMessageTemplate = (): Promise<string> => {
		return this.initTemplate('sentMessageTemplate');
	}
	private initReceivedMessageTemplate = (): Promise<string> => {
		return this.initTemplate('receivedMessageTemplate');
	}
	/**
	 * Initialise the reminder item template for parsing
	 * @method initTemplate
	 * @private
	 * @async
	 * @returns {Promise<string>} Returns a promise containing the template.
	 */
	private initTemplate = (templateName: string): Promise<string> => {
		return new Promise((resolve, reject): any => {
			return F7.framework7App.app.request.get('pages/templates/' + templateName + '.html', (templateData: string) => {
				resolve(templateData);
			}),
			((error: Error) => {
				reject(error.message);
			});
		});
	}

	/**
	 * Refresh the current message list on screen.
	 * @method onMessageReceived
	 * @private
	 * @async
	 * @param {IMqttMessage} presences The message from the event listener
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onMessageReceived = (message: IMqttMessage): Promise<void> => {
		Logger.silly('MessagesUI.onMessageReceived : Message : %o', message);
		if (message.mqttPayload.messageType === MqttMessageType.MESSAGE) {
			F7.framework7App.dom('#messages').append(message.mqttPayload.body.data);
		}
		return;
	}

	/**
	 * Handle the back button event
	 * @method
	 * @private
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onBack = (): Promise<void> => {
		F7.framework7App.app.views.main.router.back('/', {animate: true});
		return;
	}

	/**
	 * Handle the sent message display
	 * @method onMessageSent
	 * @private
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onMessageSent = (message: IMqttMessage): Promise<void> => {
		let html = this.messageDisplayWindow.html();
		if (message) {
			html += this.sendMessageTemplate(message);
			this.messageDisplayWindow.html(html);
		}
		return;
	}

	private sendMessage = (): void => {
		console.log(this.messageToSend.val());
		console.log('send');
		console.log('to ' + this.recipient);

		const payload: IMqttPayload = MqttPayloadUtils.createPayload(MqttMessageType.MESSAGE, this.recipient.host, {data: this.messageToSend.val()});
		const message: IMqttMessage = {topic: Configuration.MQTT_NODE_PATH + this.recipient.host, mqttPayload: payload};
		console.log('sending %o', message);
		this.onMessageSent(message);
		Listeners.mqttListener.mqttMessageSend.publish(message);
	}
}
