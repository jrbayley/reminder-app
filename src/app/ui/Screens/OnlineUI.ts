import { Dom7Instance } from 'dom7';
import Handlebars from 'handlebars';

import { IMqttStatusUpdate } from '../../core/comms/mqtt/interfaces/IMqttStatusUpdate';
import { MqttPresence } from '../../core/comms/mqtt/interfaces/MqttPresence';
import { MqttStatus } from '../../core/comms/mqtt/interfaces/MqttStatus';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';
import { Logger } from '../../core/logger/Logger';
import { BasicScreenInterface } from './BasicScreenInterface';

/**
 * The app online users UI controller class
 * @class
 * @implements {BasicScreenInterface}
 */

export class OnlineUI implements BasicScreenInterface {

	/** @var {Dom7Instance} backButton Reference to the back button element */
	private backButton : Dom7Instance;

	/** @var {HandlebarsTemplateDelegate<any>} parsedTemplate Reference to the list template */
	private parsedTemplate : HandlebarsTemplateDelegate<any>;

	/** @var {IMqttStatusUpdate} mqttStatusUpdate The last status update  */
	private mqttStatusUpdate : IMqttStatusUpdate;

	/**
	 * @constructor
	 */
	constructor() {
		Listeners.deviceListener.deviceBack.subscribe(this.onBack);
		Listeners.mqttListener.mqttStatus.subscribe(this.onStatusChange);
		this.parseTemplate();
	}

	/**
	 * Initialise the page
	 * @method onPageInit
	 * @public
	 */
	public onPageInit = (): void => {
		this.registerDomElements();
		this.initUi();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageIn
	 * @public
	 */
	public onBeforePageIn = (): void => {
		Listeners.deviceListener.deviceBack.subscribe(this.onBack);
	}

	/**
	 * Handle transition to online
	 * @method onOnline
	 * @private
	 */
	private onOnline = (): void => {
		Listeners.mqttListener.presenceListUpdated.subscribe(this.onPresencesUpdated);
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onAfterPageIn
	 * @public
	 */
	public onAfterPageIn = (): void => {
		Listeners.mqttListener.mqttPresenceSend.publish();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageOut
	 * @public
	 */
	public onBeforePageOut = (): void => {
		Listeners.deviceListener.deviceBack.unsubscribe(this.onBack);
		Listeners.mqttListener.presenceListUpdated.unsubscribe(this.onPresencesUpdated);
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onPageReinit
	 * @public
	 */
	public onPageReinit = (): void => {
		//
	}

	/**
	 * Initialise the dom element references
	 * @method registerDomElements
	 * @private
	 */
	private registerDomElements = (): void => {
		this.backButton = F7.framework7App.dom('#online-back-link');
	}

	/**
	 * Initialise the UI components.
	 * @method initUi
	 * @private
	 */
	private initUi = (): void => {
		this.backButton.off('click', undefined).on('click', this.onBack);
		Listeners.mqttListener.mqttPresenceReceived.publish(undefined);

	}

	/**
	 * Initialise the local template then parse if for handlebars
	 * @method parseTemplate
	 * @private
	 * @returns {Promise<string>} Returns a promise containing the template.
	 */
	private parseTemplate = async (): Promise<void> => {
		try {
			const template = await this.initTemplate();
			this.parsedTemplate = Handlebars.compile(template);
		}
		catch (error) {
			Logger.error('RemindersUi.initPage : Error loading template %s', error.message);
		}
	}

	/**
	 * Initialise the reminder item template for parsing
	 * @method initTemplate
	 * @private
	 * @async
	 * @returns {Promise<string>} Returns a promise containing the template.
	 */
	private initTemplate = (): Promise<string> => {
		return new Promise((resolve, reject): any => {
			return F7.framework7App.app.request.get('pages/templates/onlineListItem.html', (templateData: string) => {
				resolve(templateData);
			}),
			((error: Error) => {
				reject(error.message);
			});
		});
	}

	/**
	 * Refresh the current reminder list on screen.
	 * @method onPresencesUpdated
	 * @private
	 * @async
	 * @param {MqttPresence[]} presences The presences array from the updated event
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onPresencesUpdated = (presences: MqttPresence[]): Promise<void> => {
		let html = '';
		for (const presence of presences) {
			if (presence.mqttHost !== this.mqttStatusUpdate.hostname) {
				html += this.parsedTemplate(presence.asInterface());
			}
		}
		F7.framework7App.dom('#online').html(html);
		return;
	}

	/**
	 * Handle the back button event
	 * @method onBack
	 * @private
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onBack = (): Promise<void> => {
		F7.framework7App.app.views.main.router.back('/', {animate: true});
		return;
	}

	/**
	 * Capture any event changes
	 * @method onStatusChange
	 * @private
	 * @param {IMqttStatusUpdate} mqttStatusUpdate The current mqtt status 
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onStatusChange = (mqttStatusUpdate: IMqttStatusUpdate): Promise<void> => {
		this.mqttStatusUpdate = mqttStatusUpdate;
		if (mqttStatusUpdate.status === MqttStatus.ONLINE) {
			this.onOnline();
		}
		return;
	}
}
