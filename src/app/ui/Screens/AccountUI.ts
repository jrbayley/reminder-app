import { Dom7Instance } from 'dom7';

import { AppData } from '../../core/appData/AppData';
import { LoggedInUser } from '../../core/appData/LoggedInUser';
import { Comms } from '../../core/comms/Comms';
import { Configuration } from '../../core/config/Configuration';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';
import { Logger } from '../../core/logger/Logger';
import { StringUtils } from '../../core/utils/StringUtils';
import { IAccountResponse } from '../../reminder/user/IAccountResponse';
import { IPasswordUpdate } from '../../reminder/user/IPasswordUpdate';
import { UiUtils } from '../utils/UiUtils';
import { UIValidator } from '../utils/UIValidator';
import { BasicScreenInterface } from './BasicScreenInterface';

/**
 * The app account UI controller class
 * @class
 * @implements {BasicScreenInterface}
 */

export class AccountUI implements BasicScreenInterface {

	/** @var {Dom7Instance} backButton Reference to the back button element */
	private backButton : Dom7Instance;

	/** @var {Dom7Instance} emailField field */
	private emailField  : Dom7Instance;

	/** @var {Dom7Instance} accountDescription field */
	private accountDescription  : Dom7Instance;

	/** @var {Dom7Instance} currentPasswordField current password field */
	private currentPasswordField  : Dom7Instance;

	/** @var {Dom7Instance} newPasswordField new password field */
	private newPasswordField  : Dom7Instance;

	/** @var {Dom7Instance} confirmPasswordField confirm password field */
	private confirmPasswordField  : Dom7Instance;

	/** @var {Dom7Instance} savePasswordButton save changes button */
	private savePasswordButton  : Dom7Instance;

	/** @var {boolean} currentPasswordValid Records the validity of the current password */
	private currentPasswordValid : boolean;

	/** @var {boolean} newPasswordValid Records the validity of the new password */
	private newPasswordValid : boolean;

	/** @var {boolean} confirmPasswordValid Records the validity of the confirm password */
	private confirmPasswordValid : boolean;

	/** @var {LoggedInUser} loggedInUser Records the validity of the confirm password */
	private loggedInUser : LoggedInUser;

	/**
	 * @constructor
	 */
	constructor() {
		Listeners.authenticationListener.userSignout.subscribe(this.clearUserDetails);
	}

	/**
	 * Initialise the account page.
	 * @method onPageInit
	 * @public
	 */
	public onPageInit = async (): Promise<void> => {
		Logger.info('AccountUI.initPage : Loading Account');
		try {
			await this.initUserDetails();
		} catch (err) {
			Logger.error('AccountUi.initPage : Error loading user : %s', err.message);
		}
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageIn
	 * @public
	 */
	public onBeforePageIn = async(): Promise<void> => {
		Listeners.deviceListener.deviceBack.subscribe(this.onBack);
		this.registerDomElements();
		this.initUi();
		this.initPassword();
		this.updateUsername();
		return;
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onAfterPageIn
	 * @public
	 */
	public onAfterPageIn = (): Promise<void> => {
		return;
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageOut
	 * @public
	 */
	public onBeforePageOut = (): Promise<void> => {
		Listeners.deviceListener.deviceBack.unsubscribe(this.onBack);
		return;
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onPageReinit
	 * @public
	 */
	public onPageReinit = (): Promise<void> => {
		return;
	}

	/**
	 * Initialise the UI components.
	 * @method initUi
	 * @private
	 */
	private initUi = (): void => {
		this.backButton.off('click', undefined).on('click', this.onBack);
	}

	/**
	 * Initialise the dom element references
	 * @method registerDomElements
	 * @private
	 */
	private registerDomElements = (): void => {
		this.backButton = F7.framework7App.dom('#account-back-link');
		this.accountDescription = F7.framework7App.dom('#accountDescription');
		this.emailField = F7.framework7App.dom('#email');
		this.currentPasswordField = F7.framework7App.dom('#currentPassword');
		this.newPasswordField = F7.framework7App.dom('#newPassword');
		this.confirmPasswordField = F7.framework7App.dom('#confirmPassword');
		this.savePasswordButton = F7.framework7App.dom('#changePassword');
	}


	/**
	 * Initialise the account page form components.
	 * @method initPassword
	 * @private
	 */
	private initPassword = (): void => {
		this.currentPasswordValid = false;
		this.newPasswordValid = false;
		this.confirmPasswordValid = false;
		this.validateForm();
		this.currentPasswordField.off('keyup', undefined).on('keyup', this.validateForm);
		this.currentPasswordField.off('change', undefined).on('change', this.validateForm);
		this.newPasswordField.off('keyup', undefined).on('keyup', this.validateForm);
		this.newPasswordField.off('change', undefined).on('change', this.validateForm);
		this.confirmPasswordField.off('keyup', undefined).on('keyup', this.validateForm);
		this.confirmPasswordField.off('change', undefined).on('change', this.validateForm);
	}


	/**
	 * Start the change password function as button was clicked.
	 * This function checks that the form is valid for submission displaying errors accordingly
	 * @method changePassword
	 * @private
	 */
	private changePassword = (): void => {
		const currentPassword = this.currentPasswordField.val();
		const newPassword = this.newPasswordField.val();
		const confirmPassword = this.confirmPasswordField.val();
		const loggedInUser: LoggedInUser = AppData.currentUser.getLoggedInUser();
		if (loggedInUser && currentPassword && currentPassword !== '') {
			if (currentPassword === loggedInUser.password) {
				if (newPassword && newPassword !== '') {
					if (newPassword === confirmPassword) {
						this.updatePassword(currentPassword, newPassword);
					} else {
						F7.framework7App.app.dialog.alert('The password confirmation does not match');
					}
				} else {
					F7.framework7App.app.dialog.alert('You must enter a new password');
				}
			} else {
				F7.framework7App.app.dialog.alert('Invalid Password');
			}
		} else {
			F7.framework7App.app.dialog.alert('You must enter your current password');
		}
	}

	/**
	 * Handle the updating of the password.
	 * @method updatePassword
	 * @private
	 * @param {string} currentPassword The current password.
	 * @param {string} newPassword The new password to be set.
	 */
	private updatePassword = (currentPassword: string, newPassword: string): void => {
		const updatePasswordRequest: IPasswordUpdate = {
			currentPassword: currentPassword,
			newPassword: newPassword
		};
		this.currentPasswordField.val('');
		this.newPasswordField.val('');
		this.confirmPasswordField.val('');
		this.validateForm();
		Comms.restRequest.restSecurePut(Configuration.REST_HOST + Configuration.REST_ACCOUNT_PASSWORD, updatePasswordRequest)
		.then((accountResponse: IAccountResponse) => {
			if (accountResponse && accountResponse.result) {
				F7.framework7App.app.dialog.alert('Password Updated.');
			} else {
				F7.framework7App.app.dialog.alert(accountResponse.message);
			}
		})
		.catch((_error: Error) => {
			F7.framework7App.app.dialog.alert('Password change failed.');
		});
	}

	/**
	 * Inisialise the form user details
	 * @method initUserDetails
	 * @private
	 * @async
	 */
	private initUserDetails = (): Promise<void> => {
		const loggedInUser = AppData.currentUser.getLoggedInUser();
		if (loggedInUser) {
			this.loggedInUser = loggedInUser;
		} else {
			Logger.debug('Failed to load user')
		}
		return;
	}

	/**
	 * Update the form username
	 * @method updateUsername
	 * @private
	 * @async
	 */
	private updateUsername = (): void => {
		this.emailField.val(this.loggedInUser.username);
		this.accountDescription.html(this.loggedInUser.username);
	}

	/**
	 * Validate the form and set the save button accordingly.
	 * @method validateForm
	 * @private
	 */
	private validateForm = (): void => {
		this.validateCurrentPassword();
		this.validateNewPassword();
		this.validateConfirmPassword();
		const validForm: boolean = this.currentPasswordValid && this.newPasswordValid && this.confirmPasswordValid;
		UiUtils.buttonEnableDisable(this.savePasswordButton, validForm, this.changePassword);
	}


	/**
	 * Validate the current password field.
	 * @method validateCurrentPassword
	 * @private
	 */
	private validateCurrentPassword = (): void => {
		this.currentPasswordValid = StringUtils.stringLengthMoreThan(this.currentPasswordField.val(), 4);
		UIValidator.validationFeedback(this.currentPasswordField, this.currentPasswordValid);
	}

	/**
	 * Validate the new password field.
	 * @method validateNewPassword
	 * @private
	 */
	private validateNewPassword = (): void => {
		this.newPasswordValid = StringUtils.stringLengthMoreThan(this.newPasswordField.val(), 4);
		UIValidator.validationFeedback(this.newPasswordField, this.newPasswordValid);
	}

	/**
	 * Validate the confirm password field.
	 * @method validateConfirmPassword
	 * @private
	 */
	private validateConfirmPassword = (): void => {
		const lengthOk =  StringUtils.stringLengthMoreThan(this.confirmPasswordField.val(), 4);
		const matched = this.confirmPasswordField.val() === this.newPasswordField.val();
		this.confirmPasswordValid = lengthOk && matched;
		UIValidator.validationFeedback(this.confirmPasswordField, this.confirmPasswordValid);
	}

	/**
	 * Clean the local user details cache
	 * @method clearUserDetails
	 * @private
	 */
	private clearUserDetails = (): Promise<void> => {
		this.loggedInUser = undefined;
		return;
	}

	/**
	 * Handle the back button event
	 * @method onBack
	 * @private
	 */
	private onBack = (): Promise<void> => {
		F7.framework7App.app.views.main.router.back('/', {animate: true});
		return;
	}
}
