import { F7 } from '../../core/framework/F7';

/**
 * Splashscreen setup and clear
 * @class
 */

export class SplashScreen {

	/**
	 * Clear the splash screen after a delay
	 * @method clearSplashScreen
	 * @public
	 * @static
	 */
	public static clearSplashScreen = (): void => {
		F7.framework7App.dom('.loader-splash-screen').addClass('splash-fade');
		setTimeout(() => {
			F7.framework7App.dom('.loader-splash-screen').addClass('splash-left');
		}, 300);
	}
}