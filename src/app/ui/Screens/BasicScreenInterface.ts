/**
 * @class
 * Basic App screen interface
 */

export interface BasicScreenInterface {


	/**
	 * initPage The initialise page method
	 * @method initPage
	 */
	onPageInit? (): void;

	/**
	 * handle and before page in actions
	 * @method onBeforePageIn
	 */

	onBeforePageIn (): void;
	/**
	 * handle and after page in actions
	 * @method onAfterPageIn
	 */
	onAfterPageIn (): void;

	/**
	 * handle and before page out actions
	 * @method onBeforePageOut
	 */
	onBeforePageOut (): void;

	/**
	 * handle and before page reinitialise
	 * @method onPageReinit
	 */
	onPageReinit (): void;

	/**
	 * initPageDate The initialise page date method
	 * @method initPageData
	 */
	initPageData? (): void;

	/**
	 * initPageWithItem The initialise page with a specific item method
	 * @method initPageWithItem
	 * @param {string} elementId The element string to retrieve and populate the page with
	 */
	onPageInitWithItem? (elementId: string): void;

	/**
	 * initPage The initialise page method
	 * @method initItemPageData
	 * @param {number} elementId The element number to retrieve and populate the page with
	 */
	initItemPageData? (elementId: number): void;
}