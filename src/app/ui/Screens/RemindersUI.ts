import { Dom7Instance } from 'dom7';
import Handlebars from 'handlebars';

import { AppData } from '../../core/appData/AppData';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';
import { Logger } from '../../core/logger/Logger';
import { ReminderDisplay } from '../../reminder/reminder/ReminderDisplay';
import { BasicScreenInterface } from './BasicScreenInterface';

/**
 * The app settings UI controller class
 * @class
 * @implements {BasicScreenInterface}
 */

export class RemindersUI implements BasicScreenInterface {

	/** @var {Dom7Instance} backButton Reference to the back button element */
	private backButton : Dom7Instance;

	/** @var {HandlebarsTemplateDelegate<any>} parsedTemplate Reference to the list template */
	private parsedTemplate : HandlebarsTemplateDelegate<any>;

	/**
	 * @constructor
	 */
	constructor() {
		this.parseTemplate();
	}

	/**
	 * Initialise the page
	 * @method onPageInit
	 * @public
	 */
	public onPageInit = (): void => {
		this.registerDomElements();
		this.initUi();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageIn
	 * @public
	 */
	public onBeforePageIn = (): void => {
		Listeners.deviceListener.deviceBack.subscribe(this.onBack);
		this.refreshReminderList();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onAfterPageIn
	 * @public
	 */
	public onAfterPageIn = (): void => {
		//
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageOut
	 * @public
	 */
	public onBeforePageOut = (): void => {
		Listeners.deviceListener.deviceBack.unsubscribe(this.onBack);
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onPageReinit
	 * @public
	 */
	public onPageReinit = (): void => {
		//
	}

	/**
	 * Initialise the dom element references
	 * @method registerDomElements
	 * @private
	 */
	private registerDomElements = (): void => {
		this.backButton = F7.framework7App.dom('#reminders-back-link');
	}

	/**
	 * Initialise the UI components.
	 * @method initUi
	 * @private
	 */
	private initUi = (): void => {
		Logger.debug('RemindersUI.initUi : initialising');
		this.backButton.off('click', undefined).on('click', this.onBack);
	}

	/**
	 * Initialise the local template then parse if for handlebars
	 * @method parseTemplate
	 * @private
	 * @returns {Promise<string>} Returns a promise containing the template.
	 */
	public parseTemplate = async (): Promise<void> => {
		try {
			const template = await this.initTemplate();
			this.parsedTemplate = Handlebars.compile(template);
		}
		catch (error) {
			Logger.error('RemindersUi.initPage : Error loading template %s', error.message);
		}
	}

	/**
	 * Initialise the reminder item template for parsing
	 * @method initTemplate
	 * @private
	 * @returns {Promise<string>} Returns a promise containing the template.
	 */
	public initTemplate = (): Promise<string> => {
		return new Promise((resolve, reject): any => {
			return F7.framework7App.app.request.get('pages/templates/reminderListItem.html', (templateData: string) => {
				resolve(templateData);
			}),
			((error: Error) => {
				reject(error.message);
			});
		});
	}

	/**
	 * Refresh the reminder lists on screen.
	 * @method refreshReminderList
	 * @private
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private refreshReminderList = (): Promise<void> => {
		this.refreshCurrentReminderList();
		this.refreshPastReminderList();
		this.refreshArchivedReminderList();
		this.refreshDisabledReminderList();
		return;
	}

	/**
	 * Refresh the current reminder list on screen.
	 * @method refreshCurrentReminderList
	 * @private
	 */
	private refreshCurrentReminderList = (): void => {
		Logger.debug('RemindersUI.refreshCurrentReminderList : Starting redraw.');
		let currentReminderList = '';
		let currentReminderCount = 0;
		for (const reminder of AppData.reminders.getActiveReminders()) {
			const reminderDisplay: ReminderDisplay = new ReminderDisplay(reminder);
			currentReminderList = currentReminderList + this.parsedTemplate(reminderDisplay);
			currentReminderCount++;
		}
		F7.framework7App.dom('.reminder-current').html(currentReminderCount.toString());
		F7.framework7App.dom('#reminderListCurrent').html(currentReminderList);
		Logger.debug('RemindersUI.refreshCurrentReminderList : Finished redraw.');
	}

	/**
	 * Refresh the past reminder list on screen.
	 * @method refreshPastReminderList
	 * @private
	 */
	private refreshPastReminderList = (): void => {
		Logger.debug('RemindersUI.refreshPastReminderList : Starting redraw.');
		let pastReminderList = '';
		let pastReminderCount = 0;
		for (const reminder of AppData.reminders.getPastReminders()) {
			const reminderDisplay: ReminderDisplay = new ReminderDisplay(reminder);
			pastReminderList = pastReminderList + this.parsedTemplate(reminderDisplay);
			pastReminderCount++;
		}
		F7.framework7App.dom('.reminder-past').html(pastReminderCount.toString());
		F7.framework7App.dom('#reminderListPast').html(pastReminderList);
		Logger.debug('RemindersUI.refreshPastReminderList : Finished redraw.');
	}

	/**
	 * Refresh the archived reminder list on screen.
	 * @method refreshArchivedReminderList
	 * @private
	 */
	private refreshArchivedReminderList = (): void => {
		let archivedReminderList = '';
		let archivedReminderCount = 0;
		for (const reminder of AppData.reminders.getArchivedReminders()) {
			const reminderDisplay: ReminderDisplay = new ReminderDisplay(reminder);
			archivedReminderList = archivedReminderList + this.parsedTemplate(reminderDisplay);
			archivedReminderCount++;
		}
		F7.framework7App.dom('.reminder-archived').html(archivedReminderCount.toString());
		F7.framework7App.dom('#reminderListArchived').html(archivedReminderList);
	}

	/**
	 * Refresh the disabled reminder list on screen.
	 * @method refreshDisabledReminderList
	 * @private
	 */
	private refreshDisabledReminderList = (): void => {
		let disabledReminderList = '';
		let disabledReminderCount = 0;
		for (const reminder of AppData.reminders.getDisabledReminders()) {
			const reminderDisplay: ReminderDisplay = new ReminderDisplay(reminder);
			disabledReminderList = disabledReminderList + this.parsedTemplate(reminderDisplay);
			disabledReminderCount++;
		}
		F7.framework7App.dom('.reminder-disabled').html(disabledReminderCount.toString());
		F7.framework7App.dom('#reminderListDisabled').html(disabledReminderList);
	}

	/**
	 * Handle the back button event
	 * @method onBack
	 * @private
	 */
	private onBack = (): Promise<void> => {
		F7.framework7App.app.views.main.router.back('/', {animate: true});
		return;
	}
}
