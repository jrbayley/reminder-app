import { Dom7Instance } from 'dom7';

import { AppData } from '../../core/appData/AppData';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';
import { Logger } from '../../core/logger/Logger';
import { StringUtils } from '../../core/utils/StringUtils';
import { IAddReminder } from '../../reminder/reminder/IAddReminder';
import { ReminderInsertResponse } from '../../reminder/reminder/ReminderInsertResponse';
import { ReminderTools } from '../../reminder/reminder/ReminderTools';
import { UiPicker } from '../utils/UiPicker';
import { UIToast } from '../utils/UIToast';
import { UIValidator } from '../utils/UIValidator';
import { BasicScreenInterface } from './BasicScreenInterface';

/**
 * The Reminder Add UI controller class
 * @class
 * @implements {BasicScreenInterface}
 */

export class AddReminderUI implements BasicScreenInterface {

	/** @var {Dom7Instance} backButton Reference to the back button element */
	private backButton : Dom7Instance;

	/** @var {Dom7Instance} cancelButton Reference to the cancel button element */
	private cancelButton : Dom7Instance;

	/** @var {Dom7Instance} saveReminderButton Reference to the save reminder button */
	private saveReminderButton : Dom7Instance;

	/** @var {Dom7Instance} dateField Reference to the date field */
	private dateField : Dom7Instance;

	/** @var {Dom7Instance} descriptionField Reference to the description field */
	private descriptionField : Dom7Instance;

	/**
	 * @constructor
	 */
	constructor() {
		//
	}

	/**
	 * Initialise the add reminder page class
	 * @method onPageInit
	 * @public
	 */
	public onPageInit = (): void => {
		this.registerDomElements();
		this.initUi();
		this.initFormControls();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageIn
	 * @public
	 */
	public onBeforePageIn = (): void => {
		Listeners.deviceListener.deviceBack.subscribe(this.onBack);
	}


	/**
	 * Handle any tasks before the page transitions in
	 * @method onAfterPageIn
	 * @public
	 */
	public onAfterPageIn = (): void => {
		//
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageOut
	 * @public
	 */
	public onBeforePageOut = (): void => {
		Listeners.deviceListener.deviceBack.unsubscribe(this.onBack);
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onPageReinit
	 * @public
	 */
	public onPageReinit = (): void => {
		//
	}

	/**
	 * Initialise the dom element references
	 * @method registerDomElements
	 * @private
	 */
	private registerDomElements = (): void => {
		this.backButton = F7.framework7App.dom('#addReminder-back-link');
		this.cancelButton = F7.framework7App.dom('#reminder-add-cancel-button');
		UiPicker.calendarWithEvent('#add_reminder_date', this.validateReminderDate);
		UiPicker.timePicker('#add_reminder_time');
		this.dateField = F7.framework7App.dom('#add_reminder_date');
		this.descriptionField = F7.framework7App.dom('#add_reminder_description');
		this.saveReminderButton = F7.framework7App.dom('#reminder-add-save-button');
	}

	/**
	 * Initialise the UI components.
	 * @method initUi
	 * @private
	 */
	private initUi = (): void => {
		this.backButton.off('click', undefined).on('click', this.onBack);
		this.cancelButton.off('click', undefined).on('click', this.onBack);
	}
	/**
	 * Initialise the form controls now that there is a page loaded.
	 * @method initFormControls
	 * @private
	 */
	private initFormControls = (): void => {
		this.dateField.off('change', undefined).on('change', this.validateReminderDate);
		this.saveReminderButton.off('click', undefined).on('click', this.saveReminder);
	}

	/**
	 * Validate all the fields on the form.
	 * @method validateForm
	 * @private
	 */
	private validateForm = (): void => {
		this.validateReminderDate();
		this.validateReminderDescription();
	}

	/**
	 * Start reminder date UI validation
	 * @method validateReminderDate
	 * @private
	 */
	private validateReminderDate = (): void => {
		UIValidator.inputValidator(this.dateField, 'You must enter a date', this.dateIsValid());
	}

	/**
	 * Start reminder date UI validation
	 * @method validateReminderDate
	 * @private
	 */
	private validateReminderDescription = (): void => {
		UIValidator.inputValidator(this.descriptionField, 'You must enter a description', this.descriptionIsValid());
	}
	/**
	 * Validate the reminder date
	 * @method dateIsValid
	 * @private
	 * @returns {boolean} True if the date is valid
	 */
	private dateIsValid = (): boolean => {
		return StringUtils.stringLengthMoreThan(this.dateField.val(), 5);
	}

	/**
	 * Validate the reminder date
	 * @method descriptionIsValid
	 * @private
	 * @returns {boolean} True if the date is valid
	 */
	private descriptionIsValid = (): boolean => {
		return StringUtils.stringLengthMoreThan(this.descriptionField.val(), 4);
	}

	/**
	 * Save the reminder addition to the server.
	 * @method saveReminder
	 * @private
	 */
	private saveReminder = (): void => {
		this.validateForm();
		const addReminder: any = F7.framework7App.app.form.convertToData('#addReminderForm');
		const newReminder: IAddReminder = ReminderTools.createAddReminder(addReminder);
		this.uploadReminder(newReminder)
		.then((reminderInsertResponse: ReminderInsertResponse) => {
			if (reminderInsertResponse && reminderInsertResponse.success) {
				Logger.debug('AddReminderUi.saveReminder : Saved Reminder');
				UIToast.displayCloseableToastWithIconAndTimeout('fa-check', 'Saved', 1300);
				AppData.reminders.refreshReminders()
				.then(() => {
					F7.framework7App.app.views.main.router.navigate('/reminders/', {animate: true, transition: 'f7-dive', history: false});
				});
			} else {
				UIToast.displayCloseableToastWithIconAndTimeout('fa-times', reminderInsertResponse.message, 3000);
			}
		});
	}

	/**
	 * Save the reminder addition to the server.
	 * @method saveReminder
	 * @private
	 */
	private uploadReminder = async(reminder: IAddReminder): Promise<ReminderInsertResponse> => {
		return AppData.reminders.insertReminder(reminder);
	}

	/**
	 * Handle the back button event
	 * @method onBack
	 * @private
	 */
	private onBack = (): Promise<void> => {
		F7.framework7App.app.views.main.router.back('/', {animate: true});
		return;
	}
}
