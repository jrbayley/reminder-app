import { BasicScreenInterface } from './BasicScreenInterface';

/**
 * Basic App screen with Pull to Refresh interface
 * @class
 */

export interface PtrScreenInterface extends BasicScreenInterface {

	/**
	 * reload the item after the ptr fires
	 * @method
	 * @param {number} elementId The element number to retrieve and populate the page with
	 */
	reloadItemView? (elementId: string): void;

	/**
	 * initialise the Pull to refresh item
	 * @method
	 */
	initPtr (): void;
}