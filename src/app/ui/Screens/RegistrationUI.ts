import { Dom7Instance } from 'dom7';

import { SigninStage } from '../../core/authentication/models/SigninStage';
import { Comms } from '../../core/comms/Comms';
import { Configuration } from '../../core/config/Configuration';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';
import { Logger } from '../../core/logger/Logger';
import { StringUtils } from '../../core/utils/StringUtils';
import { IAccountResponse } from '../../reminder/user/IAccountResponse';
import { UIToast } from '../utils/UIToast';
import { UIValidator } from '../utils/UIValidator';

export class RegistrationUI {

	/** @var {Dom7Instance} cancelRegistrationButton Reference to the cancelbutton element */
	private cancelRegistrationButton : Dom7Instance;

	/** @var {Dom7Instance} submitRegistrationButton Reference to the submit button element */
	private submitRegistrationButton : Dom7Instance;

	/** @var {Dom7Instance} firstnameField Reference to the firstname field element */
	private firstnameField : Dom7Instance;

	/** @var {Dom7Instance} surnameField Reference to the surname field element */
	private surnameField : Dom7Instance;

	/** @var {Dom7Instance} emailField Reference to the email field element */
	private emailField : Dom7Instance;

	/** @var {Dom7Instance} passwordField Reference to the  password element */
	private passwordField : Dom7Instance;

	/** @var {Dom7Instance} confirmPasswordField Reference to the confirm password element */
	private confirmPasswordField : Dom7Instance;

	/** @var {Dom7Instance} termsAndConditionsCheckbox Reference to the terms checkbox element */
	private termsAndConditionsCheckbox : Dom7Instance;
	/**
	 * @constructor
	 */
	constructor() {
		Listeners.authenticationListener.signinStage.subscribe(this.startRegistration);
	}

	/**
	 * Initialise the about page class
	 * @method onPageInit
	 * @private
	 */
	public onPageInit = (): void => {
		this.registerDomElements();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageIn
	 * @public
	 */
	public onBeforePageIn = (): void => {
		//
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onAfterPageIn
	 * @public
	 */
	public onAfterPageIn = (): void => {
		//
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageOut
	 * @public
	 */
	public onBeforePageOut = (): void => {
		//
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onPageReinit
	 * @public
	 */
	public onPageReinit = (): void => {
		//
	}

	/**
	 * Initialise the dom element references
	 * @method registerDomElements
	 * @private
	 */
	private registerDomElements = (): void => {
		this.cancelRegistrationButton = F7.framework7App.dom('#cancel-registration-button');
		this.submitRegistrationButton = F7.framework7App.dom('#submit-registration-button');
		this.firstnameField = F7.framework7App.dom('#registration_firstname');
		this.surnameField = F7.framework7App.dom('#registration_surname');
		this.emailField = F7.framework7App.dom('#registration_email');
		this.passwordField = F7.framework7App.dom('#registration_password');
		this.confirmPasswordField = F7.framework7App.dom('#registration_password_confirm');
		this.termsAndConditionsCheckbox = F7.framework7App.dom('#registration_terms');
		this.initUi();
	}

	/**
	 * Initialise the UI components.
	 * @method initUi
	 * @private
	 */
	private initUi = (): void => {
		this.cancelRegistrationButton.off('click', undefined).on('click', this.cancelRegistration);
		this.submitRegistrationButton.off('click', undefined).on('click', this.submitRegistration);
		this.termsAndConditionsCheckbox.off('click', undefined).on('click', this.validateTerms);
		this.firstnameField.off('keyup', undefined).on('keyup', this.validateFirstname);
		this.surnameField.off('keyup', undefined).on('keyup', this.validateSurname);
		this.passwordField.off('keyup', undefined).on('keyup', this.validatePassword);
		this.confirmPasswordField.off('keyup', undefined).on('keyup', this.validatePasswordConfirmation);
		this.confirmPasswordField.off('keyup', undefined).on('keyup', this.validatePasswordConfirmation);
		this.emailField.off('keyup', undefined).on('keyup', this.validateEmail);
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method startRegistration
	 * @public
	 */
	public startRegistration = (): Promise<void> => {
		this.registerDomElements();
		return;
	}

	/**
	 * Cancel the registration and return to the home screen
	 * @method cancelRegistration
	 * @public
	 */
	public cancelRegistration = (): void => {
		Listeners.authenticationListener.signinStage.publish(SigninStage.START);
		setTimeout(() => {
			F7.framework7App.app.views.main.router.back('/', {animate: true});
		}, 500);
	}

	/**
	 * validate the firstname field
	 * @method validateFirstname
	 * @public
	 * @returns {boolean} True if the field is valid
	 */
	public validateFirstname = (): boolean => {
		const firstnameValid = StringUtils.stringLengthMoreThan(this.firstnameField.val(), 1);
		UIValidator.inputValidator(this.firstnameField, 'You must enter a first name', firstnameValid);
		return firstnameValid;
	}

	/**
	 * validate the surname field
	 * @method validateSurname
	 * @public
	 * @returns {boolean} True if the field is valid
	 */
	public validateSurname = (): boolean => {
		const surnameValid = StringUtils.stringLengthMoreThan(this.surnameField.val(), 1);
		UIValidator.inputValidator(this.surnameField, 'You must enter a surname', surnameValid);
		return surnameValid;
	}

	/**
	 * validate the email field
	 * @method validateEmail
	 * @public
	 * @returns {boolean} True if the field is valid
	 */
	public validateEmail = (): boolean => {
		const emailValid = StringUtils.isValidEmail(this.emailField.val());
		UIValidator.inputValidator(this.emailField, 'You must enter a valid email address', emailValid);
		return emailValid;
	}

	/**
	 * Validate the password field
	 * @method validatePassword
	 * @public
	 * @returns {boolean} True if the field is valid
	 */
	public validatePassword = (): boolean => {
		const passwordValid = StringUtils.stringLengthMoreThan(this.passwordField.val(), 4);
		UIValidator.inputValidator(this.passwordField, 'Password must be 4 characters or more', passwordValid);
		return passwordValid;
	}

	/**
	 * Validate the password confirmation field
	 * @method validatePasswordConfirmation
	 * @public
	 * @returns {boolean} True if the field is valid
	 */
	public validatePasswordConfirmation = (): boolean => {
		const passwordValid = this.passwordField.val() !== '' && this.passwordField.val() === this.confirmPasswordField.val();
		UIValidator.inputValidator(this.confirmPasswordField, 'Passwords do not match', passwordValid);
		return passwordValid;
	}

	/**
	 * Validate the password confirmation field
	 * @method validateTerms
	 * @public
	 * @returns {boolean} True if the field is valid
	 */
	public validateTerms = (): boolean => {
		const termsValid = this.termsAndConditionsCheckbox.prop('checked');
		UIValidator.checkboxValidator(this.termsAndConditionsCheckbox, 'You must accept the terms and conditions', termsValid);
		return termsValid;
	}

	/**
	 * Submit the registration
	 * @method validateForm
	 * @public
	 */
	public validateForm = (): boolean => {
		const termsValid = this.validateTerms();
		const firstnameValid = this.validateFirstname();
		const surnameValid = this.validateSurname();
		const passwordValid = this.validatePassword();
		const confirmationPasswordValid = this.validatePasswordConfirmation();
		const emailValid = this.validateEmail();
		return (termsValid && passwordValid && confirmationPasswordValid && emailValid && firstnameValid && surnameValid);
	}

	/**
	 * Submit the registration
	 * @method submitRegistration
	 * @public
	 */
	public submitRegistration = (): void => {
		if (this.validateForm()) {
			const registrationForm = F7.framework7App.app.form.convertToData('#registration-form');
			Comms.restRequest.restPost(Configuration.REST_HOST + Configuration.REST_REGISTRATION, registrationForm)
			.then((registrationResponse: IAccountResponse) => {
				if (registrationResponse.result) {
					UIToast.displayCloseableToastWithIconAndTimeout('fa-check', 'Check your email for the email verification code', 3000);
					Listeners.authenticationListener.signinStage.publish(SigninStage.VERIFY);
				} else {
					UIToast.displayCloseableToastWithIconAndTimeout('fa-times', registrationResponse.message, 3000);
				}
			})
			.catch((error: Error) => {
				Logger.error('RegistrationUI.submitRegistration : Error registering user : %s', error.message);
			});
		}
	}
}