import { Dom7Instance } from 'dom7';
import moment from 'moment';

import { AppData } from '../../core/appData/AppData';
import { IDeleteResponse } from '../../core/appData/IDeleteResponse';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';
import { Logger } from '../../core/logger/Logger';
import { IEditReminder } from '../../reminder/reminder/IEditReminder';
import { IReminder } from '../../reminder/reminder/IReminder';
import { ReminderTools } from '../../reminder/reminder/ReminderTools';
import { UiDialog } from '../utils/UiDialog';
import { UiPicker } from '../utils/UiPicker';
import { UIToast } from '../utils/UIToast';
import { BasicScreenInterface } from './BasicScreenInterface';

/**
 * The Reminder Edit UI controller class
 * @class
 * @implements {BasicScreenInterface}
 */

export class ReminderUI implements BasicScreenInterface {

	/** @var {Dom7Instance} backButton Reference to the back button element */
	private backButton : Dom7Instance;

	/** @var {IReminder} reminder Reference to currently open reminder */
	private reminder : IReminder;

	/** @var {Dom7Instance} saveReminderChangesButton Reference to the save button element */
	private saveReminderChangesButton : Dom7Instance;

	/** @var {Dom7Instance} deleteReminderChangesButton Reference to the delete button element */
	private deleteReminderChangesButton : Dom7Instance;

	/** @var {Dom7Instance} reminderIdField Reference to the reminder id element */
	private reminderIdField : Dom7Instance;


	/**
	 * @constructor
	 */
	constructor() {
		//
	}

	/**
	 * Initialise the reminder page class
	 * @method onPageInitWithItem
	 * @public
	 * @param {string} id The reminder ID to obtain.
	 */
	public onPageInitWithItem = (id: string): void => {
		const reminderId: number = parseInt(id);
		const reminder: IReminder = AppData.reminders.getReminder(reminderId);
		this.reminder = reminder;

		this.registerDomElements();
		this.initFormControls();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageIn
	 * @public
	 */
	public onBeforePageIn = (): void => {
		Listeners.deviceListener.deviceBack.subscribe(this.onBack);
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onAfterPageIn
	 * @public
	 */
	public onAfterPageIn = (): void => {
		//
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageOut
	 * @public
	 */
	public onBeforePageOut = (): void => {
		Listeners.deviceListener.deviceBack.unsubscribe(this.onBack);
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onPageReinit
	 * @public
	 */
	public onPageReinit = (): void => {
		//
	}

	/**
	 * Initialise the dom element references
	 * @method registerDomElements
	 * @private
	 */
	private registerDomElements = (): void => {
		this.saveReminderChangesButton = F7.framework7App.dom('#reminder-edit-save-button');
		this.reminderIdField = F7.framework7App.dom('#edit_reminder_id');
		this.deleteReminderChangesButton = F7.framework7App.dom('#reminder-edit-delete-button');
		this.backButton = F7.framework7App.dom('#reminder-back-link');
	}

	/**
	 * Initialise the form controls now that there is a page loaded.
	 * @method initFormControls
	 * @private
	 */
	private initFormControls = (): void => {
		this.backButton.off('click', undefined).on('click', this.onBack);
		const editReminder: IEditReminder = ReminderTools.createEditReminder(this.reminder);
		F7.framework7App.app.form.fillFromData('#editReminderForm', editReminder);

		const selectedDate: Date[] = [];
		selectedDate.push(moment(this.reminder.reminder_date).toDate());
		UiPicker.calendarWithDate('#edit_reminder_date', selectedDate);
		UiPicker.timePicker('#edit_reminder_time');
		// Buttons
		this.saveReminderChangesButton.off('click', undefined).on('click', this.saveReminderChanges);
		this.deleteReminderChangesButton.off('click', undefined).on('click', this.deleteReminderConfirm);
	}

	/**
	 * Display the reminder delete confirmation dialogue.
	 * @method deleteReminderConfirm
	 * @private
	 */
	private deleteReminderConfirm = (): void => {
		UiDialog.dialogConfirm('Do you really want to delete this reminder?', 'Confirm Delete', this.deleteReminder, this.cancelDeleteReminder);
	}

	/**
	 * Manage the reminder delete.
	 * @method deleteReminder
	 * @private
	 */
	private deleteReminder = (): void => {
		const reminderId = this.reminderIdField.val();
		AppData.reminders.deleteReminderFromServer(reminderId)
		.then((response: IDeleteResponse) => {
			if (response.deleted === 1) {
				AppData.reminders.refreshReminders();
				F7.framework7App.app.views.main.router.navigate('/reminders/', {animate: true, transition: 'f7-dive', history: false});
			}
		})
		.catch((error: Error) => {
			Logger.error('Error deleting reminder %s', error.message);
		});
	}

	/**
	 * Cancel the request to delete.
	 * @method cancelDeleteReminder
	 * @private
	 */
	private cancelDeleteReminder = (): void => {
		//
	}

	/**
	 * Save the reminder changes to the server.
	 * @method saveReminderChanges
	 * @private
	 */
	private saveReminderChanges = (): void => {
		const editReminderForm: any = F7.framework7App.app.form.convertToData('#editReminderForm');
		const editReminder: IReminder = ReminderTools.createReminderUpdate(editReminderForm);
		AppData.reminders.updateReminder(editReminder)
		.then(() => {
			UIToast.displayCloseableToastWithIconAndTimeout('fa-check', 'Saved', 1300);
		});
	}

	/**
	 * @method
	 * @private
	 * Handle the back button event
	 */
	private onBack = (): Promise<void> => {
		F7.framework7App.app.views.main.router.back('/', {animate: true});
		return;
	}
}
