import { Dom7Instance } from 'dom7';

import { AppData } from '../../core/appData/AppData';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';
import { Logger } from '../../core/logger/Logger';
import { BasicScreenInterface } from './BasicScreenInterface';

/**
 * The app settings UI controller class
 * @class
 * @implements {BasicScreenInterface}
 */

export class SettingsUI implements BasicScreenInterface {

	/**
	 * @constructor
	 */
	constructor() {
		this.initOnChangeListeners();
		this.initSettings();
	}

	/**
	 * Initialise the settings page class
	 * @method onPageInit
	 * @public
	 */
	public onPageInit = (): void => {
		this.registerDomElements();
		this.initUi();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageIn
	 * @public
	 */
	public onBeforePageIn = (): void => {
		Listeners.deviceListener.deviceBack.subscribe(this.onBack);
	}

	/**
	 * Initialise the subscription to settings changes
	 * @method initOnChangeListeners
	 * @public
	 */

	 private initOnChangeListeners = (): void => {
		Listeners.settingsListener.settingsColourChanged.subscribe(this.onColourChange);
		Listeners.settingsListener.settingsThemeChanged.subscribe(this.onThemeChange);
	}

	/**
	 * Handle any tasks after the page transitions in
	 * @method onAfterPageIn
	 * @public
	 */
	public onAfterPageIn = (): void => {
		this.updateSettingsUi();
	}

	/**
	 * Handle any tasks before the page transitions out
	 * @method onBeforePageOut
	 * @public
	 */
	public onBeforePageOut = (): void => {
		Listeners.deviceListener.deviceBack.unsubscribe(this.onBack);
	}

	/**
	 * Handle any tasks before the page re-initialises
	 * @method onPageReinit
	 * @public
	 */
	public onPageReinit = (): void => {
		this.updateSettingsUi();
	}

	/**
	 * Initialise the dom element references
	 * @method registerDomElements
	 * @private
	 */
	private registerDomElements = (): void => {
		//
	}

	/**
	 * Initialise the UI components.
	 * @method initUi
	 * @private
	 */
	private initUi = (): void => {
		F7.framework7App.dom('#settings-back-link').off('click', undefined).on('click', this.onBack);
		F7.framework7App.dom('#settings-radio-dark').off('change', undefined).on('change', this.setDarkTheme);
		F7.framework7App.dom('#settings-radio-light').off('change', undefined).on('change', this.setLightTheme);
		F7.framework7App.dom('.settings-radio-colour').off('change', undefined).on('change', this.setColorTheme);
	}

	/**
	 * set the app to dark theme
	 * @method setDarkTheme
	 * @private
	 */
	private setDarkTheme = (): void => {
		AppData.appSettings.setTheme('theme-dark');
		Listeners.settingsListener.settingsThemeChanged.publish('theme-dark');
	}

	/**
	 * set the app to light theme
	 * @method setLightTheme
	 * @private
	 */
	private setLightTheme = (): void => {
		AppData.appSettings.setTheme('theme-light');
		Listeners.settingsListener.settingsThemeChanged.publish('theme-light');
	}

	/**
	 * set the app to the requested colour theme
	 * @method setColorTheme
	 * @param {Event} event The event that was fired when changing theme
	 */
	private setColorTheme = (event: Event): void => {
		AppData.appSettings.setColour((<HTMLInputElement>event.srcElement).value);
	}

	/**
	 * Handle the back button event
	 * @method onBack
	 * @private
	 */
	private onBack = (): Promise<void> => {
		F7.framework7App.app.views.main.router.back('/', {animate: true});
		return;
	}

	/**
	 * Update the UI with the settings from this class variables
	 * @method
	 * @public
	 */
	public updateSettingsUi = (): void => {
		if (AppData.appSettings.appTheme === 'theme-dark') {
			F7.framework7App.dom('#settings-radio-dark').prop('checked', true);
		} else {
			F7.framework7App.dom('#settings-radio-light').prop('checked', true);
		}
		Logger.debug('Selected Color is %s', AppData.appSettings.appTheme);
		F7.framework7App.dom('#settings-radio-color-theme-' + AppData.appSettings.appColour).prop('checked', true);
	}

	/**
	 * handle settings colour change event
	 * @method onColourChange
	 * @private
	 * @param {string} colour The colour to update the ui with
	 */
	private onColourChange = (colour: string): Promise<void> => {
		const body: Dom7Instance = F7.framework7App.dom('body');
		const navList: Dom7Instance = F7.framework7App.dom('#nav-list');
		body.removeClass('color-theme-red');
		body.removeClass('color-theme-green');
		body.removeClass('color-theme-blue');
		body.removeClass('color-theme-pink');
		body.removeClass('color-theme-yellow');
		body.removeClass('color-theme-orange');
		body.removeClass('color-theme-purple');
		body.removeClass('color-theme-deeppurple');
		body.removeClass('color-theme-lightbuel');
		body.removeClass('color-theme-teal');
		body.removeClass('color-theme-lime');
		body.removeClass('color-theme-deeporange');
		body.removeClass('color-theme-gray');
		body.removeClass('color-theme-white');
		body.removeClass('color-theme-black');

		navList.removeClass('text-color-red');
		navList.removeClass('text-color-green');
		navList.removeClass('text-color-blue');
		navList.removeClass('text-color-pink');
		navList.removeClass('text-color-yellow');
		navList.removeClass('text-color-orange');
		navList.removeClass('text-color-purple');
		navList.removeClass('text-color-deeppurple');
		navList.removeClass('text-color-lightbuel');
		navList.removeClass('text-color-teal');
		navList.removeClass('text-color-lime');
		navList.removeClass('text-color-deeporange');
		navList.removeClass('text-color-gray');
		navList.removeClass('text-color-white');
		navList.removeClass('text-color-black');
		body.addClass('color-theme-' + colour);
		navList.addClass('text-color-' + colour);
		this.updateSettingsUi();
		return;
	}

	/**
	 * Handle theme change event
	 * @method onThemeChange
	 * @private
	 * @param {string} theme The theme name to update the ui with
	 */
	private onThemeChange = (theme: string): Promise<void> => {
		F7.framework7App.dom('body').removeClass('theme-light');
		F7.framework7App.dom('body').removeClass('theme-dark');
		F7.framework7App.dom('body').addClass(theme);
		this.updateSettingsUi();
		return;
	}

	/**
	 * Initialise settings by retrieving stored settings and publishing to the listeners
	 * @method initSettings
	 * @private
	 */
	private initSettings = (): void => {
		Listeners.settingsListener.settingsColourChanged.publish(AppData.appSettings.getColour());
		Listeners.settingsListener.settingsThemeChanged.publish(AppData.appSettings.getTheme());
	}
}
