import { Dom7Instance } from 'dom7';

import { AppConfiguration } from '../../core/config/AppConfiguration';
import { Device } from '../../core/device/Device';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';
import { Version } from '../../Version';
import { BasicScreenInterface } from './BasicScreenInterface';

/**
 * The app about UI controller class
 * @class
 * @implements {BasicScreenInterface}
 */

export class AboutUI implements BasicScreenInterface {

	/** @var {Dom7Instance} backButton Reference to the back button element */
	private backButton : Dom7Instance;

	/** @var {Dom7Instance} name The App name */
	private name : Dom7Instance;

	/** @var {Dom7Instance} version The App version */
	private version : Dom7Instance;

	/** @var {Dom7Instance} build The App build id */
	private build : Dom7Instance;

	/** @var {Dom7Instance} buildTime The App buildTime */
	private buildTime : Dom7Instance;


	/** @var {Dom7Instance} deviceInfoCordova The cordova flag for device section */
	private deviceInfoCordova : Dom7Instance;

	/** @var {Dom7Instance} deviceInfoManufacturer The manufacturerfor device section */
	private deviceInfoManufacturer : Dom7Instance;

	/** @var {Dom7Instance} deviceInfoModel The model for device section */
	private deviceInfoModel : Dom7Instance;

	/** @var {Dom7Instance} deviceInfoPlatform The platform for device section */
	private deviceInfoPlatform : Dom7Instance;

	/** @var {Dom7Instance} deviceInfoVersion The version for device section */
	private deviceInfoVersion : Dom7Instance;

	/** @var {Dom7Instance} deviceInfoSerial The serial No for device section */
	private deviceInfoSerial : Dom7Instance;

	/** @var {Dom7Instance} deviceInfoUuid The UUID for device section */
	private deviceInfoUuid : Dom7Instance;

	/** @var {Dom7Instance} deviceInfoVirtual The virtual flag for device section */
	private deviceInfoVirtual : Dom7Instance;

	/** @var {Dom7Instance} deviceInfoAvailable The available flag for device section */
	private deviceInfoAvailable : Dom7Instance;

	/** @var {Dom7Instance} networkStatus The network status */
	private networkStatus : Dom7Instance;

	/** @var {Dom7Instance} connectionType The connection type */
	private connectionType : Dom7Instance;

	/**
	 * @constructor
	 */
	constructor() {
		//
	}

	/**
	 * Initialise the about page class
	 * @method onPageInit
	 * @private
	 */
	public onPageInit = (): void => {
		Listeners.deviceListener.deviceNetwork.subscribe(this.onNetworkChange);
		this.registerDomElements();
		this.initUi();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageIn
	 * @public
	 */
	public onBeforePageIn = (): void => {
		Listeners.deviceListener.deviceBack.subscribe(this.onBack);
		this.backButton.off('click', undefined).on('click', this.onBack);
		this.initFormData();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onAfterPageIn
	 * @public
	 */
	public onAfterPageIn = (): void => {
		//
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageOut
	 * @public
	 */
	public onBeforePageOut = (): void => {
		Listeners.deviceListener.deviceBack.unsubscribe(this.onBack);
		Listeners.deviceListener.deviceNetwork.unsubscribe(this.onNetworkChange);
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onPageReinit
	 * @public
	 */
	public onPageReinit = (): void => {
		return this.onBeforePageIn();
	}

	/**
	 * @method
	 * @private
	 * Initialise the dom element references
	 */
	private registerDomElements = (): void => {
		this.backButton = F7.framework7App.dom('#about-back-link');
		this.name = F7.framework7App.dom('#device_info_name');
		this.version = F7.framework7App.dom('#device_info_version');
		this.build = F7.framework7App.dom('#device_info_build');
		this.buildTime = F7.framework7App.dom('#device_info_build_time');
		this.networkStatus = F7.framework7App.dom('#device_diags_network');
		this.connectionType = F7.framework7App.dom('#device_diags_connection_type');
		this.deviceInfoCordova = F7.framework7App.dom('#device_info_cordova');
		this.deviceInfoManufacturer = F7.framework7App.dom('#device_info_manufacturer');
		this.deviceInfoModel = F7.framework7App.dom('#device_info_model');
		this.deviceInfoPlatform = F7.framework7App.dom('#device_info_platform');
		this.deviceInfoVersion = F7.framework7App.dom('#device_info_version');
		this.deviceInfoSerial = F7.framework7App.dom('#device_info_serial');
		this.deviceInfoUuid = F7.framework7App.dom('#device_info_uuid');
		this.deviceInfoVirtual = F7.framework7App.dom('#device_info_virtual');
		this.deviceInfoAvailable = F7.framework7App.dom('#device_info_available');
	}

	/**
	 * @method
	 * @private
	 * Initialise the UI components.
	 */
	private initUi = (): void => {
		//
	}

	/**
	 * Initialise the values into the dom element references
	 * @method initFormData
	 * @private
	 */
	private initFormData = (): void => {
		this.name.html(AppConfiguration.APPNAME);
		this.version.html(Version.version);
		this.build.html(Version.buildID);
		this.buildTime.html(Version.buildTime);
		this.deviceInfoCordova.html(Device.localDevice.deviceInformation.cordova);
		this.deviceInfoManufacturer.html(Device.localDevice.deviceInformation.manufacturer);
		this.deviceInfoModel.html(Device.localDevice.deviceInformation.model);
		this.deviceInfoPlatform.html(Device.localDevice.deviceInformation.platform);
		this.deviceInfoVersion.html(Device.localDevice.deviceInformation.version);
		this.deviceInfoSerial.html(Device.localDevice.deviceInformation.serial);
		this.deviceInfoUuid.html(Device.localDevice.deviceInformation.uuid);
		this.deviceInfoVirtual.html((Device.localDevice.deviceInformation.isVirtual) ? 'Yes' : 'No');
		this.deviceInfoAvailable.html((Device.localDevice.deviceInformation.available) ? 'Yes' : 'No');;
		this.updateUiNetworkStatus(undefined);
	}

	/**
	 * Handle the network state change from listener
	 * @method onNetworkChange
	 * @private
	 * @async
	 * @param {string} status The current network state
	 * @returns {Promise<void>} Returns an emtyp promise
	 */
	private onNetworkChange = (status: string): Promise<void> => {
		this.updateUiNetworkStatus(status);
		return;
	}

	/**
	 * Update the UI with the network state and type
	 * @method updateUiNetworkStatus
	 * @private
	 * @param {string} status The current network state
	 */
	private updateUiNetworkStatus = (status: string): void => {
		const online = (status) ? status : 'Unavailable';
		const connectionType =  (Device && Device.cordova && Device.cordova.network) ? Device.cordova.network.connectionType : 'Unavailable';
		this.networkStatus.html(online);
		this.connectionType.html(connectionType);
	}

	/**
	 * Handle the back button event
	 * @method onBack
	 * @private
	 */
	private onBack = (): Promise<void> => {
		F7.framework7App.app.views.main.router.back('/', {animate: true});
		return;
	}
}
