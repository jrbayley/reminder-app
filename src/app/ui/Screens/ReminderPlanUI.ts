import { Dom7Instance } from 'dom7';
import Handlebars from 'handlebars';
import moment from 'moment';

import { AppData } from '../../core/appData/AppData';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';
import { Logger } from '../../core/logger/Logger';
import { IReminderPlan } from '../../reminder/reminder/IReminderPlan';
import { ReminderPlanDisplay } from '../../reminder/reminder/ReminderPlanDisplay';
import { BasicScreenInterface } from './BasicScreenInterface';

/**
 * The app reminder plan UI controller class
 * @class
 * @implements {BasicScreenInterface}
 */

export class ReminderPlanUI implements BasicScreenInterface {

	/** @var {Dom7Instance} backButton Reference to the back button element */
	private backButton : Dom7Instance;

	/** @var {HandlebarsTemplateDelegate<any>} parsedTemplate Reference to the list template */
	private parsedTemplate : HandlebarsTemplateDelegate<any>;

	/**
	 * @constructor
	 */
	constructor() {
		Listeners.deviceListener.deviceBack.subscribe(this.onBack);
		this.parseTemplate();
	}

	/**
	 * Initialise the page
	 * @method onPageInit
	 * @public
	 */
	public onPageInit = (): void => {
		this.registerDomElements();
		this.initPageDates();
		this.initUi();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageIn
	 * @public
	 */
	public onBeforePageIn = (): void => {
		Listeners.deviceListener.deviceBack.unsubscribe(this.onBack);
		Listeners.reminderListener.reminderPlanChanged.subscribe(this.refreshReminderPlanList);
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onAfterPageIn
	 * @public
	 */
	public onAfterPageIn = (): void => {
		//
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageOut
	 * @public
	 */
	public onBeforePageOut = (): void => {
		Listeners.deviceListener.deviceBack.unsubscribe(this.onBack);
		Listeners.reminderListener.reminderPlanChanged.unsubscribe(this.refreshReminderPlanList);
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onPageReinit
	 * @public
	 */
	public onPageReinit = (): void => {
		//
	}

	/**
	 * Initialise the UI components.
	 * @method initUi
	 * @private
	 */
	private initUi = (): void => {
		this.backButton.off('click', undefined).on('click', this.onBack);
		this.initTemplate();
		this.refreshReminderPlanList();
	}

	/**
	 * Initialise the dom element references
	 * @method registerDomElements
	 * @private
	 */
	private registerDomElements = (): void => {
		this.backButton = F7.framework7App.dom('#reminderPlan-back-link');
	}

	/**
	 * Initialise the local template then parse if for handlebars
	 * @method parseTemplate
	 * @private
	 * @returns {Promise<string>} Returns a promise containing the template.
	 */
	public parseTemplate = async (): Promise<void> => {
		try {
			const template = await this.initTemplate();
			this.parsedTemplate = Handlebars.compile(template);
		}
		catch (error) {
			Logger.error('ReminderPlanUi.initPage : Error loading template %s', error.message);
		}
	}

	/**
	 * Initialise the reminder item template for parsing
	 * @method initTemplate
	 * @private
	 * @async
	 * @returns {Promise<string>} Returns a promise containing the template.
	 */
	public initTemplate = (): Promise<string> => {
		return new Promise((resolve, reject): any => {
			return F7.framework7App.app.request.get('pages/templates/reminderPlanListItem.html', (templateData: string) => {
				resolve(templateData);
			}),
			((error: Error) => {
				reject(error.message);
			});
		});
	}

	/**
	 * Refresh the reminder list on screen.
	 * @method refreshReminderPlanList
	 * @private
	 * @async
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private refreshReminderPlanList = (): Promise<void> => {
		let reminderTotal = 0;
		F7.framework7App.dom('.reminder-plan-counter-bubble').html('0');
		F7.framework7App.dom('.reminder-plan-counter-bubble').removeClass('with-reminder');
		F7.framework7App.dom('.reminder-content-calendar-month').html('');
		const reminderPlans: IReminderPlan[] = AppData.reminderPlans.getReminderPlan();
		for (const reminderPlan of reminderPlans) {
			if (reminderPlan) {
				const displayMonth = moment(reminderPlan.reminder_date).format('M');
				const currentCalCount = parseInt(F7.framework7App.dom('#reminder-plan-counter-bubble-' + displayMonth).html()) + 1;
				reminderTotal++;
				F7.framework7App.dom('#reminder-plan-counter-bubble-' + displayMonth).addClass('with-reminder');
				F7.framework7App.dom('#reminder-plan-counter-bubble-' + displayMonth).html(currentCalCount.toString());
				const reminderPlanDisplay: ReminderPlanDisplay = new ReminderPlanDisplay(reminderPlan);
				F7.framework7App.dom('#reminder-plan-' + displayMonth).append(this.parsedTemplate(reminderPlanDisplay));
				F7.framework7App.dom('#reminder-plan-month-title-' + displayMonth).html(reminderPlanDisplay.month + ' ' + reminderPlanDisplay.year);
				F7.framework7App.dom('#reminder-plan-month-title-' + displayMonth).addClass('text-color-primary');
			}
		}
		if (reminderTotal > 0) {
			F7.framework7App.dom('#reminder-total').html('<span class="badge color-primary">' + reminderTotal + '</span>');
		} else {
			F7.framework7App.dom('#reminder-total').html('');
		}
		return;
	}

	/**
	 * Initialise the dates on the calendar page.
	 * @method initPageDates
	 * @private
	 */
	private initPageDates = (): void => {
		for (let month = 1; month <= 12; month ++) {
			F7.framework7App.dom('#reminder-plan-month-title-' + month).html(F7.framework7App.dom('#reminder-plan-month-title-' + month).html() + ' ' + moment().format('YYYY'));
		}
	}

	/**
	 * Handle the back button event
	 * @method onBack
	 * @private
	 */
	private onBack = (): Promise<void> => {
		F7.framework7App.app.views.main.router.back('/', {animate: true});
		return;
	}
}
