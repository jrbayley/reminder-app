import { Dom7Instance } from 'dom7';

import { LoggedInUser } from '../../core/appData/LoggedInUser';
import { IMqttMessage } from '../../core/comms/mqtt/interfaces/IMqttMessage';
import { IMqttStatusUpdate } from '../../core/comms/mqtt/interfaces/IMqttStatusUpdate';
import { MqttMessageType } from '../../core/comms/mqtt/interfaces/MqttMessageType';
import { MqttStatus } from '../../core/comms/mqtt/interfaces/MqttStatus';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';

/**
 * AppLoginUi controls any UI aspects of login
 * @class
 */

export class HomeUI {

	/** @var {Dom7Instance} loggedInUser The logged in user data element */
	private loggedInUser : LoggedInUser;

	/** @var {Dom7Instance} loggedInUserText The logged in user text element */
	private loggedInUserText : Dom7Instance;

	/** @var {Dom7Instance} mqttStatus The mqtt status element */
	private mqttStatusDisplay : Dom7Instance;

	/** @var {Dom7Instance} mqttStatusText The mqtt status text element */
	private mqttStatusText : Dom7Instance;

	/**
	 * @constructor
	 * Register listener to capture user info for updating the screen.
	 */
	constructor () {
		this.onPageInit();
		Listeners.authenticationListener.userSignin.subscribe(this.onUserSignedIn);
		Listeners.mqttListener.mqttStatus.subscribe(this.onMqttStatus);
		Listeners.mqttListener.mqttMessageReceived.subscribe(this.onMessageReceived);
	}

	/**
	 * Initialise the page
	 * @method onPageInit
	 * @public
	 */
	public onPageInit = (): void => {
		this.loggedInUserText = F7.framework7App.dom('#loggedInUserText');
		this.mqttStatusDisplay = F7.framework7App.dom('#mqttStatus');
		this.mqttStatusText = F7.framework7App.dom('#mqttStatusText');
		this.onPageReinit();
	}

	/**
	 * Initialise the local pointers to form elements
	 * @method onPageReinit
	 * @private
	 */
	public onPageReinit = (): void => {
		this.updateUi();
	}

	/**
	 * Handle any tasks before the page transitions in
	 * @method onBeforePageIn
	 * @public
	 */
	public onBeforePageIn = (): void => {
		this.updateUi();
	}

	/**
	 * Handle any tasks after the page transitions in
	 * @method onAfterPageIn
	 * @public
	 */
	public onAfterPageIn = (): void => {
		//
	}

	/**
	 * Handle any tasks before the page transitions out
	 * @method onBeforePageOut
	 * @public
	 */
	public onBeforePageOut = (): void => {
		//
	}

	/**
	 * Handle any tasks after the page transitions out
	 * @method onAfterPageOut
	 * @public
	 */
	public onAfterPageOut = (): void => {
		//
	}

	/**
	 * Capture the logged in user details for screen updates
	 * @method onUserSignedIn
	 * @private
	 * @async
	 * @param {LoggedInUser} loggedInUser The current logged in user
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onUserSignedIn = (loggedInUser: LoggedInUser): Promise<void> => {
		this.loggedInUser = loggedInUser;
		this.updateUi();
		return;
	}

	/**
	 * The user is signed in so update the UI
	 * @method updateUi
	 * @private
	 * @async
	 */
	public updateUi = (): void => {
		this.updateUiWithUserdata();
		this.updateUiForSignOut();
	}

	/**
	 * The user is signed in so update the UI to customise and personalise
	 * @method updateUiWithUserdata
	 * @private
	 * @async
	 */
	private updateUiWithUserdata = (): void => {
		if (this.loggedInUser) {
			this.loggedInUserText.html(this.loggedInUser.username);
		}
		return;
	}

	/**
	 * The user is signed in so update the UI to customise and personalise
	 * @method updateUiWithUserdata
	 * @private
	 * @async
	 */
	private updateUiWithMqttStatus = (mqttStatusUpdate: IMqttStatusUpdate): void => {
		this.mqttStatusText.html(mqttStatusUpdate.status);
		if (mqttStatusUpdate.status === MqttStatus.ONLINE) {
			this.mqttStatusDisplay.removeClass('far fa-circle text-color-gray');
			this.mqttStatusDisplay.addClass('fas fa-globe-americas text-color-green');
		} else {
			this.mqttStatusDisplay.removeClass('fas fa-globe-americas text-color-green');
			this.mqttStatusDisplay.addClass('far fa-circle text-color-gray');
		}
	}
	/**
	 * Update UI components with the logout funciton
	 * @method updateUiForSignOut
	 * @private
	 */
	private updateUiForSignOut = (): void => {
		F7.framework7App.dom('#signOutUser').off('click', undefined).on('click', () => {
			this.confirmSignOut();
		});
		F7.framework7App.dom('#menuSignOut').off('click', undefined).on('click', () => {
			this.confirmSignOut();
		});
	}

	/**
	 * The check to see if the user really wants to log out
	 * @method confirmSignOut
	 * @private
	 */
	private confirmSignOut = (): void => {
  		F7.framework7App.app.dialog.confirm('Really Sign Out?', () => {
			Listeners.authenticationListener.userSignout.publish();
  		});
	}

	private onMqttStatus = (mqttStatusUpdate: IMqttStatusUpdate): Promise<void> => {
		this.updateUiWithMqttStatus(mqttStatusUpdate);
		return;
	}

	private onMessageReceived = (message: IMqttMessage): Promise<void> => {
		if (message.mqttPayload.messageType === MqttMessageType.MESSAGE) {
			return this.onNotificationRequested(message);
		}
	}

	private onNotificationRequested = (message: IMqttMessage): Promise<void> => {
		console.log('MESSAGE')
		const notificationFull = F7.framework7App.app.notification.create({
			icon: '<i class="icon demo-icon">7</i>',
			title: 'Framework7',
			titleRightText: 'now',
			subtitle: 'This is a subtitle',
			text: 'This is a simple notification message',
			closeTimeout: 3000,
		  });
		  notificationFull.open();
		return;
	}
}