import { Dom7Instance } from 'dom7';

import { Comms } from '../../core/comms/Comms';
import { Configuration } from '../../core/config/Configuration';
import { F7 } from '../../core/framework/F7';
import { StringUtils } from '../../core/utils/StringUtils';
import { IAccountResponse } from '../../reminder/user/IAccountResponse';
import { UIToast } from '../utils/UIToast';
import { UIValidator } from '../utils/UIValidator';
import { IPasswordResetRequest } from './IPasswordResetRequest';

/**
 * The Recovery of lost password management class
 * @class
 */

export class AppRecoveryUi {

	/** @var {Dom7Instance} recoveryButton The recovery send button */
	private recoveryStartButton : Dom7Instance;

	/** @var {Dom7Instance} recoveryCloseButton The close recovery panel button */
	private recoveryCloseButton : Dom7Instance;

	/** @var {Dom7Instance} recoveryCompleteButton The complete recovery button */
	private recoveryCompleteButton : Dom7Instance;

	/** @var {Dom7Instance} haveRecoveryCodeButton The already have a recovery code button */
	private haveRecoveryCodeButton : Dom7Instance;

	/** @var {Dom7Instance} recoveryEmail The recovery email field */
	private recoveryEmail : Dom7Instance;

	/** @var {Dom7Instance} recoveryPasswordField The recovery password field */
	private recoveryPasswordField : Dom7Instance;

	/** @var {Dom7Instance} confirmationPasswordField The confirmation password field */
	private confirmationPasswordField : Dom7Instance;

	/** @var {Dom7Instance} confirmationCodeField The recovery confirmation code */
	private confirmationCodeField : Dom7Instance;

	/** @var {Dom7Instance} passwordRecoveryPartOne The recovery process part one */
	private passwordRecoveryPartOne : Dom7Instance;

	/** @var {Dom7Instance} passwordRecoveryPartTwo The recovery process part two */
	private passwordRecoveryPartTwo : Dom7Instance;

	/** @var {string} passwordRecoveryPanel The recovery panel id */
	private passwordRecoveryPanel : string;

	/**
	 * @constructor
	 */
	constructor() {
		this.initialiseUi();
		this.initialiseRecoveryForm();
	}

	/**
	 * Initialise the ui fields and buttons.
	 * @method initialiseUi
	 * @private
	 */
	private initialiseUi = (): void => {
		this. recoveryStartButton = F7.framework7App.dom('#recovery-start-button');
		this. recoveryCloseButton = F7.framework7App.dom('#recovery-close-button');
		this. recoveryCompleteButton = F7.framework7App.dom('#recovery-complete-button');
		this. haveRecoveryCodeButton = F7.framework7App.dom('#have-recovery-code');
		this.recoveryEmail = F7.framework7App.dom('#recovery-email');
		this.confirmationCodeField = F7.framework7App.dom('#confirmation-code');
		this.recoveryPasswordField = F7.framework7App.dom('#recovery-password');
		this.confirmationPasswordField = F7.framework7App.dom('#confirm-password');
		this.passwordRecoveryPanel = '#password-recovery-screen';
		this.passwordRecoveryPartOne = F7.framework7App.dom('#recovery-part-one');
		this.passwordRecoveryPartTwo = F7.framework7App.dom('#recovery-part-two');
	}

	/**
	 * Initialise the recovery form activating buttons and initialising the view.
	 * @method initialiseRecoveryForm
	 * @public
	 */
	public initialiseRecoveryForm = (): void => {
		this.recoveryStartButton.off('click', undefined).on('click', () => {
			this.recoverPassword();
		});
		this.recoveryCloseButton.off('click', undefined).on('click', () => {
			this.closeRecoveryPanel();
		});
		this.recoveryCompleteButton.off('click', undefined).on('click', () => {
			this.checkRecoveryForm();
		});
		this.haveRecoveryCodeButton.off('click', undefined).on('click', () => {
			this.checkEmailAndInitPartTwo();
		});
		this.initialisePartOne();
		this.openRecoveryPanel();
	}

	/**
	 * Open the part one form and close part two
	 * @method initialisePartOne
	 * @private
	 */
	private initialisePartOne = (): void => {
		this.passwordRecoveryPartOne.show();
		this.passwordRecoveryPartTwo.hide();
	}

	/**
	 * Close the part one field and open part two to reset the password
	 * @method
	 * @private
	 */
	private initialisePartTwo = (): void => {
		this.passwordRecoveryPartOne.hide();
		this.passwordRecoveryPartTwo.show();
	}

	/**
	 * Check there is an email that we can use for part two
	 * @method
	 * @private
	 */
	private checkEmailAndInitPartTwo = (): void => {
		const email = this.recoveryEmail.val();
		if (StringUtils.isValidEmail(email)) {
			this.initialisePartTwo();
		} else {
			UIToast.displayCloseableToastWithIcon('fa-times', 'You must enter the email address co continue');
		}
	}

	/**
	 * Close the recovery panel
	 * @method openRecoveryPanel
	 * @private
	 */
	private openRecoveryPanel = (): void => {
		F7.framework7App.app.popup.open(this.passwordRecoveryPanel, true);
	}

	/**
	 * Close the recovery panel
	 * @method closeRecoveryPanel
	 * @private
	 */
	private closeRecoveryPanel = (): void => {
		F7.framework7App.app.popup.close(this.passwordRecoveryPanel, true);
	}

	/**
	 * Start the recovery process
	 * @method recoverPassword
	 * @private
	 */
	private recoverPassword = (): void => {
		const username = this.recoveryEmail.val();
		if (StringUtils.isValidEmail(username)) {
			Comms.restRequest.restGet(Configuration.REST_HOST + Configuration.REST_PASSWORD_RESET_EMAIL + '?email=' + username, undefined);
			this.startRecoveryStageTwo();
		} else {
			UIToast.displayCloseableToastWithIconAndTimeout('fa-times', 'You must enter a valid email address for recovery', 3000);
		}
	}

	/**
	 * Start second stage of the recovery process
	 * @method startRecoveryStageTwo
	 * @private
	 */
	private startRecoveryStageTwo = (): void => {
		UIToast.displayCloseableToastWithIconAndTimeout('fa-envelope-open', 'We have emailed a reset code. Please check your inbox', 3000);
		this.initialisePartTwo();
	}
	/**
	 * Start the recovery process
	 * @method checkRecoveryForm
	 * @private
	 */
	private checkRecoveryForm = (): void => {
		const email = this.recoveryEmail.val();
		const confirmationCode = this.confirmationCodeField.val();
		const recoveryPassword = this.recoveryPasswordField.val();
		const confirmationPassword = this.confirmationPasswordField.val();
		let confirmationCodeValid = false;
		if (confirmationCode.length === 6) {
			confirmationCodeValid = (StringUtils.containsNumbersOnly(confirmationCode));
			UIValidator.inputValidator(this.confirmationCodeField, 'The code contains numbers only', confirmationCodeValid);
		} else {
			confirmationCodeValid = false;
			UIValidator.inputValidator(this.confirmationCodeField, 'The code contains 6 numbers', false);
		}

		const recoveryPasswordValid = StringUtils.stringLengthMoreThan(recoveryPassword, 4);
		UIValidator.inputValidator(this.recoveryPasswordField, 'Password must be 4 or more characters', recoveryPasswordValid);
		const confirmationPasswordValid = (StringUtils.stringLengthMoreThan(confirmationPassword, 4) && recoveryPassword === confirmationPassword);
		UIValidator.inputValidator(this.confirmationPasswordField, 'The passwords do not match', confirmationPasswordValid);

		if (recoveryPasswordValid && confirmationPasswordValid && confirmationCodeValid) {
			const resetPasswordRequest: IPasswordResetRequest = { email: email, password: recoveryPassword, confirmPasword: confirmationPassword, confirmationCode: confirmationCode};
			Comms.restRequest.restPost(Configuration.REST_HOST + Configuration.REST_PASSWORD_RESET, resetPasswordRequest)
			.then((accountResponse: IAccountResponse) => {
				if (accountResponse.result) {
					UIToast.displayCloseableToastWithIconAndTimeout('fa-check', 'Your password was reset', 1500);
				} else {
					UIToast.displayCloseableToastWithIconAndTimeout('fa-times', accountResponse.message, 1500);
				}
				this.closeRecoveryPanel();
			});
		}
	}
}