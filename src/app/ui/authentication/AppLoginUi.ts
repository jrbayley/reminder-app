import { Dom7Instance } from 'dom7';

import { AppData } from '../../core/appData/AppData';
import { Authentication } from '../../core/authentication/Authentication';
import { SigninStage } from '../../core/authentication/models/SigninStage';
import { F7 } from '../../core/framework/F7';
import { Listeners } from '../../core/listener/Listeners';
import { Logger } from '../../core/logger/Logger';
import { SplashScreen } from '../Screens/SplashScreen';
import { UiDialog } from '../utils/UiDialog';
import { UIToast } from '../utils/UIToast';
import { AppRecoveryUi } from './AppRecoveryUi';

/**
 * AppLoginUi controls any UI aspects of login
 * @class
 */

export class AppLoginUi {

	/** @var {AppRecoveryUi} appRecoveryUi The button to start signin */
	private appRecoveryUi : AppRecoveryUi;

	/** @var {() => void} signoutFunction The stored signout function for access via modals */
	public signOutFunction : () => void;

	/** @var {Dom7Instance} signinEmail The signin email element */
	private signinEmail : Dom7Instance;

	/** @var {Dom7Instance} signinPassword The signin password element */
	private signinPassword : Dom7Instance;

	/** @var {Dom7Instance} signinConfirmationCode The signin confirmation code element */
	private signinConfirmationCode : Dom7Instance;

	/** @var {Dom7Instance} loggedInUserText The logged in user text element */
	private loggedInUserText : Dom7Instance;

	/** @var {string} signinScreen The signin screen id */
	private signinScreen : string;

	/** @var {string} signingInScreen The signing in screen id */
	private signingInScreen : string;

	/** @var {Dom7Instance} startRecoveryButton The button to start the lost password recovery process */
	private openRecoveryButton : Dom7Instance;

	/** @var {Dom7Instance} signinButton The button to start signin */
	private signinButton : Dom7Instance;

	/** @var {Dom7Instance} startRegistrationButton The button to start registration */
	private startRegistrationButton : Dom7Instance;

	/**
	 * @constructor
	 * Start the login form initialisation and subscribe to listeners.
	 */
	constructor () {
		this.initialiseLoginElements();
		Listeners.authenticationListener.signinStage.subscribe(this.onSigninChange);
	}

	/**
	 * Initialise the local pointers to form elements
	 * @method initialiseLoginElements
	 * @private
	 */
	private initialiseLoginElements = (): void => {
		this.signinEmail = F7.framework7App.dom('#signin-email');
		this.signinPassword = F7.framework7App.dom('#signin-password');
		this.signinConfirmationCode = F7.framework7App.dom('#signin-confirmation');
		this.signinScreen = '#app-login-screen';
		this.signingInScreen = '#app-signing-in-screen';
		this.loggedInUserText = F7.framework7App.dom('#loggedInUserText');
		this.openRecoveryButton =  F7.framework7App.dom('#open-recovery-button');
		this.signinButton =  F7.framework7App.dom('#signin-button');
		this.startRegistrationButton =  F7.framework7App.dom('#open-registration-button');
	}

	/**
	 * Track changes to the signin process and update the UI accordingly
	 * @method onSigninChange
	 * @private
	 * @async
	 * @param {SigninStage} signinStage The current signin stage.
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onSigninChange = (signinStage: SigninStage): Promise<void> => {
		switch (signinStage) {
			case SigninStage.START :
				this.onSigninStart();
				break;
			case SigninStage.LOGIN_FROM_FORM :
				this.onLoginFromForm();
				break;
			case SigninStage.LOGIN_FROM_BIO :
				this.onLoginFromBio();
				break;
			case SigninStage.COMPLETE :
				this.onSigninComplete();
				break;
			case SigninStage.RECOVER :
				this.onRecovery();
				break;
			case SigninStage.FAILED :
				this.signinFailed();
				break;
			case SigninStage.REGISTER :
				this.onRegistration();
				break;
			case SigninStage.VERIFY :
				this.onVerify();
				break;
		}
		return;
	}

	/**
	 * Start the sign fail
	 * @method
	 * @private
	 */
	private signinFailed = (): void => {
		Listeners.authenticationListener.signinStage.publish(SigninStage.START);
		UiDialog.dialogAlert('Invalid email or password', 'Signin');
	}

	/**
	 * Handle on registration event.
	 * @method onRegistration
	 * @private
	 */
	private onRegistration = (): void => {
		this.closeLoginPanel();
	}

	/**
	 * Update the UI when the signin starts.
	 * @method onSigninStart
	 * @private
	 */
	private onSigninStart = (): void => {
		Logger.silly('AppLoginUi.onSigninStart : Initialising');
		this.closeSigningInPanel();
		this.openLoginPanel();
		this.initialiseButtons();
	}

	/**
	 * Update the UI when the signin starts from form.
	 * @method onLoginFromForm
	 * @private
	 */
	private onLoginFromForm = (): void => {
		Logger.silly('AppLoginUi.onLoginFromForm : Starting form login');
		this.closeLoginPanel();
		this.openSigningInPanel();
	}

	private onVerify = (): void => {
		UIToast.displayCloseableToastWithTimeout('Enter the verification code to enable your account', 1800);
		F7.framework7App.dom('.item-confirmation').removeClass('display-none');
	}

	/**
	 * Update the UI when the signin starts from biometrics.
	 * @method onLoginFromBio
	 * @private
	 */
	private onLoginFromBio = (): void => {
		Logger.silly('AppLoginUi.onLoginFromBio : Starting form bio');
		this.closeLoginPanel();
		this.openSigningInPanel();
	}

	/**
	 * Update the UI when the signin is completed and attach the logout listener.
	 * @method onSigninComplete
	 * @private
	 */
	private onSigninComplete = (): void => {
		Logger.silly('AppLoginUi.onSigninComplete : Signin complete');
		this.closeLoginPanel();
		this.closeSigningInPanel();
		F7.framework7App.dom('.item-confirmation').removeClass('display-none');
		Listeners.authenticationListener.userSignout.subscribe(this.onSignout);
	}

	/**
	 * Initialise the buttons and controls then open the login panel
	 * @method initialiseButtons
	 * @private
	 */
	private initialiseButtons = (): void => {
		this.signinButton.off('click', undefined).on('click', () => {
			this.signinPreCheck();
		});
		this.openRecoveryButton.off('click', undefined).on('click', () => {
			this.startRecovery();
		});
		this.startRegistrationButton.off('click', undefined).on('click', () => {
			this.startRegistration();
		});
		this.signinPassword.off('keyup', undefined).on('keyup', this.checkForEnterKey);
		this.openLoginPanel();
	}

	/**
	 * Start the registration process by displaying the registration form
	 * @method startRegistration
	 * @private
	 */
	private startRegistration = (): void => {
		F7.framework7App.app.views.main.router.navigate('/registration/', {animate: false, history: false});
		Listeners.authenticationListener.signinStage.publish(SigninStage.REGISTER);
	}

	/**
	 * Open the signin popup panel
	 * @method openLoginPanel
	 * @private
	 */
	private openLoginPanel = (): void => {
		this.populateUsername();
		F7.framework7App.app.loginScreen.open(this.signinScreen, true);
		SplashScreen.clearSplashScreen();
	}

	/**
	 * Close the signin popup
	 * @method closeLoginPanel
	 * @private
	 */
	private closeLoginPanel = (): void => {
		F7.framework7App.app.loginScreen.close(this.signinScreen, true);
	}

	/**
	 * Populate the ui username text item
	 * @method populateUsername
	 * @private
	 */
	private populateUsername = (): void => {
		const loggedInUser = AppData.currentUser.getLoggedInUser();
		if (loggedInUser) {
			this.signinEmail.val(loggedInUser.username);
		}
	}

	/**
	 * Open the signing in splash screen for visual feedback
	 * @method openSigningInPanel
	 * @private
	 */
	private openSigningInPanel = (): void => {
		F7.framework7App.app.loginScreen.open(this.signingInScreen, true);
	}

	/**
	 * Close the signing in splash panel
	 * @method closeSigningInPanel
	 * @private
	 */
	private closeSigningInPanel = (): void => {
		F7.framework7App.app.loginScreen.close(this.signingInScreen, true);
	}

	/**
	 * Open the recovery popup and initialise recovery components
	 * @method onRecovery
	 * @private
	 */
	private onRecovery = (): void => {
		if (! this.appRecoveryUi) {
			this.appRecoveryUi = new AppRecoveryUi();
		}
		this.appRecoveryUi.initialiseRecoveryForm();
	}

	/**
	 * Fire the listener to start recoveru UI changes
	 * @method startRecovery
	 * @private
	 */
	private startRecovery = (): void => {
		Listeners.authenticationListener.signinStage.publish(SigninStage.RECOVER);
	}

	/**
	 * The check that the necessary items are entered to perform a login
	 * @method signinPreCheck
	 * @private
	 */
	private signinPreCheck = (): void => {
		const username: string = this.signinEmail.val().toString().toLowerCase();
		const password: string = this.signinPassword.val();
		const confirmationCode: string = this.signinConfirmationCode.val();
		const userCheck: boolean = (username && username.length > 2);
		const passCheck: boolean = (password && password.length > 2);
		if (userCheck && passCheck) {
			Authentication.appLogin.signin(username, password, confirmationCode);
		} else {
			if (! userCheck) {
				UIToast.displayCloseableToastWithIconAndTimeout('fa-times', 'You must enter a username', 3000);
			} else if (!passCheck) {
				UIToast.displayCloseableToastWithIconAndTimeout('fa-times', 'You must enter a password', 3000);
			}
		}
	}

	/**
	 * Test for enter key to trigger signin
	 * @method checkForEnterKey
	 * @private
	 * @param {KeyboardEvent} event The event to check.
	 */
	private checkForEnterKey = (event: KeyboardEvent): void => {
		if (event.keyCode === 13) {
			this.signinButton.click();
		}
	}

	/**
	 * A signout has been fired so reset the UI for login
	 * @method onSignout
	 * @private
	 * @async
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onSignout = (): Promise<void> => {
		Listeners.authenticationListener.signinStage.publish(SigninStage.START);
		return;
	}
}