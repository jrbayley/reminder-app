import { F7 } from '../../core/framework/F7';

/**
 * @class
 * Notification generation utility class
 */

export class UiNotification {

	/**
	 * Create a notification with a close button
	 * @method notificationWithClose
	 * @static
	 * @param {string} title The notification title
	 * @param {string} message The notification message
	 */
	public static notificationWithClose = (title: string, message: string): void => {
		const notificationClickToClose = F7.framework7App.app.notification.create({
			icon: '<i class="fa fa-cloud"></i>',
			title: title,
			titleRightText: 'now',
			subtitle: '',
			text: message,
			closeOnClick: true,
			closeTimeout: 3000,
		});
		notificationClickToClose.open();
	}
}