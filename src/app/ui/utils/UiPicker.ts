import { F7 } from '../../core/framework/F7';

/**
 * Date / Time picker generation utility class
 * @class
 */

export class UiPicker {

	/**
	 * Generate a time picker with hours and minutes selections
	 * @method timePicker
	 * @public
	 * @static
	 * @param {string} element The element selector to link the timepicker to
	 */
	public static timePicker = (element: string): void => {
		F7.framework7App.app.picker.create({
			inputEl: element,
			rotateEffect: true,
			cols: [
					{
						textAlign: 'left',
						values: ('00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23').split(' ')
					},
					{
						values: ('00 05 10 15 20 25 30 35 40 45 50 55').split(' ')
					},
			  	]
		});
	}

	/**
	 * Generate a calendar date picker
	 * @method calendar
	 * @public
	 * @static
	 * @param {string} element The element selector to link the calendar to
	 */
	public static calendar = (element: string): void => {
		F7.framework7App.app.calendar.create({
			inputEl: element,
			timePicker: false,
			dateFormat: 'dd-mm-yyyy',
			closeOnSelect: true,
			monthSelector: true,
			monthPicker: true,
			yearSelector:	true,
			yearPicker: true
		});
	}

	/**
	 * Generate a calendar date picker with initial dates set
	 * @method calendarWithDate
	 * @public
	 * @static
	 * @param {string} element The element selector to link the calendar to
	 * @param {Date[]} selectedDates The dates to set in the picker
	 */
	public static calendarWithDate = (element: string, selectedDates: Date[]): void => {
		F7.framework7App.app.calendar.create({
			inputEl: element,
			timePicker: false,
			dateFormat: 'dd-mm-yyyy',
			closeOnSelect: true,
			monthSelector: true,
			monthPicker: true,
			yearSelector:	true,
			yearPicker: true,
			value: selectedDates
		});
	}

	/**
	 * Generate a calendar date picker with an event on selection
	 * @method calendarWithEvent
	 * @public
	 * @static
	 * @param {string} element The element selector to link the calendar to
	 * @param {() => void} event The event to fire when the picker is changed
	 */
	public static calendarWithEvent = (element: string, event: () => void): void => {
		F7.framework7App.app.calendar.create({
			inputEl: element,
			timePicker: false,
			dateFormat: 'dd-mm-yyyy',
			closeOnSelect: true,
			monthSelector: true,
			monthPicker: true,
			yearSelector:	true,
			yearPicker: true,
		}).on('change', event);
	}
}