import { F7 } from '../../core/framework/F7';

/**
 * Utility class for creating UiDialog controls
 * @class
 */

export class UiDialog {

	/**
	 * Generate a dialog of type alert
	 * @method dialogAlert
	 * @static
	 * @private
	 * @param {string} message The alert message
	 * @param {string} title The optional alert title
	 * @param {() => void} callback THe callback function to call after the dialog is closed
	 */
	public static dialogAlert = (message: string, title?: string,  callback?: () => void): void => {
		F7.framework7App.app.dialog.alert(message, title, callback);
	}

	/**
	 * Generate a dialog of type confirm with confirm / cancel buttons
	 * @method dialogConfirm
	 * @static
	 * @private
	 * @param {string} message The alert message
	 * @param {string} title The optional alert title
	 * @param {() => void} callbackOk THe callback function to call when the OK is clicked
	 * @param {() => void} callbackCancel THe callback function to call when the cancel is clicked
	 */
	public static dialogConfirm = (message: string, title?: string,  callbackOk?: () => void,  callbackCancel?: () => void): void => {
		F7.framework7App.app.dialog.confirm(message, title, callbackOk, callbackCancel);
	}
}