import { F7 } from '../../core/framework/F7';

/**
 * Utility class for displaying popup toast notifications.
 * @class
 */

export class UIToast {

	/**
	 * Create a toast and display it for a set time.
	 * @method displayToastWithTimeout
	 * @public
	 * @static
	 * @param {string} message The toast message
	 * @param {string} timeout The toast display time in ms
	 */
	public static displayToastWithTimeout = (message: string, timeout: number): void => {
		F7.framework7App.app.toast.create({
			text: message,
			position: 'center',
			closeTimeout: timeout
		}).open();
	}

	/**
	 * Create a toast with a close button and display it for a set time or until closed.
	 * @method displayCloseableToastWithTimeout
	 * @public
	 * @static
	 * @param {string} message The toast message
	 * @param {string} timeout The toast display time in ms
	 */
	public static displayCloseableToastWithTimeout = (message: string, timeout: number): void => {
		F7.framework7App.app.toast.create({
			text: message,
			position: 'center',
			closeButton: true,
			closeTimeout: timeout,
		}).open();
	}

	/**
	 * Create a toast with a close button, icon and display it for a set time or until closed.
	 * @method displayCloseableToastWithIcon
	 * @public
	 * @static
	 * @param {string} icon The toast font aweseom icon class
	 * @param {string} message The toast message
	 */
	public static displayCloseableToastWithIcon = (icon: string, message: string): void => {
		F7.framework7App.app.toast.create({
			icon: F7.framework7App.app.theme === 'ios' ? '<i class="fa fa-2x ' + icon + '"></i>' : '<i class="fa fa-2x ' + icon + '"></i>',
			text: message,
			position: 'center',
			closeButton: true
		}).open();
	}

	/**
	 * Create a toast with an icon and display it for a set time or until closed.
	 * @method displayToastWithIconAndTimeout
	 * @public
	 * @static
	 * @param {string} icon The toast font aweseom icon class
	 * @param {string} message The toast message
	 * @param {string} timeout The toast display time in ms
	 */
	public static displayToastWithIconAndTimeout = (icon: string, message: string, timeout: number): void => {
		F7.framework7App.app.toast.create({
			icon: F7.framework7App.app.theme === 'ios' ? '<i class="fa fa-2x ' + icon + '"></i>' : '<i class="fa fa-2x ' + icon + '"></i>',
			text: message,
			position: 'center',
			closeTimeout: timeout,
		}).open();
	}

	/**
	 * Create a toast with a close button, icon and display it for a set time or until closed.
	 * @method displayCloseableToastWithIconAndTimeout
	 * @public
	 * @static
	 * @param {string} icon The toast font aweseom icon class
	 * @param {string} message The toast message
	 * @param {string} timeout The toast display time in ms
	 */
	public static displayCloseableToastWithIconAndTimeout = (icon: string, message: string, timeout: number): void => {
		F7.framework7App.app.toast.create({
			icon: F7.framework7App.app.theme === 'ios' ? '<i class="fa fa-2x ' + icon + '"></i>' : '<i class="fa fa-2x ' + icon + '"></i>',
			text: message,
			position: 'center',
			closeButton: true,
			closeTimeout: timeout,
		}).open();
	}
}