import { Dom7Instance } from 'dom7';

import { F7 } from '../../core/framework/F7';

/**
 * Utility class for handling UI validation
 * @class
 */

export class UIValidator {

	/**
	 * This method is used to trigger a manual run of built in validation on an element
	 * @method validationFeedback
	 * @public
	 * @static
	 * @param {Dom7Instance} element The element to trigger validation on
	 */
	public static validationFeedback = (element: Dom7Instance, valid: boolean): void => {
		if (valid) {
			element.addClass('text-color-green');
			element.parent().siblings('.item-title').addClass('text-color-green');
			element.siblings('.input-feedback').removeClass('input-feedback-required');
			element.siblings('.input-feedback').addClass('input-feedback-completed');
			element.siblings('.input-feedback').html('checkmark_alt');
		} else {
			element.removeClass('text-color-green');
			element.parent().siblings('.item-title').removeClass('text-color-green');
			element.siblings('.input-feedback').addClass('input-feedback-required');
			element.siblings('.input-feedback').removeClass('input-feedback-completed');
			element.siblings('.input-feedback').html('chevron_left');
		}
	}


	/**
	 * This method is used to trigger a manual run of built in validation on an element
	 * @method manualValidate
	 * @public
	 * @static
	 * @param {Dom7Instance} element The element to trigger validation on
	 */
	public static manualValidate = (element: Dom7Instance): void => {
		F7.framework7App.app.input.validate('#' + element.attr('id'));
	}


	/**
	 * This method is used as a custom validation generator to create or clear validation UI components from fields.
	 * @method inputValidator
	 * @public
	 * @static
	 * @param {Dom7Instance} element The element to attach validation UI components to.
	 * @param {string} message The message string to embed in the form validator
	 * @param {boolean} valid The validation set or clear binary flag
	 */
	public static inputValidator = (element: Dom7Instance, message: string, valid: boolean): void => {
		const parent: Dom7Instance = element.parents('.item-input-wrap');
		if (valid) {
			element.removeClass('input-invalid');
			parent.removeClass('item-input-with-error-message');
			parent.removeClass('item-input-invalid');
			element.next('.item-input-error-message').remove();
		} else {
			element.addClass('input-invalid');
			parent.addClass('item-input-with-error-message item-input-invalid');
			if (element.next('.item-input-error-message').length === 0) {
				F7.framework7App.dom('<div class="item-input-error-message">' + message + '</div>').insertAfter(element);
			} else {
				element.next('.item-input-error-message').html(message);
			}
		}
	}
	/**
	 * This method is used as a custom validation generator to create or clear validation UI components from fields.
	 * @method checkboxValidator
	 * @public
	 * @static
	 * @param {Dom7Instance} element The element to attach validation UI components to.
	 * @param {string} message The message string to embed in the form validator
	 * @param {boolean} valid The validation set or clear binary flag
	 */
	public static checkboxValidator = (element: Dom7Instance, message: string, valid: boolean): void => {
		const parent: Dom7Instance = element.parents('.checkbox-content');
		if (valid) {
			element.removeClass('input-invalid');
			parent.removeClass('item-input-with-error-message');
			parent.removeClass('item-input-invalid');
			parent.next('.item-input-error-message').remove();
		} else {
			element.addClass('input-invalid');
			parent.addClass('item-input-with-error-message item-input-invalid');
			parent.children('.item-input-error-message').remove();
			parent.append(F7.framework7App.dom('<div class="item-input-error-message">' + message + '</div>'));
		}
	}
}