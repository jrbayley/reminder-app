import { Dom7Instance } from 'dom7';

/**
 * Utility class for manipulating UI elements
 * @class
 */

export class UiUtils {

	/**
	 * This method is used to enable or disable a button based on the check
	 * @method buttonEnableDisable
	 * @public
	 * @static
	 * @param {Dom7Instance} element The element to trigger validation on
	 * @param {boolean} valid The check flag to enable or disable
	 * @param {()=> void)} validFunction The function to attach to the button if the valid is true
	 */
	public static buttonEnableDisable = (element: Dom7Instance, valid: boolean, validFunction: () => void): void => {
		UiUtils.buttonEnableDisableWithColour(element, valid, validFunction, 'color-green', 'color-gray');
	}

	/**
	 * This method is used to enable or disable a button based on the check
	 * @method buttonEnableDisableWithColour
	 * @public
	 * @static
	 * @param {Dom7Instance} element The element to trigger validation on
	 * @param {boolean} valid The check flag to enable or disable
	 * @param {()=> void)} validFunction The function to attach to the button if the valid is true
	 * @param {string} colourClass The class to add to the element if valid
	 */
	public static buttonEnableDisableWithColour = (element: Dom7Instance, valid: boolean, validFunction: () => void, validColourClass: string, invalidColourClass: string): void => {
		if (valid) {
			element.removeClass(invalidColourClass);
			element.addClass(validColourClass);
			element.off('click', undefined).on('click', validFunction);
		} else {
			element.addClass(invalidColourClass);
			element.removeClass(validColourClass);
			element.off('click', undefined)
		}
	}
}