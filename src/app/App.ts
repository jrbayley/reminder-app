import { AppLoginUi } from './ui/authentication/AppLoginUi';
import { AboutUI } from './ui/Screens/AboutUI';
import { AccountUI } from './ui/Screens/AccountUI';
import { AddReminderUI } from './ui/Screens/AddReminderUI';
import { CalendarUI } from './ui/Screens/CalendarUI';
import { ChatWithUI } from './ui/Screens/ChatWithUI';
import { HomeUI } from './ui/Screens/HomeUI';
import { OnlineUI } from './ui/Screens/OnlineUI';
import { RegistrationUI } from './ui/Screens/RegistrationUI';
import { ReminderPlanUI } from './ui/Screens/ReminderPlanUI';
import { RemindersUI } from './ui/Screens/RemindersUI';
import { ReminderUI } from './ui/Screens/ReminderUI';
import { SettingsUI } from './ui/Screens/SettingsUI';

/**
 * The UI interface management class
 * @class
 */

export class App {

	/** @var {AppLoginUi} appLoginUi the login ui class instance */
	public static appLoginUi : AppLoginUi;

	/** @var {AboutUI} aboutUi The about user interface */
	public static aboutUi : AboutUI;

	/** @var {AccountUI} accountUi The account user interface */
	public static accountUi : AccountUI;

	/** @var {AddReminderUI} reminderUi The add reminder user interface */
	public static addReminderUi : AddReminderUI;

	/** @var {CalendarUI} reminderUi The calendar user interface */
	public static calendarUi : CalendarUI;

	/** @var {HomeUI} homeUi The home user interface */
	public static homeUi : HomeUI;

	/** @var {OnlineUI} onlineUi The online user interface */
	public static onlineUi : OnlineUI;

	/** @var {ChatWithUI} chatWithUi The messages user interface */
	public static chatWithUi : ChatWithUI;

	/** @var {RegistrationUI} registrationUi The reminders plan user interface */
	public static registrationUi : RegistrationUI;

	/** @var {ReminderPlanUI} remindersUi The reminders plan user interface */
	public static reminderPlanUi : ReminderPlanUI;

	/** @var {RemindersUI} remindersUi The reminders list user interface */
	public static remindersUi : RemindersUI;

	/** @var {ReminderUI} reminderUi The reminder view / edit user interface */
	public static reminderUi : ReminderUI;

	/** @var {SettingsUI} settingsUi The settings user interface */
	public static settingsUi : SettingsUI;


	/**
	 * Initialise the UI Controllers
	 * @method
	 * @public
	 * @static
	 * @returns {Promise<void>} An empty promise
	 */
	public static init = (): void => {
		App.appLoginUi = new AppLoginUi();
		App.registrationUi = new RegistrationUI();
		App.aboutUi = new AboutUI();
		App.accountUi = new AccountUI();
		App.addReminderUi = new AddReminderUI();
		App.calendarUi = new CalendarUI();
		App.homeUi = new HomeUI();
		App.onlineUi = new OnlineUI();
		App.chatWithUi = new ChatWithUI();
		App.reminderPlanUi = new ReminderPlanUI();
		App.remindersUi = new RemindersUI();
		App.reminderUi = new ReminderUI();
		App.settingsUi = new SettingsUI();
	}
}

