import { IReminderPlan } from '../../reminder/reminder/IReminderPlan';
import { Comms } from '../comms/Comms';
import { Configuration } from '../config/Configuration';
import { Listeners } from '../listener/Listeners';
import { Logger } from '../logger/Logger';

/**
 * Local reminders cache and reminder management.
 * @class
 */

export class ReminderPlans {

	/** @var {IReminder[]} reminders the cached reminders */
	private reminderPlans : IReminderPlan[];

	/**
	 * Initialise the local reminder cache
	 * @constructor
	 */
	constructor() {
		Listeners.authenticationListener.userSignin.subscribe(this.refreshReminderPlan);
	}

	/**
	 * get all the cached reminders
	 * @method
	 * @public
	 * @returns {IReminder[]} The reminders from cache
	 */
	public getReminderPlan = (): IReminderPlan[] => {
		return this.reminderPlans;
	}

	/**
	 * Refresh the local cache
	 * @method
	 * @public
	 * @async
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public refreshReminderPlan = async(): Promise<void> => {
		Logger.debug('Refresh reminder plans request');
		try {
			this.reminderPlans = await this.getReminderPlansFromServer();
			Listeners.reminderListener.reminderPlanChanged.publish();
		} catch (error) {
			//
		}
	}

	/**
	 * Refresh the cache from the backend server
	 * @method
	 * @private
	 * @async
	 * @returns {Promise<IReminderPlan[]>} An array of reminders
	 */
	private getReminderPlansFromServer = async (): Promise<IReminderPlan[]> => {
		try {
			const reminders = await Comms.restRequest.restSecureGet(Configuration.REST_HOST + Configuration.REST_REMINDER_PLAN);
			return reminders;
		}
		catch (_error) {
			return [];
		}
	}
}