import { Database } from '../database/Database';
import { Listeners } from '../listener/Listeners';

/**
 * The app settings and theme controls
 * @class
 */

export class AppSettings {

	/** @var {string} appTheme the app dark or light theme name */
	public appTheme : string;

	/** @var {string} appColour the app colour */
	public appColour : string;

	/**
	 * @constructor
	 */
	constructor() {
		this.getCurrentSettings();
	}

	/**
	 * Get the stored theme dark or light
	 * @method getTheme
	 * @public
	 * @returns {string} The stored theme name
	 */
	public getTheme = (): string => {
		return Database.localDb.getItem('appTheme') || 'theme-light';
	}

	/**
	 * Set the stored theme dark or light
	 * @method setTheme
	 * @public
	 * @param {string} theme The theme name to store
	 */
	public setTheme = (theme: string): void => {
		this.appTheme = theme;
		Database.localDb.setItem('appTheme', theme);
		Listeners.settingsListener.settingsThemeChanged.publish(theme);
	}

	/**
	 * @method
	 * @public
	 * Get the stored app colour
	 * @returns {string} The stored colour
	 */
	public getColour = (): string => {
		return Database.localDb.getItem('appColour') || 'color-theme-red';
	}

	/**
	 * Set the stored colour
	 * @method
	 * @public
	 * @param {string} colour The colour to store
	 */
	public setColour = (colour: string): void => {
		this.appColour = colour;
		Database.localDb.setItem('appColour', colour);
		Listeners.settingsListener.settingsColourChanged.publish(colour);
	}

	/**
	 * Refresh the UI by triggering the theme listeners.
	 * @method
	 * @public
	 */

	public refresh = (): void => {
		Listeners.settingsListener.settingsColourChanged.publish(this.appColour);
		Listeners.settingsListener.settingsThemeChanged.publish(this.appTheme);
	}

	/**
	 * Get all the stored colour and themes
	 * @method
	 * @public
	 */

	private getCurrentSettings = (): void => {
		this.appTheme = this.getTheme();
		this.appColour = this.getColour();
	}
}