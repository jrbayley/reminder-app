import { IUser } from '../authentication/models/IUser';

/**
 * The logged in use interface that is retrieved from the server on signin
 * @interface
 */

export interface ILoggedInUser {

	/** @var {string} username The username of the current user */
	username : string;

	/** @var {string} password The password of the current user */
	password : string;

		/** @var {string} token The jwt for secure access */
	token : string;

	/** @var {IUser} user The username of the current user */
	user : IUser;
}