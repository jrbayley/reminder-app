import _, { ListIterateeCustom } from 'lodash';
import moment from 'moment';

import { IAddReminder } from '../../reminder/reminder/IAddReminder';
import { IReminder } from '../../reminder/reminder/IReminder';
import { IReminderUpdate } from '../../reminder/reminder/IReminderUpdate';
import { ReminderInsertResponse } from '../../reminder/reminder/ReminderInsertResponse';
import { ReminderPredicate } from '../../reminder/reminder/ReminderPredicate';
import { ReminderPredicateFunction } from '../../reminder/reminder/ReminderPredicateFunction';
import { IDeleteResponse } from '../appData/IDeleteResponse';
import { Comms } from '../comms/Comms';
import { Configuration } from '../config/Configuration';
import { Listeners } from '../listener/Listeners';
import { Logger } from '../logger/Logger';
import { LoggedInUser } from './LoggedInUser';

/**
 * Local reminders cache and reminder management.
 * @class
 */

export class Reminders {

	/** @var {IReminder[]} reminders the cached reminders */
	private reminders : IReminder[];

	/**
	 * Initialise the local reminder cache
	 * @constructor
	 */
	constructor() {
		Listeners.authenticationListener.userSignin.subscribe(this.refreshReminders);
	}

	/**
	 * update the reminder in the cached reminders
	 * @method updateReminder
	 * @public
	 * @async
	 * @param {IReminder} reminder The updated reminder to save
	 */
	public updateReminder = async (reminderUpdate: IReminderUpdate): Promise<void> => {
		const reminder: IReminder =  await Comms.restRequest.restSecurePut(Configuration.REST_HOST + Configuration.REST_REMINDER, reminderUpdate);
		return this.refreshReminders();
	}

	/**
	 * insert the reminder in the cached reminders
	 * @method insertReminder
	 * @public
	 * @async
	 * @param {IReminder} reminder The updated reminder to save
	 */
	public insertReminder = (reminderUpdate: IAddReminder): Promise<ReminderInsertResponse> => {
		return Comms.restRequest.restSecurePost(Configuration.REST_HOST + Configuration.REST_REMINDER, reminderUpdate);
	}

	/**
	 * Refresh the local cache
	 * @method refreshReminders
	 * @public
	 * @async
	 */
	public refreshReminders = async(data?: LoggedInUser): Promise<void> => {
		Logger.debug('Reminders.refreshReminders : Starting refresh.');
		try {
			this.reminders = await this.getRemindersFromServer();
			Logger.debug('Reminders.refreshReminders : Received %d reminders.', this.reminders.length);
			Listeners.reminderListener.reminderChanged.publish(this.reminders);
		} catch (error) {
			//
		}
	}

	/**
	 * Refresh the cache from the backend server
	 * @method deleteReminderFromServer
	 * @private
	 * @async
	 * @returns {Promise<IDeleteResponse>} An array of reminders
	 */
	public deleteReminderFromServer = async (reminderId: number): Promise<IDeleteResponse> => {
		try {
			const response = await Comms.restRequest.restSecureDelete(Configuration.REST_HOST + Configuration.REST_REMINDER, reminderId);
			return response;
		}
		catch (_error) {
			return undefined;
		}
	}

	/**
	 * get all the cached reminders
	 * @method getReminders
	 * @public
	 * @returns {IReminder[]} The reminders from cache
	 */
	public getReminders = (): IReminder[] => {
		return this.reminders;
	}

	/**
	 * get a reminder by id
	 * @method getReminder
	 * @public
	 * @param {number} reminderId The reminder id to get
	 * @returns {IReminder} The reminders from cache
	 */
	public getReminder = (reminderId: number): IReminder => {
		return _.find(this.reminders, {reminder_id: reminderId});
	}

	/**
	 * getActiveReminders get all a users reminders
	 * @method getActiveReminders
	 * @public
	 * @static
	 * @returns {IReminder[]} returns an array of active reminder data or undefined if there is no match
	 */
	public getActiveReminders = (): IReminder[] => {
		const predicate: ReminderPredicateFunction = (reminder: IReminder): boolean => {
			return (moment(reminder.reminder_date) > moment() &&
					reminder.reminder_enabled === true &&
					reminder.reminder_deleted === false &&
					reminder.reminder_archived === false);
				};
		return this.filterReminders(predicate);
	}

	/**
	 * getActiveReminders get all a users reminders
	 * @method getPastReminders
	 * @public
	 * @static
	 * @returns {IReminder[]} returns an array of active reminder data or undefined if there is no match
	 */
	public getPastReminders = (): IReminder[] => {
		const predicate: ReminderPredicateFunction = (reminder: IReminder): boolean => {
			return (moment(reminder.reminder_date).isBefore(moment()) &&
					reminder.reminder_enabled === true &&
					reminder.reminder_deleted === false &&
					reminder.reminder_archived === false);
				};
		return this.filterReminders(predicate);
	}

	/**
	 * getArchivedReminders get all a users reminders
	 * @method getArchivedReminders
	 * @public
	 * @static
	 * @returns {IReminder[]} returns an array of archived reminder data or undefined if there is no match
	 */
	public getArchivedReminders = (): IReminder[] => {
		const predicate: ReminderPredicate = {reminder_archived: true};
		return this.filterReminders(predicate);
	}

	/**
	 * getArchivedReminders get all a users reminders
	 * @method getDisabledReminders
	 * @public
	 * @static
	 * @returns {IReminder[]} returns an array of disabled reminder data or undefined if there is no match
	 */
	public getDisabledReminders = (): IReminder[] => {
		const predicate: ReminderPredicate = {reminder_enabled: false};
		return this.filterReminders(predicate);
	}

	/**
	 * getArchivedReminders get all a users reminders
	 * @method getDeletedReminders
	 * @public
	 * @static
	 * @returns {IReminder[]} returns an array of archived reminder data or undefined if there is no match
	 */
	public getDeletedReminders = (): IReminder[] => {
		const predicate: ReminderPredicate = {reminder_deleted: true};
		return this.filterReminders(predicate);
	}

	/**
	 * @method filterReminders
	 * getEnabledReminders get all a users reminders
	 * @private
	 * @static
	 * @param {ReminderPredicate} predicate The predicate to filter reminders on
	 * @returns {IReminder[]} returns an array of archived reminder data or undefined if there is no match
	 */
	private filterReminders = (predicate: ListIterateeCustom<IReminder, boolean>): IReminder[] => {
		const reminders: IReminder[] = this.getReminders();
		return  _.filter(reminders, predicate);
	}

	/**
	 * Refresh the cache from the backend server
	 * @method getRemindersFromServer
	 * @private
	 * @returns {Promise<IReminder>} An array of reminders
	 */
	private getRemindersFromServer = async (): Promise<IReminder[]> => {
		Logger.debug('Refresh reminder request');
		try {
			const reminders = await Comms.restRequest.restSecureGet(Configuration.REST_HOST + Configuration.REST_REMINDERS);
			Logger.debug('Reminders.getRemindersFromServer : Received %d reminders', (reminders) ? reminders.length : 0);
			return reminders;
		}
		catch (error) {
			Logger.error('Reminders.getRemindersFromServer : Error getting reminders : %s', error.message);
			return [];
		}
	}
}