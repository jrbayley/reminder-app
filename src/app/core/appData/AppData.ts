import { AppSettings } from './AppSettings';
import { CurrentUser } from './CurrentUser';
import { ReminderPeriods } from './ReminderPeriods';
import { ReminderPlans } from './ReminderPlans';
import { Reminders } from './Reminders';

/**
 * Appdata class for instantiating and holding any local data caches
 * @class
 */

export class AppData {

	/** @var {CurrentUser} currentUser The current user data access and persistence */
	public static currentUser : CurrentUser;

	/** @var {ReminderPeriods} reminderPeriods The reminder periods data access and persistence */
	public static reminderPeriods : ReminderPeriods;

	/** @var {Reminders} reminders The current user reminder data access and persistence */
	public static reminders : Reminders;

	/** @var {ReminderPlan} reminders The current user reminder data access and persistence */
	public static reminderPlans : ReminderPlans;

	/** @var {AppSettings} appSettings The persisted app settings */
	public static appSettings : AppSettings;

	/**
	 * Initialise the local app data management classes
	 * @method init
	 * @public
	 * @static
	 */
	public static init = (): void => {
		AppData.currentUser = new CurrentUser();
		AppData.appSettings = new AppSettings();
		AppData.reminderPlans = new ReminderPlans();
		AppData.reminderPeriods = new ReminderPeriods();
		AppData.reminders = new Reminders();
	}
}