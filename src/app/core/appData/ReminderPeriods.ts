import _ from 'lodash';

import { IReminderPeriod } from '../../reminder/reminderPeriod/IReminderPeriod';
import { ReminderPeriodPredicate } from '../../reminder/reminderPeriod/ReminderPeriodPredicate';
import { Comms } from '../comms/Comms';
import { Configuration } from '../config/Configuration';
import { Listeners } from '../listener/Listeners';

/**
 * Local reminder period cache and management.
 * @class
 */

export class ReminderPeriods {

	/** @var {IReminderPeriod[]} reminderPeriods All reminder periods */
	private reminderPeriods : IReminderPeriod[];

	/**
	 * @constructor
	 */
	constructor() {
		Listeners.authenticationListener.userSignin.subscribe(this.refreshReminderPeriods);
	}

	/**
	 * Initialise the local reminder period cache
	 * @method
	 * @public
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public init = (): Promise<void> => {
		return this.refreshReminderPeriods();
	}

	/**
	 * get all the cached reminders
	 * @method
	 * @public
	 * @returns {IReminderPeriod[]} The reminders from cache
	 */
	public getReminderPeriods = (): IReminderPeriod[] => {
		return this.reminderPeriods;
	}

	/**
	 * get a reminder period by id
	 * @method
	 * @public
	 * @param {number} reminderId The reminder id to get
	 * @returns {IReminderPeriod} The reminder period from cache
	 */
	public getReminderPeriodByPeriod = (period: number): IReminderPeriod => {
		const reminderPredicate: ReminderPeriodPredicate = {period: period};
		return this.findReminderPeriods(reminderPredicate);
	}


	/**
	 * get a reminder period by code
	 * @method
	 * @public
	 * @param {string} periodCode The reminder code to get
	 * @returns {IReminderPeriod} The reminder period from cache
	 */
	public getReminderPeriodByCode = (periodCode: string): IReminderPeriod => {
		const reminderPredicate: ReminderPeriodPredicate = {periodCode: periodCode};
		return this.findReminderPeriods(reminderPredicate);
	}


	/**
	 * Refresh the local cache
	 * @method
	 * @public
	 * @async
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public refreshReminderPeriods = async(): Promise<void> => {
		this.reminderPeriods = await this.getReminderPeriodsFromServer();
	}

	/**
	 * Refresh the cache from the backend server
	 * @method getReminderPeriodsFromServer
	 * @private
	 * @async
	 * @returns {Promise<IReminderPeriod>} An array of reminders
	 */
	private getReminderPeriodsFromServer = async (): Promise<IReminderPeriod[]> => {
		try {
			const reminders = await Comms.restRequest.restSecureGet(Configuration.REST_HOST + Configuration.REST_REMINDER_PERIODS);
			return reminders;
		}
		catch (_error) {
			return [];
		}
	}

	/**
	 * getEnabledUserReminderPeriods get all a users reminders
	 * @method findReminderPeriods
	 * @private
	 * @static
	 * @param {ReminderPeriodPredicate} predicate The predicate to filter reminders on
	 * @returns {IReminderPeriod[]} returns an array of archived reminder data or undefined if there is no match
	 */
	private findReminderPeriods = (reminderPredicate: ReminderPeriodPredicate): IReminderPeriod => {
		return _.find(this.reminderPeriods, reminderPredicate);
	}
}