/**
 * The app version data that is populated bu cordova build
 * @class
 */

export class Version {

	/** @var {string} version The app verion number */
	public static version = 'version';

	/** @var {string} buildID The app build number */
	public static buildID = 'buildID';

	/** @var {string} buildTime The app build time */
	public static buildTime = 'buildTime';
}
