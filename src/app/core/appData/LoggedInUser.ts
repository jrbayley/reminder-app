import { IUser } from '../authentication/models/IUser';
import { ILoggedInUser } from './ILoggedInUser';

/**
 * Logged in user model for managing user data and validation.
 * @class
 * @implements ILoggedInUser
 */

export class LoggedInUser implements ILoggedInUser {

	/** @var {string} username The username of the current user */
	public username : string;

	/** @var {string} password The password of the current user */
	public password : string;

		/** @var {string} token The jwt for secure access */
	public token : string;

	/** @var {IUser} user The username of the current user */
	public user : IUser;

	/**
	 * @constructor
	 * @param {LoggedINUser} loggedInUser Initialise with a loggedin user interface
	 */
	constructor(loggedInUser: ILoggedInUser) {
		this.username = loggedInUser.username;
		this.password = loggedInUser.password;
		this.token = loggedInUser.token;
		this.user = loggedInUser.user;
	}

	/**
	 * Check to see if the usermodel is valid.
	 * @method
	 * @public
	 * @returns {boolean} true if the data is valid
	 */
	public valid = (): boolean => {
		const usernameOk: boolean = this.username !== undefined && this.username.length > 2;
		const passwordOk: boolean = this.password !== undefined && this.password.length > 3;
		return usernameOk && passwordOk;
	}
}