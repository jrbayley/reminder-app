/**
 * A delete response from the server
 * @interface
 */

export interface IDeleteResponse {
	
	/** @var {number} deleted The number of items deleted */
	deleted : number;
}