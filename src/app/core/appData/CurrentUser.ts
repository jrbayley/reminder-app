import { Database } from '../database/Database';
import { LoggedInUser } from './LoggedInUser';

/**
 * Class for managing the access to the local user data
 * @class
 */

export class CurrentUser {

	/** @var {LocalUser} currentUser The currently logged in user */
	private currentUser : LoggedInUser;

	/**
	 * populate the local user data.
	 * @constructor
	 */

	constructor () {
		this.retrieveCurrentUser();
	}

	/**
	 * retrieve the stored current user from the localstorage and store to local variable
	 * @method
	 * @private
	 */
	private retrieveCurrentUser = (): void => {
		const userString = Database.localDb.getItem('loggedInUser');
		if (userString && userString.length > 1) {
			this.currentUser = new LoggedInUser(JSON.parse(userString));
		}
	}

	/**
	 * retrieve the stored current user from the localstorage and store to local variable
	 * @method
	 * @private
	 * @returns {LoggedInUser | undefined} Returns the local current user if it is valid.
	 */
	public getLoggedInUser = (): LoggedInUser | undefined => {
		return (this.currentUser && this.currentUser.valid()) ? this.currentUser : undefined;
	}

	/**
	 * retrieve the stored current user from the localstorage and store to local variable
	 * @method
	 * @public
	 * @param {LoggedInUser | undefined} loggedInUser the logged in user.
	 */
	public setLoggedInUser = (loggedInUser: LoggedInUser | void): void => {
		if (loggedInUser) {
			this.currentUser = loggedInUser;
			Database.localDb.setItem('loggedInUser', JSON.stringify(loggedInUser));
		}
	}

	/**
	 * Clear the user from the local db
	 * @method
	 * @public
	 */
	public clearLoggedInUser = (): void => {
		Database.localDb.removeItem('loggedInUser');
	}
}