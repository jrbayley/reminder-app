import { SigninControllerType } from '../authentication/SigninControllerType';

/**
 *
 * Configuration returnseters for the project..
 * @class
 *
 */

export class Configuration {

	/**
	 * Returns the configuration item
	 * @method REST_HOST
	 * @public
	 * @static
	 * @returns {string} REST_HOST The url of the rest host to connect to
	 */
	public static REST_HOST = 'https://littlereminder.co.uk';
	// public static REST_HOST = 'http://localhost:3000';

	/**
	 * Returns the configuration item for the signin endpoint
	 * @method REST_SIGN_IN
	 * @public
	 * @static
	 * @returns {string} REST_SIGN_IN The uri for signin endpoint
	 */
	public static REST_SIGN_IN = '/rem/signin';

	/**
	 * Returns the configuration item reminders endpoint for lists
	 * @method REST_REMINDERS
	 * @public
	 * @static
	 * @returns {string} REST_REMINDERS The uri for reminder REST calls
	 */
	public static REST_REMINDERS = '/rem/reminders';

	/**
	 * Returns the configuration item for reminder singular items
	 * @method REST_REMINDER
	 * @public
	 * @static
	 * @returns {string} REST_REMINDER The uri for reminder REST calls
	 */
	public static REST_REMINDER = '/rem/reminder';

	/**
	 * Returns the configuration item for reminder periods
	 * @method REST_REMINDER_PERIODS
	 * @public
	 * @static
	 * @returns {string} REST_REMINDER_PERIODS The uri for reminder period REST calls
	 */
	public static REST_REMINDER_PERIODS = '/rem/reminderPeriods';

	/**
	 * Returns the configuration item for reminder plans
	 * @method REST_REMINDER_PLAN
	 * @public
	 * @static
	 * @returns {string} REST_REMINDER_PLAN The uri for reminder plan REST calls
	 */
	public static REST_REMINDER_PLAN = '/rem/reminderPlan';

	/**
	 * Returns the configuration item for managing the account password
	 * @method REST_ACCOUNT_PASSWORD
	 * @public
	 * @static
	 * @returns {string} REST_ACCOUNT_PASSWORD The uri for reminder account REST calls
	 */
	public static REST_ACCOUNT_PASSWORD = '/rem/account';

	/**
	 * Handles the remote send of email to the users that have forgotten their password.
	 * @method REST_PASSWORD_RESET_EMAIL
	 * @public
	 * @static
	 * @returns {string} REST_PASSWORD_RESET_EMAIL The uri for reminder lost password recovery requests.
	 */
	public static REST_PASSWORD_RESET_EMAIL = '/rem/recover';

	/**
	 * Handles the remote send of registration data.
	 * @method REST_REGISTRATION
	 * @public
	 * @static
	 * @returns {string} REST_REGISTRATION The uri for reminder registration.
	 */
	public static REST_REGISTRATION = '/rem/register';

	/**
	 * Handles the reset password using the reset code and the new password.
	 * @method REST_PASSWORD_RESET
	 * @public
	 * @static
	 * @returns {string} REST_PASSWORD_RESET The uri for reminder lost password recovery requests.
	 */
	public static REST_PASSWORD_RESET = '/rem/reset';

	/**
	 * The protocol to use to connect to AWS IoT
	 * @method MQTT_PROTOCOL
	 * @public
	 * @static
	 * @returns {string} MQTT_PROTOCOL The mqtt protocol to use mqtt:// ws:// or wss://
	 */
	public static MQTT_PROTOCOL = 'wss://';

	/**
	 * The mqtt protocol path to use after the url when using aws
	 * @method AWS_MQTT_PATH
	 * @public
	 * @static
	 * @returns {string} AWS_MQTT_PATH The mqtt protocol path to use after the url when using aws
	 */
	public static AWS_MQTT_PATH = '/mqtt';

	/**
	 * The mqtt name for connecting to AWS
	 * @method MQTT_NAME
	 * @public
	 * @static
	 * @returns {string} MQTT_NAME The mqtt name for connecting to AWS
	 */
	public static MQTT_NAME = 'webapp';


	/**
	 * The topic path to use for any node functions and communication
	 * @method MQTT_NODE_PATH
	 * @public
	 * @static
	 * @returns {string} MQTT_NODE_PATH The mqtt topic for nodes
	 */
	public static MQTT_NODE_PATH = 'littlereminder/nodes/';


	/**
	 * The topic path to use for any presence communication
	 * @method MQTT_PRESENCE_PATH
	 * @public
	 * @static
	 * @returns {string} MQTT_PRESENCE_PATH The mqtt topic for presence
	 */
	public static MQTT_PRESENCE_PATH = 'littlereminder/presence/';


	/**
	 * The Authentication signin type
	 * @method SIGNIN_TYPE
	 * @public
	 * @static
	 * @returns {SigninControllerType} SIGNIN_TYPE The Authentication signin type
	 */
	public static SIGNIN_TYPE = SigninControllerType.STANDARD;
	// public static SIGNIN_TYPE = SigninControllerType.COGNITO;


	/**
	 * The FCM Server registration endpoint
	 * @method FCM_ENDPOINT
	 * @public
	 * @static
	 * @returns {string} FCM_ENDPOINT The FCM Server registration endpoint
	 */
	public static FCM_ENDPOINT = '/rem/fcm';


	/**
	 * The Mqtt hostname to use for standard mqtt
	 * @method MQTT_HOST
	 * @public
	 * @static
	 * @returns {string} MQTT_HOST The Mqtt hostname
	 */
	public static MQTT_HOST = 'wss://littlereminder.co.uk/mqtt/';
	// public static MQTT_HOST = 'ws://localhost:8888';

	/**
	 * The Mqtt port to connect to
	 * @method MQTT_PORT
	 * @public
	 * @static
	 * @returns {string} MQTT_PORT The Mqtt port to connect to
	 */
	public static MQTT_PORT = '';

	/**
	 * The number of times to retry connecting to mqtt service
	 * @method MQTT_RETRY_LIMIT
	 * @public
	 * @static
	 * @returns {number} MQTT_RETRY_LIMIT The retry limit
	 */
	public static MQTT_RETRY_LIMIT = 10;
}