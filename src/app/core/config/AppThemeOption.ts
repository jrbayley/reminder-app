
/**
 * Available options for uithemes
 * @enum
 */

export enum AppThemeOption {
	material = 'material',
	ios = 'ios',
	auto = 'auto',
	aurora = 'aurora'
}