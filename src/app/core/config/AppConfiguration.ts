import { AppThemeOption } from './AppThemeOption';

/**
 * App configuration for the UI
 * @class
 */

export class AppConfiguration {

	/**
	 * Returns the app config item APPID that contains the app id
	 * @method APPID
	 * @public
	 * @static
	 * @returns {string} APPID The App identifier
	 */
	public static APPID = 'com.jbayley.reminder';

	/**
	 * Returns the app config item APPTHEME that contains the chosen theme
	 * @method APPTHEME
	 * @public
	 * @static
	 * @returns {string} APPTHEME The App theme to render
	 */
	public static APPTHEME : AppThemeOption = AppThemeOption.ios;

	/**
	 * Returns the app config item APPNAME that returns the app name.
	 * @method APPNAME
	 * @public
	 * @static
	 * @returns {string} APPNAME The App name used in the UI
	 */
	public static APPNAME = 'Reminder';
}
