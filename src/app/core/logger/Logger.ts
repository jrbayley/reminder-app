import { Loglevel } from './Loglevel';

/**
 * The app logger class
 * @class
 */

export class Logger {

	/** @var {Loglevel} loglevel The chosen log level */
	private static loglevel : Loglevel;

	/**
	 * Initialise the logger
	 * @method init
	 * @public
	 * @static
	 */
	public static init = (): void => {
		Logger.loglevel = Loglevel.DEBUG;
	}

	/**
	 * Log an error level message if loglevel permits
	 * @method error
	 * @public
	 * @static
	 * @param {string} message The message to log
	 * @param {any} args An array of arguments to map to the output
	 */
	public static error = (message: string, ...args: any[]): void => {
		if (Logger.loglevel && Logger.loglevel >= 0) {
			let enhancedArgs = args[0];
			if (args && args.length > 1) {
				enhancedArgs = args.map(arg => JSON.stringify(arg, undefined, 4));
			}
			enhancedArgs = (enhancedArgs) ? enhancedArgs : ' ';
			console.error('Error: ' + message, enhancedArgs);
		}
	}

	/**
	 * Log a warning level message if loglevel permits
	 * @method warn
	 * @public
	 * @static
	 * @param {string} message The message to log
	 * @param {any} args An array of arguments to map to the output
	 */
	public static warn = (message: string, ...args: any[]): void => {
		if (Logger.loglevel && Logger.loglevel >= 1) {
			let enhancedArgs = args[0];
			if (args && args.length > 1) {
				enhancedArgs = args.map(arg => JSON.stringify(arg, undefined, 4));
			}
			enhancedArgs = (enhancedArgs) ? enhancedArgs : ' ';
			console.warn('Warn: ' + message, enhancedArgs);
		}
	}

	/**
	 * Log an info level message if loglevel permits
	 * @method info
	 * @public
	 * @static
	 * @param {string} message The message to log
	 * @param {any} args An array of arguments to map to the output
	 */
	public static info = (message: string, ...args: any[]): void => {
		if (Logger.loglevel && Logger.loglevel >= 2) {
			let enhancedArgs = args[0];
			if (args && args.length > 1) {
				enhancedArgs = args.map(arg => JSON.stringify(arg, undefined, 4));
			}
			enhancedArgs = (enhancedArgs) ? enhancedArgs : ' ';
			console.info('Info: ' + message, enhancedArgs);
		}
	}

	/**
	 * Log a debug level message if loglevel permits
	 * @method debug
	 * @public
	 * @static
	 * @param {string} message The message to log
	 * @param {any} args An array of arguments to map to the output
	 */
	public static debug = (message: string, ...args: any[]): void => {
		if (Logger.loglevel && Logger.loglevel >= 3) {
			let enhancedArgs = args[0];
			if (args && args.length > 1) {
				enhancedArgs = args.map(arg => JSON.stringify(arg, undefined, 4));
			}
			enhancedArgs = (enhancedArgs) ? enhancedArgs : ' ';
			console.log('Debug: ' + message, enhancedArgs);
		}
	}

	/**
	 * Log a silly level message if loglevel permits
	 * @method silly
	 * @public
	 * @static
	 * @param {string} message The message to log
	 * @param {any} args An array of arguments to map to the output
	 */
	public static silly = (message: string, ...args: any[]): void => {
		if (Logger.loglevel && Logger.loglevel >= 4) {
			let enhancedArgs = args[0];
			if (args.length > 1) {
				enhancedArgs = args.map(arg => JSON.stringify(arg, undefined, 4));
			}
			enhancedArgs = (enhancedArgs) ? enhancedArgs : ' ';
			console.log('Silly: ' + message, enhancedArgs);
		}
	}
}