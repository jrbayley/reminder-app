/**
 * The available log levels
 * @enum
 */

export enum Loglevel {
	ERROR,
	WARNING,
	INFO,
	DEBUG,
	SILLY
}