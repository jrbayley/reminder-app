import { Dom7Instance } from 'dom7';
import Framework7 from 'framework7';
import { Router } from 'framework7/modules/router/router';

import { App } from '../../App';
import { Logger } from '../logger/Logger';
import { F7 } from './F7';

export class Framework7PageEvents {

	/**
	 * @method
	 * @public
	 * @static
	 * Initialise the F7 events for binding controllers to a UI
	 */
	public static init = (): void => {
		const appDom: Dom7Instance = F7.framework7App.dom('#app');
		const app: Framework7 = F7.framework7App.app;
		const pageQueryId: 'id' = 'id';
		app.on('pageInit', (event: Router.Page) => {
			Logger.debug('Framework7PageEvents: Event %o', event);
			// Add some css classes for transition transforms
			if (event.direction === 'forward') {
				appDom.addClass('entering');
				appDom.removeClass('entered');
				appDom.removeClass('leaving');
			}
			F7.framework7App.dom('.nav-menu-link').removeClass('active');
			F7.framework7App.dom('#' + event.name + '-nav-button').addClass('active');
			F7.framework7App.dom('.starting').removeClass('starting');

			// Determine what to do based on the event name
			switch (event.name) {
				case 'home':
					App.homeUi.onPageInit();
					break;
				case 'registration':
					App.registrationUi.onPageInit();
					break;
				case 'reminders':
					App.remindersUi.onPageInit();
					break;
				case 'calendar':
					App.calendarUi.onPageInit();
					break;
				case 'reminderPlan':
					App.reminderPlanUi.onPageInit();
					break;
				case 'addReminder':
					App.addReminderUi.onPageInit();
					break;
				case 'editReminder':
					App.reminderUi.onPageInitWithItem(event.route.query[pageQueryId]);
					break;
				case 'settings':
					App.settingsUi.onPageInit();
					break;
				case 'account':
					App.accountUi.onPageInit();
					break;
				case 'about':
					App.aboutUi.onPageInit();
					break;
				case 'online':
					App.onlineUi.onPageInit();
					break;
				case 'chatWith':
					App.chatWithUi.onPageInitWithItem(event.route.query[pageQueryId]);
					break;
				}
		});

		app.on('pageBeforeIn', (event) => {
			appDom.addClass('entering');
			switch (event.name) {
				case 'home':
					App.homeUi.onBeforePageIn();
					break;
				case 'reminders':
					App.remindersUi.onBeforePageIn();
					break;
				case 'calendar':
					App.calendarUi.onBeforePageIn();
					break;
				case 'reminderPlan':
					App.reminderPlanUi.onBeforePageIn();
					break;
					case 'addReminder':
					App.addReminderUi.onBeforePageIn();
					break;
				case 'editReminder':
					App.reminderUi.onBeforePageIn();
					break;
				case 'settings':
					App.settingsUi.onBeforePageIn();
					break;
				case 'account':
					App.accountUi.onBeforePageIn();
					break;
				case 'about':
					App.aboutUi.onBeforePageIn();
					break;
				case 'online':
					App.onlineUi.onBeforePageIn();
					break;
				case 'chatWith':
					App.chatWithUi.onBeforePageIn();
					break;
			}
		});

		app.on('pageAfterIn', (event) => {
			appDom.removeClass('entering');
			switch (event.name) {
				case 'home':
					App.homeUi.onAfterPageIn();
					break;
				case 'reminders':
					App.remindersUi.onAfterPageIn();
					break;
				case 'calendar':
					App.calendarUi.onAfterPageIn();
					break;
				case 'reminderPlan':
					App.reminderPlanUi.onAfterPageIn();
					break;
					case 'addReminder':
					App.addReminderUi.onAfterPageIn();
					break;
				case 'editReminder':
					App.reminderUi.onAfterPageIn();
					break;
				case 'settings':
					App.settingsUi.onAfterPageIn();
					break;
				case 'account':
					App.accountUi.onAfterPageIn();
					break;
				case 'about':
					App.aboutUi.onAfterPageIn();
					break;
				case 'online':
					App.onlineUi.onAfterPageIn();
					break;
				case 'chatWith':
					App.chatWithUi.onAfterPageIn();
					break;
				}
		});

		app.on('pageBeforeOut', (event) => {
			switch (event.name) {
				case 'home':
					App.homeUi.onBeforePageOut();
					break;
				case 'reminders':
					App.remindersUi.onBeforePageOut();
					break;
				case 'calendar':
					App.calendarUi.onBeforePageOut();
					break;
				case 'reminderPlan':
					App.reminderPlanUi.onBeforePageOut();
					break;
					case 'addReminder':
					App.addReminderUi.onBeforePageOut();
					break;
				case 'editReminder':
					App.reminderUi.onBeforePageOut();
					break;
				case 'settings':
					App.settingsUi.onBeforePageOut();
					break;
				case 'account':
					App.accountUi.onBeforePageOut();
					break;
				case 'about':
					App.aboutUi.onBeforePageOut();
					break;
				case 'online':
					App.onlineUi.onBeforePageOut();
					break;
				case 'chatWith':
					App.chatWithUi.onBeforePageOut();
					break;
			}
		});

		app.on('pageReinit', (event) => {
			switch (event.name) {
				case 'home':
					App.homeUi.onPageReinit();
					break;
				case 'reminders':
					App.remindersUi.onPageReinit();
					break;
				case 'calendar':
					App.calendarUi.onPageReinit();
					break;
				case 'reminderPlan':
					App.reminderPlanUi.onPageReinit();
					break;
					case 'addReminder':
					App.addReminderUi.onPageReinit();
					break;
				case 'editReminder':
					App.reminderUi.onPageReinit();
					break;
				case 'settings':
					App.settingsUi.onPageReinit();
					break;
				case 'account':
					App.accountUi.onPageReinit();
					break;
				case 'about':
					App.aboutUi.onPageReinit();
					break;
				case 'online':
					App.onlineUi.onPageReinit();
					break;
				case 'chatWith':
					App.chatWithUi.onPageReinit();
					break;
			}
		});
	}
}

