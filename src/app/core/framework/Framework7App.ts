import Dom7 from 'dom7';
import Framework7 from 'framework7';

import { AppConfiguration } from '../config/AppConfiguration';
import { Routes } from '../router/Routes';

/**
 * The core framework initialisation and references
 * @class
 */

export class Framework7App {

	/** @var {Dom7} dom Reference to the dom controls */
	public dom = Dom7;

	/** @var {Framework7} app Reference to the F7 core instance */
	public app : Framework7;

	/**
	 * Initialise the F7 app and router
	 * @constructor
	 */
	public constructor() {
		const routes = new Routes();
		this.app = new Framework7({
			root: '#app', 						// App root element
			id: AppConfiguration.APPID, 		// App bundle ID
			name: AppConfiguration.APPNAME, 	// App name
			theme: AppConfiguration.APPTHEME, 	// Automatic theme detection
			routes: routes.routes,				// App routes
			autoDarkTheme : false
		});
	}
}


