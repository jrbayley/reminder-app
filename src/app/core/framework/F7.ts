import { Framework7App } from './Framework7App';

/**
 * The Framework module root class
 * @class
 */

export class F7 {

	/** @var {Framework7App} framework7App The framework 7 app instance */
	public static framework7App : Framework7App;

	/**
	 * Initialise the framework instance
	 * @method init
	 * @public
	 * @static
	 */
	public static init = (): void => {
		F7.framework7App = new Framework7App();
	}
}