/**
 * Setup the app routes
 * @class
 */

export class Routes {

	/** @var {Routes.ModalRouteParameters[]} routes An array of app routes */
	public routes = [
		{
			path: '/master-detail/',
			url: './pages/master.html',
			master: true,
			detailRoutes: [
				{
					path: '/master-detail/:id/',
					templateUrl: './pages/detail.html',
				},
			]
		},
		{
			path: '/',
			url: './index.html',
		},
		{
			path: '/registration/',
			url: './pages/registration.html',
		},
		{
			path: '/about/',
			url: './pages/about.html',
			options: {
				transition: 'f7-parallax',
			},
		},
		{
			path: '/settings/',
			url: './pages/settings.html',
			options: {
				transition: 'f7-parallax',
			},
		},
		{
			path: '/addReminder/',
			url: './pages/addReminder.html',
		},
		{
			path: '/addVehicle/',
			url: './pages/addVehicle.html',
		},
		{
			path: '/addHouse/',
			url: './pages/addHouse.html',
		},
		{
			path: '/reminders/',
			url: './pages/reminders.html',
		},
		{
			path: '/online/',
			url: './pages/online.html',
		},
		{
			path: '/chatWith/',
			url: './pages/chatWith.html',
		},
		{
			path: '/reminder/',
			url: './pages/reminder.html',
			options: {
				transition: 'f7-flip',
			},
		},
		{
			path: '/listReminders/',
			url: './pages/listReminders.html',
		},
		{
			path: '/reminderPlan/',
			url: './pages/reminderPlan.html',
		},
		{
			path: '/calendar/',
			url: './pages/calendar.html',
		},
		{
			path: '/terms/',
			url: './pages/terms.html',
		},
		{
			path: '/privacy/',
			url: './pages/privacy.html',
		},
		{
			path: '/account/',
			url: './pages/account.html',
		},

		// Default route (404 page). MUST BE THE LAST
		{
			path: '(.*)',
			url: './pages/404.html',
		},
	];

}