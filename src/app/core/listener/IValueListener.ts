/**
 * @type
 * Define the type for the listener
 */

export type IValueListener<T, R> = (value?: T)  => R;