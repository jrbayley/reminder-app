import { Publisher } from '../Publisher';

/**
 * @class
 * Collection of all the app fcm listeners
 */
export class PluginListener {

	/** @var signature Signal that the signature plugin was changed */
	public signature : Publisher<(string)>;

	/**
	 * Initialise all the listeners to allow publish of fcm state changes
	 * @constructor
	 */
	constructor() {
		this.signature = new Publisher();
	}
}