import { IReminder } from '../../../reminder/reminder/IReminder';
import { Publisher } from '../Publisher';

/**
 * @class
 * Collection of all the app reminder listeners
 */
export class ReminderListener {

	/** @var reminderChanged Signal that the reminder data has changed */
	public reminderChanged : Publisher<(IReminder[])>;

	/** @var reminderPlanChanged Signal that the reminder plan data changed */
	public reminderPlanChanged : Publisher<(IReminder[])>;

	/**
	 * Initialise all the listeners to allow publish of reminder state changes
	 * @constructor
	 */
	constructor() {
		this.reminderChanged = new Publisher();
		this.reminderPlanChanged = new Publisher();
	}
}