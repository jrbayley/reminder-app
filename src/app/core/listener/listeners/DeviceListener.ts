import { Publisher } from '../Publisher';

/**
 * @class
 * Collection of all the app device listeners
 */
export class DeviceListener {

	/** @var deviceReady Signal that the device cordova init is completed */
	public deviceReady : Publisher<(Event)>;

	/** @var deviceBack Signal that the device back button was pressed */
	public deviceBack : Publisher<(Event)>;

	/** @var devicePause Signal that the device has triggered app pause */
	public devicePause : Publisher<(Event)>;

	/** @var deviceResume Signal that the device has triggered app resume */
	public deviceResume : Publisher<(Event)>;

	/** @var deviceNetwork Signal that the device network state has changed */
	public deviceNetwork : Publisher<(string)>;

	/**
	 * Initialise all the listeners to allow publish of device state changes
	 * @constructor
	 */
	constructor() {
		this.deviceReady = new Publisher();
		this.deviceBack = new Publisher();
		this.devicePause = new Publisher();
		this.deviceResume = new Publisher();
		this.deviceNetwork = new Publisher();
	}
}