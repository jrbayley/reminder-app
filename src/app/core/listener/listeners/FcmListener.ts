import { INotificationEventResponse } from '../../comms/fcm/INotificationEventResponse';
import { IUpdateFcmToken } from '../../comms/fcm/IUpdateFcmToken';
import { Publisher } from '../Publisher';

/**
 * @class
 * Collection of all the app fcm listeners
 */
export class FcmListener {

	/** @var fcmNotificationReceived Signal that there was an fcm token received */
	public fcmNotificationReceived : Publisher<(INotificationEventResponse)>;

	/** @var fcmRegistered Signal that there was an fcm token received */
	public fcmRegistered : Publisher<(IUpdateFcmToken)>;

	/**
	 * Initialise all the listeners to allow publish of fcm state changes
	 * @constructor
	 */
	constructor() {
		this.fcmRegistered = new Publisher();
		this.fcmNotificationReceived = new Publisher();
	}
}