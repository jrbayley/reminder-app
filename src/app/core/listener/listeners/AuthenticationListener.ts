import { LoggedInUser } from '../../appData/LoggedInUser';
import { IUserValidationResponse } from '../../authentication/models/IUserValidationResponse';
import { SigninStage } from '../../authentication/models/SigninStage';
import { Publisher } from '../Publisher';

/**
 * @class
 * Collection of all the app authentication listeners
 */
export class AuthenticationListener {

	/** @var userSignin Signal that the user has signed in */
	public signinStage : Publisher<(SigninStage)>;

	/** @var userSignin Signal that the user has signed in */
	public userSignin : Publisher<(LoggedInUser)>;

	/** @var userSignout Signal that the user has signed out */
	public userSignout : Publisher<void>;

	/** @var authenticationListener Signal that standard authentication has signed in */
	public authenticationListener : Publisher<(IUserValidationResponse)>;

	/**
	 * Initialise all the listeners to allow publish of authentication state changes
	 * @constructor
	 */
	constructor() {
		this.signinStage = new Publisher();
		this.userSignin = new Publisher();
		this.userSignout = new Publisher();
		this.authenticationListener = new Publisher();
	}
}