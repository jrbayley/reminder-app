import { Publisher } from '../Publisher';

/**
 * @class
 * Collection of all the app reminder listeners
 */
export class SettingsListener {

	/** @var settingsThemeChanged Signal that the app theme changed */
	public settingsThemeChanged : Publisher<string>;

	/** @var settingsColourChanged Signal that the app colour changed */
	public settingsColourChanged : Publisher<string>;


	/**
	 * Initialise all the listeners to allow publish of settings state changes
	 * @constructor
	 */
	constructor() {
		this.settingsThemeChanged = new Publisher();
		this.settingsColourChanged = new Publisher();
	}
}