import { IMqttMessage } from '../../comms/mqtt/interfaces/IMqttMessage';
import { IMqttStatusUpdate } from '../../comms/mqtt/interfaces/IMqttStatusUpdate';
import { MqttPresence } from '../../comms/mqtt/interfaces/MqttPresence';
import { Publisher } from '../Publisher';

/**
 * @class
 * Collection of all the app mqtt listeners
 */
export class MqttListener {

	/** @var mqttStatus Signal the mqtt status */
	public mqttStatus : Publisher<(IMqttStatusUpdate)>;

	/** @var mqttMessageSend Signal the mqtt message send */
	public mqttMessageSend : Publisher<(IMqttMessage)>;

	/** @var mqttMessageReceived Signal mqtt received a message */
	public mqttMessageReceived : Publisher<(IMqttMessage)>;

	/** @var mqttPresenceSend Signal mqtt received a presence message */
	public mqttPresenceSend : Publisher<(void)>;

	/** @var mqttPresenceReceived Signal mqtt received a presence message */
	public mqttPresenceReceived : Publisher<(IMqttMessage)>;

	/** @var presenceListUpdated Signal that the local presence list of devices was updeted */
	public presenceListUpdated : Publisher<(MqttPresence[])>;

	/**
	 * Initialise all the listeners to allow publish of mqtt state changes
	 * @constructor
	 */
	constructor() {
		this.mqttStatus = new Publisher();
		this.mqttMessageSend = new Publisher();
		this.mqttMessageReceived = new Publisher();
		this.mqttPresenceReceived = new Publisher();
		this.mqttPresenceSend = new Publisher();
		this.presenceListUpdated = new Publisher();
	}
}