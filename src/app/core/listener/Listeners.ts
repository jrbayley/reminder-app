import { AuthenticationListener } from './listeners/AuthenticationListener';
import { DeviceListener } from './listeners/DeviceListener';
import { FcmListener } from './listeners/FcmListener';
import { MqttListener } from './listeners/MqttListener';
import { PluginListener } from './listeners/PluginListener';
import { ReminderListener } from './listeners/ReminderListener';
import { SettingsListener } from './listeners/SettingsListener';

/**
 * @class
 * Collection of all the app listeners
 */

export class Listeners {

	/** @var addonListener Handle signals for plugin changes */
	public static pluginListener : PluginListener;

	/** @var authenticationListener Handle signals for authentication changes */
	public static authenticationListener : AuthenticationListener;

	/** @var deviceListener Handle signals for device changes */
	public static deviceListener : DeviceListener;

	/** @var fcmListener Handle signals for fcm changes */
	public static fcmListener : FcmListener;

	/** @var mqttListener Handle signals for mqtt changes */
	public static mqttListener : MqttListener;

	/** @var reminderListener Handle signals for reminder changes */
	public static reminderListener : ReminderListener;

	/** @var settingsListener Handle signals for settings changes */
	public static settingsListener : SettingsListener;

	/**
	 * Initialise all the listeners to allow publish of add state
	 * @method
	 * @public
	 * @static
	 */
	public static init = (): void => {
		Listeners.authenticationListener = new AuthenticationListener();
		Listeners.deviceListener = new DeviceListener();
		Listeners.fcmListener = new FcmListener();
		Listeners.mqttListener = new MqttListener();
		Listeners.reminderListener = new ReminderListener();
		Listeners.settingsListener = new SettingsListener();
		Listeners.pluginListener = new PluginListener();
	}
}