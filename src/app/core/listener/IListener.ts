import { IValueListener } from './IValueListener';

/**
 * The base listener interface
 * @interface
 */

export interface IListener<T> extends IValueListener<T, Promise<void>> {
	//
}