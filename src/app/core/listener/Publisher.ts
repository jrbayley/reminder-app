import _ from 'lodash';

import { IListener } from './IListener';

/**
 * Publisher generic class
 * @class
 */

export class Publisher<T> {

	/** @var {IListener<T>[]} subscribers An array of promised based subscribers */
	private subscribers : IListener<T>[] = [];

	/**
	 * @constructor
	 */
	constructor() {
		//
	}

	/**
	 * Return the number of subscribers on this publisher
	 * @method numberOfSubscribers
	 * @public
	 * @returns {number} The number of subscribers
	 */
	public numberOfSubscribers = (): number => {
		return this.subscribers.length;
	};

	/**
	 * Subscribe the passed listener
	 * @method
	 * @public
	 * @param {IListener<T>} listener The listener to subscribe to the publisher
	 */
	public subscribe = (listener: IListener<T>): void => {
		this.unsubscribe(listener);
		this.subscribers.push(listener);
	};

	/**
	 * Unsubscribe the passed listener from the publisher
	 * @method
	 * @public
	 * @param {IListener<T>} listener The listener to subscribe
	 */
	public unsubscribe = (listener: IListener<T>): void => {
		_.pull(this.subscribers, listener);
	};

	/**
	 * Fire an on change event to the subscribed listeners
	 * @method
	 * @public
	 * @async
	 * @param {T} date The date to send to all subscribed listeners
	 */
	public publish = async (data?: T): Promise<void> => {
		const listeners: Promise<void>[] = [];
		for (const listener of this.subscribers) {
			listeners.push(listener(data));
		}
		await Promise.all(listeners);
	};
}