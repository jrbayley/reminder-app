import { LoggedInUser } from '../appData/LoggedInUser';
import { Device } from '../device/Device';
import { CloudNotification } from './fcm/CloudNotification';
import { RegistrationListener } from './fcm/RegistrationListener';
import { Mqtt } from './mqtt/Mqtt';
import { RestRequest } from './RestRequest';

/**
 * Manage any communication to external sources
 * @class
 */

export class Comms {

	/** @var {Mqtt} mqtt The mqtt client instance */
	public static mqtt : Mqtt;

	/** @var {RestRequest} restRequest The local rest request instance */
	public static restRequest : RestRequest;

	/** @var {CloudNotification} restRequest The local rest request instance */
	public static cloudNotification : CloudNotification;

	/** @var {RegistrationListener} restRequest The local rest request instance */
	public static registrationListener : RegistrationListener;

	/**
	 * Initialise the local static reference with a new Rest Request instance
	 * @method init
	 * @public
	 * @static
	 */
	public static init = (): void => {
		Comms.restRequest = new RestRequest();
		Comms.mqtt = new Mqtt();
		if (Device.localDevice.isMobile && Device.cordova.deviceEvents) {
			// Listeners.userSignin.subscribe(Comms.initialiseCloudNotifications);
		}
	}

	/**
	 * Initialise the local static reference with a new cloud notification instance
	 * @method initialiseCloudNotifications
	 * @public
	 * @static
	 * @param {LoggedInUser} loggedInUser The logged in user to use for the cloud notification id
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public static initialiseCloudNotifications = (loggedInUser: LoggedInUser): Promise<void> => {
		Comms.cloudNotification = new CloudNotification(loggedInUser);
		Comms.registrationListener = new RegistrationListener(Comms.restRequest).listenForRegistration();
		return;
	}

}