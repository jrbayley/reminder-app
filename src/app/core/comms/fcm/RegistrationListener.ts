import { Configuration } from '../../config/Configuration';
import { Listeners } from '../../listener/Listeners';
import { Logger } from '../../logger/Logger';
import { RestRequest } from '../RestRequest';
import { IUpdateFcmToken } from './IUpdateFcmToken';

export class RegistrationListener {

	public restRequest : RestRequest;

	constructor(restRequest: RestRequest) {
		this.restRequest = restRequest;
	}

	public listenForRegistration = (): RegistrationListener => {
		Listeners.fcmListener.fcmRegistered.subscribe(this.fcmRegistered);
		return this;
	}

	private fcmRegistered = (updateFcmToken: IUpdateFcmToken): Promise<void> => {
		return this.restRequest.restSecurePost(Configuration.FCM_ENDPOINT, updateFcmToken)
		.catch((error: Error) => {
			Logger.error('Error registering token ', error.message);
		});

	}
}