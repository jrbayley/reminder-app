/**
 * 
 */

export interface INotificationEventAdditionalData {
	[name: string]: any;

	/**
	 * Whether the notification was received while the app was in the foreground
	 * @var {boolean} foreground
	 */
	foreground? : boolean;

	/**
	 * Will be true if the application is started by clicking on the push notification, false if the app is already started. (Android/iOS only)
	 * @var {boolean} coldstart
	 */
	coldstart? : boolean;

	/**
	 * Value to use to allow collapsing into a group
	 * @var {string} collapse_key
	 */
	collapse_key? : string;

	/**
	 * Where the notification was sent from
	 * @var {string} from
	 */
	from? : string;

	/**
	 * The notification id
	 * @var {string} notId
	 */
	notId? : string;
}