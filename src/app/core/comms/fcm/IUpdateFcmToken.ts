/**
 * The update server with cloud notification token request
 * @interface
 */

export interface IUpdateFcmToken {
	username : string;
	token : string;
}