import { LoggedInUser } from '../../appData/LoggedInUser';
import { Listeners } from '../../listener/Listeners';
import { Logger } from '../../logger/Logger';
import { INotificationEventResponse } from './INotificationEventResponse';
import { IUpdateFcmToken } from './IUpdateFcmToken';

/**
 * Handle the cloud notification registration functions
 * @class
 */

export class CloudNotification {

	/** @var {LoggedInUser} loggedInUser The current logged in user details */
	private loggedInUser : LoggedInUser;

	/** @var {PhonegapPluginPush.PushNotification} push The device push notification interface */
	public push : PhonegapPluginPush.PushNotification;

	/**
	 * @constructor
	 * @param {LoggedInUser} loggedInUser The logged in user details to save locally
	 */
	constructor(loggedInUser: LoggedInUser) {
		this.loggedInUser = loggedInUser;
		this.initialise();
		this.initialiseEvents();
	}

	/**
	 * Initialise the cloud notifications
	 * @method initialise
	 * @public
	 */
	public initialise = (): void => {
		const options: PhonegapPluginPush.InitOptions = {
			ios: {
				alert: 'true',
				badge: 'true',
				sound: 'true'
			}
		};

		this.push = PushNotification.init(options);
	}

	/**
	 * Initialise the cloud notifications events
	 * @method initialiseEvents
	 * @public
	 */
	private initialiseEvents = (): void => {
		this.push.on('registration', this.onRegistration);

		this.push.on('notification', this.onNotification);

		this.push.on('error', (error: Error) => {
			Logger.error(error.message);
		});
	}

	/**
	 * Send the registration token to the server
	 * @method onRegistration
	 * @private
	 * @async
	 */
	private onRegistration = async (response: any): Promise<void> => {
		const updateFcmToken: IUpdateFcmToken = {
			username : this.loggedInUser.username,
			token : response.registrationId
		};
		Listeners.fcmListener.fcmRegistered.publish(updateFcmToken);
	}

	/**
	 * Fire the notification received listeners
	 * @method onNotification
	 * @private
	 * @param {INotificationEventResponse} notificationEventResponse The notification event from the fcm processor
	 */
	private onNotification = (notificationEventResponse: INotificationEventResponse): void => {
		Logger.debug('Received notification : ', notificationEventResponse);
		Listeners.fcmListener.fcmNotificationReceived.publish(notificationEventResponse);
	}

	/**
	 * Check to see if the app has permission to send cloud notifications
	 * @method hasPermission
	 * @public
	 * @async
	 * @returns {Promise<boolean>} Returns true if the app has permission to receive notifications
	 */
	public hasPermission = (): Promise<boolean> => {
		return new Promise<boolean>((resolve: (value: boolean) => void): void => {
			if (PushNotification !== undefined) {
				(<any>PushNotification).hasPermission((data: any) => {
					resolve(data.isEnabled);
				});
			} else {
				resolve(true);
			}
		});
	};

	/**
	 * Set the app badge
	 * @method setBadgeNumber
	 * @public
	 * @async
	 * @param {number} value The value to set on the badge
	 * @returns {Promise<boolean>} Returns true if the app badge was set
	 */
	public setBadgeNumber = (value: number): Promise<boolean> => {
		return new Promise((resolve, _reject) => {
			return this.push.setApplicationIconBadgeNumber(
				() => {
					resolve(true);
				},
				() => {
					resolve(false);
				},
				value
			);
		});
	}

	/**
	 * Get the app badge value
	 * @method getBadgeNumber
	 * @public
	 * @async
	 * @returns {Promise<number>} Returns the app badge value
	 */
	public getBadgeNumber = (): Promise<number> => {
		return new Promise((resolve, reject) => {
			return this.push.getApplicationIconBadgeNumber(
				(value: number) => {
					resolve(value);
				},
				() => {
					reject();
				}
			);
		});
	}

	/**
	 * Clear all notifications
	 * @method clearAllNotifications
	 * @public
	 * @async
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public clearAllNotifications = (): Promise<void> => {
		return new Promise((resolve, reject) => {
			return this.push.clearAllNotifications(
				() => {
					resolve();
				},
				() => {
					reject();
				}
			);
		});
	}

	/**
	 * Clear a single notification
	 * @method clearNotification
	 * @public
	 * @async
	 * @param {number} notificationId The notification id to clear
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public clearNotification = (notificationId: number): Promise<void> => {
		return new Promise((resolve, reject) => {
			return (<any>this.push).clearNotification(
				() => {
					resolve();
				},
				() => {
					reject();
				},
				notificationId
			);
		});
	}

	/**
	 * Subscribe to a notification topic
	 * @method subscribe
	 * @public
	 * @async
	 * @param {string} topic The notification topic
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public subscribe = (topic: string): Promise<void> => {
		return new Promise((resolve, reject) => {
			return this.push.subscribe(
				topic,
				() => {
					resolve();
				},
				() => {
					reject();
				}
			);
		});
	}

	/**
	 * Unsubscribe to a notification topic
	 * @method unSubscribe
	 * @public
	 * @async
	 * @param {string} topic The notification topic
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public unSubscribe = (topic: string): Promise<void> => {
		return new Promise((resolve, reject) => {
			return this.push.subscribe(
				topic,
				() => {
					resolve();
				},
				() => {
					reject();
				}
			);
		});
	}

	/**
	 * Unregister the device from cloud notifications
	 * @method unregister
	 * @public
	 * @async
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public unregister = (): Promise<void> => {
		return new Promise((resolve, reject) => {
			return this.push.unregister(
				() => {
					resolve();
				},
				() => {
					reject();
				}
			);
		});
	}
}