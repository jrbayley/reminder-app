import { INotificationEventAdditionalData } from './INotificationEventAdditionalData';

/**
 * The cloud notification content interface
 * @interface
 */

export interface INotificationEventResponse {
	
	/**
	 * The text of the push message sent from the 3rd party service.
	 * @var {string} message
	 */
	message : string;
	/**
	 * The optional title of the push message sent from the 3rd party service.
	 * @var {string} title
	 */
	title? : string;
	/**
	 * The number of messages to be displayed in the badge iOS or message count in the notification shade in Android.
	 * For windows, it represents the value in the badge notification which could be a number or a status glyph.
	 * @var {string} count
	 */
	count : string;
	/**
	 * The name of the sound file to be played upon receipt of the notification.
	 * @var {string} sound
	 */
	sound : string;
	/**
	 * The path of the image file to be displayed in the notification.
	 * @var {string} image
	 */
	image : string;
	/**
	 * An optional collection of data sent by the 3rd party push service that does not fit in the above properties.
	 * @var {INotificationEventAdditionalData} additionalData any additional data
	 */
	additionalData : INotificationEventAdditionalData;
}