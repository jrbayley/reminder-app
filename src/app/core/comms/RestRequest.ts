import axios from 'axios';

import { IDeleteResponse } from '../appData/IDeleteResponse';
import { LoggedInUser } from '../appData/LoggedInUser';
import { Listeners } from '../listener/Listeners';
import { Logger } from '../logger/Logger';

/**
 * Utility for sending rest requests to remote services
 * @class
 */

export class RestRequest {

	/** @var {string} accessToken The access token for secure access */
	private accessToken : string;

	constructor() {
		Listeners.authenticationListener.userSignin.subscribe(this.updateToken);
		Listeners.authenticationListener.userSignout.subscribe(this.clearToken);
	}

	/**
	 * Make a standard rest get request
	 * @method restGet
	 * @public
	 * @async
	 * @param {string} url The url to send to
	 * @param {any} requestData The request data to get
	 * @returns {Promise<any>} Returns the response in a promise
	 */
	public restGet = async (url: string, requestData: any): Promise<any> => {
		try {
			const response = await axios.get(url, requestData);
			return response.data;
		}
		catch (error) {
			Logger.debug('RestRequest.restGet : Error making rest request : %s', error.message);
			return Promise.reject();
		}
	}

	/**
	 * Make a standard rest post request
	 * @method restPost
	 * @public
	 * @async
	 * @param {string} url The url to send to
	 * @param {any} requestData The request data to post
	 * @returns {Promise<any>} Returns the response in a promise
	 */
	public restPost = async (url: string, requestData: any): Promise<any> => {
		try {
			const response = await axios.post(url, requestData);
			return response.data;
		}
		catch (error) {
			Logger.debug('RestRequest.restPost : Error making rest request : %s', error.message);
			return Promise.reject();
		}
	}

	/**
	 * Make a standard rest put request
	 * @method restPut
	 * @public
	 * @async
	 * @param {string} url The url to send to
	 * @param {any} requestData The request data to put
	 * @returns {Promise<any>} Returns the response in a promise
	 */
	public restPut = async (url: string, requestData: any): Promise<any> => {
		try {
			const response = await axios.put(url, requestData);
			return response.data;
		}
		catch (error) {
			Logger.debug('RestRequest.restPut : Error making rest request : %s', error.message);
			return Promise.reject();
		}
	}


	/**
	 * Make a standard rest delete request
	 * @method restDelete
	 * @public
	 * @async
	 * @param {string} url The url to send to
	 * @param {any} requestData The request data to delete
	 * @returns {Promise<any>} Returns the response in a promise
	 */
	public restDelete = async (url: string, requestData: any): Promise<any> => {
		try {
			const response = await axios.put(url, requestData);
			return response.data;
		}
		catch (error) {
			Logger.debug('RestRequest.restDelete : Error making rest request : %s', error.message);
			return Promise.reject();
		}
	}

	/**
	 * Make a secure rest get request
	 * @method restSecureGet
	 * @public
	 * @async
	 * @param {string} url The url to send to
	 * @param {any} requestData The request data to get
	 * @returns {Promise<any>} Returns the response in a promise
	 */
	public restSecureGet = async (url: string): Promise<any> => {
		if (this.accessToken) {
			const axiosHeader = {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + this.accessToken
			};
			try {
				const response = await axios.get(url, { headers: axiosHeader });
				return response.data;
			}
			catch (error) {
				Logger.debug('RestRequest.restSecureGet : Error making rest request : %s', error.message);
				return Promise.reject();
			}
		} else {
			Logger.debug('RestRequest.restSecureGet : No credentials for rest request to : ', url);
			return Promise.reject();
		}
	}

	/**
	 * Make a secure rest post request
	 * @method restSecurePost
	 * @public
	 * @param {string} url The url to send to
	 * @param {any} requestData The request data to post
	 * @returns {Promise<any>} Returns the response in a promise
	 */
	public restSecurePost = async (url: string, requestData: any): Promise<any> => {
		if (this.accessToken) {
			const axiosHeader = {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + this.accessToken
			};
			try {
				const response = await axios.post(url, requestData, { headers: axiosHeader });
				return response.data;
			}
			catch (error) {
				Logger.debug('RestRequest.restSecurePost : Error making rest request : %s', error.message);
				return Promise.reject();
			}
		} else {
			Logger.debug('RestRequest.restSecurePost : No credentials for rest request to : ', url);
			return Promise.reject();
		}
	}

	/**
	 * Make a secure rest put request
	 * @method restSecurePut
	 * @public
	 * @param {string} url The url to send to
	 * @param {any} requestData The request data to put
	 * @returns {Promise<any>} Returns the response in a promise
	 */
	public restSecurePut = async (url: string, requestData: any): Promise<any> => {
		if (this.accessToken) {
			const axiosHeader = {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + this.accessToken
			};
			try {
				const response = await axios.put(url, requestData, { headers: axiosHeader });
				return response.data;
			}
			catch (error) {
				Logger.debug('RestRequest.restSecurePut : Error making rest request : %s', error.message);
				return Promise.reject();
			}
		} else {
			Logger.debug('RestRequest.restSecurePut : No credentials for rest request to : ', url);
			return Promise.reject();
		}
	}

	/**
	 * Make a secure rest patch request
	 * @method restSecurePatch
	 * @public
	 * @param {string} url The url to send to
	 * @param {any} requestData The request data to patch
	 * @returns {Promise<any>} Returns the response in a promise
	 */
	public restSecurePatch = async (url: string, requestData: any): Promise<any> => {
		if (this.accessToken) {
			const axiosHeader = {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + this.accessToken
			};
			try {
				const response = await axios.patch(url, requestData, { headers: axiosHeader });
				return response.data;
			}
			catch (error) {
				Logger.debug('RestRequest.restSecurePatch : Error making rest request : %s', error.message);
				return Promise.reject();
			}
		} else {
			Logger.debug('RestRequest.restSecurePatch : No credentials for rest request to : ', url);
			return Promise.reject();
		}
	}

	/**
	 * Make a secure rest delete request
	 * @method restSecureDelete
	 * @public
	 * @async
	 * @param {string} url The url to send to
	 * @param {any} requestData The request data to delete
	 * @returns {Promise<any>} Returns the response in a promise
	 */
	public restSecureDelete = async (url: string, reminderId: number): Promise<IDeleteResponse> => {
		if (this.accessToken) {
			const axiosHeader = {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + this.accessToken
			};
			try {
				const response = await axios.delete(url + '/' + reminderId, { headers: axiosHeader });
				return response.data;
			}
			catch (error) {
				Logger.debug('RestRequest.restSecureDelete : Error making rest request : %s', error.message);
				return Promise.reject();
			}
		} else {
			Logger.debug('RestRequest.restSecureDelete : No credentials for rest request to : ', url);
			return Promise.reject();
		}
	}

	/**
	 * Update the token for secure access
	 * @method updateToken
	 * @private
	 * @param {LoggedInUser} loggedinUser The logged in user to pull the token from.
	 */
	private updateToken = (loggedinUser: LoggedInUser): Promise<void> => {
		Logger.debug('RestRequest.updateToken : Updating rest credentials');
		this.accessToken = loggedinUser.token;
		return;
	}

	/**
	 * Update the token for secure access
	 * @method clearToken
	 * @private
	 */
	private clearToken = (): Promise<void> => {
		Logger.debug('RestRequest.clearToken : Clearing rest credentials');
		this.accessToken =  undefined;
		return;
	}
}