import { IMqttMessage } from './interfaces/IMqttMessage';

/**
 * Handle the response of any information requests routing them to the appropriate method.
 * @class
 */

export class MqttInformationHandler {

	/**
	 * @constructor
	 */
	constructor() {
		//
	}

	/**
	 * handle the forwarding of the information message to the correct function according to the info type.
	 * @method handleInformation
	 * @public
	 * @param {IMqttMessage} mqttMessage The received message.
	 */
	public handleInformation = (mqttMessage: IMqttMessage): void => {
		if (mqttMessage.mqttPayload && mqttMessage.mqttPayload.body && mqttMessage.mqttPayload.body.command) {
			switch (mqttMessage.mqttPayload.body.command) {

			}
		}
	}


}