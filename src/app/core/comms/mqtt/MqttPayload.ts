import { IMqttPayload } from './interfaces/IMqttPayload';
import { MqttMessageType } from './interfaces/MqttMessageType';
import { MqttPayloadBody } from './interfaces/MqttPayloadBody';

/**
 * The mqtt payload implementation class
 * @class
 */

export class MqttPayload implements IMqttPayload {
	
	/** @var {string} id The MQTT Messsage id */
	public id : string;

	/** @var {string} user The optional user sending the message */
	public user? : string;

	/** @var {MqttMessageType} messageType The type of message to send */
	public messageType : MqttMessageType;

	/** @var {string} source The source host sending the message */
	public source : string;

	/** @var {string} destination The destination host to receive the message */
	public destination : string;

	/** @var {string} source The source host sending the message */
	public body : MqttPayloadBody;

	/**
	 * Initialise the class from interface / json data
	 * @constructor
	 */
	constructor (mqttPayload: IMqttPayload) {
		this.messageType = mqttPayload.messageType;
		this.source = mqttPayload.source;
		this.destination = mqttPayload.destination;
		this.body = mqttPayload.body;
		this.id = mqttPayload.id;
		this.user = mqttPayload.user;
		return this;
	}
}