import { Listeners } from '../../listener/Listeners';
import { Logger } from '../../logger/Logger';
import { IMqttMessage } from './interfaces/IMqttMessage';
import { MqttMessageType } from './interfaces/MqttMessageType';
import { MqttCommandHandler } from './MqttCommandHandler';
import { MqttInformationHandler } from './MqttInformationHandler';
import { MqttStatusHandler } from './MqttStatusHandler';

/** Handle the  */

export class MqttMessageHandler {

	/** @var {MqttCommandHandler} mqttCommandHandler The class that processes and routes any command messages */
	private mqttCommandHandler : MqttCommandHandler;

	/** @var {MqttInformationHandler} mqttInformationHandler The class that processes and routes any information messages */
	private mqttInformationHandler : MqttInformationHandler;

	/** @var {MqttStatusHandler} mqttStatusHandler The class that processes and routes any status messages */
	private mqttStatusHandler : MqttStatusHandler;

	/**
	 * Initialise the handler classes and listeners
	 * @constructor
	 */
	constructor() {
		this.mqttCommandHandler = new MqttCommandHandler();
		this.mqttInformationHandler = new MqttInformationHandler();
		this.mqttStatusHandler = new MqttStatusHandler();
		Listeners.mqttListener.mqttMessageReceived.subscribe(this.handleMqttMessage);
	}

	/**
	 * handle the forwarding of the message to the correct handler for processing and forwarding.
	 * @method handleMqttMessage
	 * @public
	 * @param {IMqttMessage} mqttMessage The received message.
	 */
	public handleMqttMessage = (mqttMessage: IMqttMessage): Promise<void> => {
		Logger.silly('MqttMessageHandler.handleMqttMessage : Handling message.');
		switch (mqttMessage.mqttPayload.messageType) {
			case MqttMessageType.WHO :
				Logger.silly('MqttMessageHandler.handleMqttMessage : Received who message.');
				this.mqttStatusHandler.handleWhoMessage(mqttMessage);
				break;
			case MqttMessageType.PRESENCE :
				Logger.silly('MqttMessageHandler.handleMqttMessage : Received presence message.');
				this.mqttStatusHandler.handlePresenceMessage(mqttMessage);
				break;
			case MqttMessageType.COMMAND:
				Logger.silly('MqttMessageHandler.handleMqttMessage : Received command message.');
				this.mqttCommandHandler.handleCommand(mqttMessage);
				break;
			case MqttMessageType.INFORMATION:
				Logger.silly('MqttMessageHandler.handleMqttMessage : Received command message.');
				this.mqttInformationHandler.handleInformation(mqttMessage);
				break;
			default :
				Logger.silly('MqttMessageHandler.handleMqttMessage : Received unknown message type : %0', mqttMessage);
		}
		return;
	}
}