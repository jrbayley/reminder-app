import _ from 'lodash';
import moment from 'moment';

import { Listeners } from '../../listener/Listeners';
import { Logger } from '../../logger/Logger';
import { IMqttMessage } from './interfaces/IMqttMessage';
import { MqttPresence } from './interfaces/MqttPresence';

export class MqttPresenceHandler {

	/** @var {MqttPresence} mqttPresence The list of currently online users */
	private mqttPresences : MqttPresence[];

	constructor() {
		this.mqttPresences = [];
		Listeners.mqttListener.mqttPresenceReceived.subscribe(this.addNewPresence);
		setInterval(() => {
			this.removeStalePresences();
		}, 30000);
	}

	public addNewPresence = (mqttMessage: IMqttMessage): Promise<void> => {
		Logger.silly('MqttPresenceHandler.addNewPresence : Received presence');
		if (mqttMessage) {
			if (this.mqttPresences.length >= 1000) {
				this.mqttPresences.pop();
			}
			Logger.silly('MqttPresenceHandler.addNewPresence : Storing presence of host %s', mqttMessage.mqttPayload.source);
			const mqttPresence: MqttPresence = new MqttPresence(mqttMessage.mqttPayload.source, mqttMessage.mqttPayload.user);
			_.remove(this.mqttPresences, {mqttHost: mqttPresence.mqttHost});
			this.mqttPresences.push(mqttPresence);
		}
		Listeners.mqttListener.presenceListUpdated.publish(this.mqttPresences);
		return;
	}

	private removeStalePresences = (): void => {
		const removed = _.remove(this.mqttPresences, (mqttPresence: MqttPresence) => {
			Logger.silly('MqttPresenceHandlerAge.removeStalePresences : Age is %d', moment().diff(mqttPresence.lastUpdated, 'seconds'));
			return (moment().diff(mqttPresence.lastUpdated, 'seconds') > 30);
		});
		if (removed.length > 0) {
			Logger.silly('MqttPresenceHandler.removeStalePresences : Removed %o', removed);
		}
		Listeners.mqttListener.presenceListUpdated.publish(this.mqttPresences);
	}
}

