/**
 * The mqtt message types that may be sent
 * @enum
 */

export enum MqttMessageType {
	WHO,
	PRESENCE,
	MESSAGE,
	COMMAND,
	INFORMATION,
	CONNECTION
}