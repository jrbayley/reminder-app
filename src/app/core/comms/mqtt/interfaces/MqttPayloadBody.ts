import { MqttCommand } from './MqttCommand';

/**
 * The body of the mqtt message payload
 * @interface
 */

export interface MqttPayloadBody {

	/** @var {MqttCommand} command The optional message command. */
	command? : MqttCommand;

	/** @var {string} data The optional message data. */
	data? : string;
}