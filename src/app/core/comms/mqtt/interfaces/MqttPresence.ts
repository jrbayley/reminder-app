import moment from 'moment';

import { IMqttPresence } from './IMqttPresence';

/**
 * The implementation of the presence recording and tracking
 * @class
 * @implements {IMqttPresence}
 */

export class MqttPresence implements IMqttPresence {

	/** @var {string} user The user that is sending the presence message */
	user? : string;

	/** @var {string} userInitial The users Initial */
	userInitial? : string;

	/** @var {string} mqttHost The host that is sending the presence message */
	mqttHost : string;

	/** @var {moment.Moment} lastUpdated The time that this presence entry was last updated */
	lastUpdated : moment.Moment;

	/**
	 * @constructor
	 * @param {string} mqttHost The hostname of the host sending the presence
	 */
	constructor(mqttHost: string, user: string) {
		this.user = user;
		this.userInitial = user.substr(0, 1).toUpperCase();
		this.mqttHost = mqttHost;
		this.refreshLastUpdated();
	}

	/**
	 * Refresh the last updated time of the presence
	 * @method refreshLastUpdated
	 * @public
	 */
	public refreshLastUpdated = (): void => {
		this.lastUpdated = moment();
	}

	/**
	 * Return the class data as an interface / json
	 * @method asInterface
	 * @public
	 */
	public asInterface = (): IMqttPresence => {
		return {
			user : this.user,
			userInitial : this.userInitial,
			mqttHost : this.mqttHost,
			lastUpdated: this.lastUpdated
		};
	}
}