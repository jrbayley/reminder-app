import moment from 'moment';

/**
 * Interface for mqtt presence management on the local system
 * @interface
 */

export interface IMqttPresence {

	/** @var {string} user? The user that is sending the presence message */
	user? : string;

	/** @var {string} userInitial? The userInitial that is sending the presence message */
	userInitial? : string;

	/** @var {string} mqttHost The host that is sending the presence message */
	mqttHost : string;

	/** @var {moment.Moment} lastUpdated The time that this presence entry was last updated */
	lastUpdated : moment.Moment;
}