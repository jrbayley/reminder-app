import { MqttStatus } from './MqttStatus';

/**
 * Mqtt status change interface
 * @interface
 */

export interface IMqttStatusUpdate {

	/** @var {MqttStatus} status The current mqtt status */
	status : MqttStatus;

	/** @var {MqttStatus} previousStatus The optional previous mqtt status */
	previousStatus? : MqttStatus;

	/** @var {string} hostname The optional hostname that the user is online as */
	hostname? : string;
}