import { MqttMessageType } from './MqttMessageType';
import { MqttPayloadBody } from './MqttPayloadBody';

/**
 * The mqtt payload for sending onto a topic
 * @interface
 */

export interface IMqttPayload {

	/** @var {string} id The MQTT Messsage id */
	id : string;

	/** @var {string} user The optional user sending the message */
	user? : string;

	/** @var {MqttMessageType} messageType The type of message to send */
	messageType : MqttMessageType;

	/** @var {string} source The source host sending the message */
	source : string;

	/** @var {string} destination The destination host to receive the message */
	destination : string;

	/** @var {string} source The source host sending the message */
	body : MqttPayloadBody;
}