import { IMqttPayload } from './IMqttPayload';

/**
 * Mqtt message sending interface
 * @interface
 */

export interface IMqttMessage {

	/** @var {string} timestamp The mqtt timestamp  */
	timestamp? : string;

	/** @var {string} topic The mqtt topic to post to */
	topic : string;

	/** @var {IMqttPayload} mqttPayload The mqtt payload to post to the topic */
	mqttPayload : IMqttPayload;

	/** @var {any} returnCall The optional return function */
	returnCall? : any;
}