/**
 * The mqtt message types that may be sent
 * @enum
 */

export enum MqttStatus {
	POWEREDOFF = 'PoweredOff',
	DISCONNECTED = 'Disconnected',
	CONNECTING = 'Connecting',
	CONNECTED = 'Connected',
	AUTHENTICATING = 'Authenticating',
	AUTHENTICATED = 'Authenticated',
	ONLINE = 'Online',
	OFFLINE = 'Offline',
	RECONNECTING = 'Reconnecting',
	ERROR = 'Error'
}