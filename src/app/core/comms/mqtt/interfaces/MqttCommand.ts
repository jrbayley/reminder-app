/**
 * The list of available Mqtt commands.
 * Commands help the mqtt processing to route the payload to the correct method
 * @enum
 */

export enum MqttCommand {
	currentLoad = 'currentLoad',
	processes = 'processes',
	memory = 'memory',
	restartFireDaemonProcess = 'restartFireDaemonProcess',
	startFireDaemonProcess = 'startFireDaemonProcess',
	stopFireDaemonProcess = 'stopFireDaemonProcess',
	listFireDaemonProcesses = 'listFireDaemonProcesses'
}
