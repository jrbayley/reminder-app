import { Listeners } from '../../listener/Listeners';
import { IMqttMessage } from './interfaces/IMqttMessage';
import { MqttPayloadUtils } from './MqttPayloadUtils';

/**
 * Handle the status messages such as presence and who's online
 * @class
 */

export class MqttStatusHandler {

	/**
	 * @constructor
	 */
	constructor () {
		//
	}

	/**
	 * Handle the receipt of a who message by sending our presence
	 * @method handleWhoMessage
	 * @public
	 * @param {IMqttMessage} mqttMessage The who message
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public handleWhoMessage = (mqttMessage: IMqttMessage): Promise<void> => {
		return Listeners.mqttListener.mqttMessageSend.publish(MqttPayloadUtils.broadcastPresence(mqttMessage.topic));
	}

	/**
	 * Handle the receipt of a presence message
	 * @method handlePresenceMessage
	 * @public
	 * @param {IMqttMessage} mqttMessage The presence message
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public handlePresenceMessage = (mqttMessage: IMqttMessage): Promise<void> => {
		return Listeners.mqttListener.mqttPresenceReceived.publish(mqttMessage);
	}
}