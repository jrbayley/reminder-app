import { IMqttUserAndHost } from './interfaces/IMqttUserAndHost';

/**
 * Create a new mqtt payload from the passed paramaters.
 * @class
 */

export class MqttMessageUtils {

	/**
	 * Separate the oser and host components.
	 * @public
	 * @static
	 * @method separateUserAndHost
	 * @returns {IMqttUserAndHost} The user and host data
	 */
	public static separateUserAndHost = (userAndHost: string): IMqttUserAndHost => {
		let mqttUserAndHost: IMqttUserAndHost = undefined;
		const parts = userAndHost.split('::');
		if (parts.length === 2) {
			const user = parts[0];
			const host = parts[1];
			mqttUserAndHost = {user: user, host: host};
		}
		return mqttUserAndHost;
	}
}