import { Logger } from '../../logger/Logger';
import { IMqttMessage } from './interfaces/IMqttMessage';

/**
 * Handle the processing and forwarding of any command messages.
 * @class
 */

export class MqttCommandHandler {

	/**
	 * @constructor
	 */
	constructor() {
		//
	}

	/**
	 * handle the forwarding of the command message to the correct function according to the command.
	 * @method handleCommand
	 * @public
	 * @param {IMqttMessage} mqttMessage The received message.
	 */
	public handleCommand = (mqttMessage: IMqttMessage): void => {
		Logger.debug('MqttCommandHandler.handleCommand : Received command message.');
		if (mqttMessage.mqttPayload && mqttMessage.mqttPayload.body && mqttMessage.mqttPayload.body.command) {
			switch (mqttMessage.mqttPayload.body.command) {
				//
			}
		}
	}
}