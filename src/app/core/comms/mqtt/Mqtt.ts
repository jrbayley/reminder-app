import { MqttClient } from './MqttClient';
import { MqttMessageHandler } from './MqttMessageHandler';
import { MqttPresenceHandler } from './MqttPresenceHandler';

/**
 * Module container class for all mqtt functions
 * @class
 */

export class Mqtt {

	/** @var {MqttClient} mqttClient The mqtt client class */
	public mqttClient : MqttClient;

	/** @var {MqttMessageHandler} mqttMessageHandler The mqtt message handler class */
	public mqttMessageHandler : MqttMessageHandler;

	/** @var {MqttPresenceHandler} mqttPresenceHandler The mqtt presence handler class */
	public mqttPresenceHandler : MqttPresenceHandler;
	/**
	 * Initialise the mqtt classes.
	 * @constructor
	 */
	constructor() {
		this.mqttClient = new MqttClient();
		this.mqttMessageHandler = new MqttMessageHandler();
		this.mqttPresenceHandler = new MqttPresenceHandler();
	}
}