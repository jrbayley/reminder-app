import { IMqttMessage } from './interfaces/IMqttMessage';
import { IMqttPayload } from './interfaces/IMqttPayload';
import { MqttMessageType } from './interfaces/MqttMessageType';
import { MqttPayloadBody } from './interfaces/MqttPayloadBody';
import { MqttPayload } from './MqttPayload';

import uuid = require('uuid/v4');

/**
 * Create a new mqtt payload from the passed paramaters.
 * @class
 */

export class MqttPayloadUtils {

	/** @var {string} username The username to use when sending mqtt messages */
	private static username : string;

	/** @var {string} hostname The hostname to use when sending mqtt messages */
	private static hostname : string;

	/**
	 * Create a new mqtt payload from the passed paramaters.
	 * @public
	 * @static
	 * @method createPayload
	 * @param {MqttMessageType} messageType The message type to send.
	 * @param {sting} destination The destination host to send the message to
	 * @param {MqttPayloadBody} body The payload body to send
	 * @returns {IMqttPayload} The payload interface / json
	 */
	public static createPayload = (messageType: MqttMessageType, destination: string, body: MqttPayloadBody): IMqttPayload => {
		return new MqttPayload({
			id: uuid(),
			user: MqttPayloadUtils.username,
			messageType: messageType,
			source: MqttPayloadUtils.hostname,
			destination: destination,
			body: body
		});
	}

	/**
	 * Set the Mqtt client username for this user
	 * @method
	 * @public
	 * @static
	 * @param {string} username The username to use for any payloads.
	 */
	public static setMqttUsername = (username: string): void => {
		MqttPayloadUtils.username = username;
	}


	/**
	 * Set the Mqtt client username for this user
	 * @method
	 * @public
	 * @static
	 * @param {string} hostname The hostname to use for any payloads.
	 */
	public static setMqttHostname = (hostname: string): void => {
		MqttPayloadUtils.hostname = hostname;
	}
	/**
	 * Create and return a presence message
	 * @method
	 * @public
	 * @static
	 * @param {string} topic The topic name to send to
	 * @returns {IMqttMessage} The mqtt message as json
	 */
	public static broadcastPresence = (topic: string): IMqttMessage => {
		const mqttPayload =  MqttPayloadUtils.createPayload(MqttMessageType.PRESENCE, '*', {});
		return { topic: topic, mqttPayload: mqttPayload };
	}

	/**
	 * Create and return a who message to prompt hosts to return a presence message.
	 * @method
	 * @public
	 * @static
	 * @param {string} topic The topic name to send to
	 * @returns {IMqttMessage} The mqtt message as json
	 */
	public static broadcastWho = (topic: string): IMqttMessage => {
		const mqttPayload =  MqttPayloadUtils.createPayload(MqttMessageType.WHO, '*', {});
		return { topic: topic, mqttPayload: mqttPayload };
	}
}