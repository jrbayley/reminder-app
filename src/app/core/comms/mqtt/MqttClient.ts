import moment from 'moment';
import mqtt, { IClientOptions } from 'mqtt';

import { LoggedInUser } from '../../appData/LoggedInUser';
import { Configuration } from '../../config/Configuration';
import { Device } from '../../device/Device';
import { Listeners } from '../../listener/Listeners';
import { Logger } from '../../logger/Logger';
import { TimeUtils } from '../../utils/TimeUtils';
import { IMqttMessage } from './interfaces/IMqttMessage';
import { IMqttPayload } from './interfaces/IMqttPayload';
import { IMqttStatusUpdate } from './interfaces/IMqttStatusUpdate';
import { MqttStatus } from './interfaces/MqttStatus';
import { MqttPayloadUtils } from './MqttPayloadUtils';

export class MqttClient {

	/** @var {mqtt.Client} mqttClient Reference to the mqtt client */
	private mqttClient : mqtt.Client;

	/** @var {string} username The logged in username */
	private username : string;

	/** @var {string} password The password  */
	private password : string;

	/** @var {number} retryCount The number of times the client has retried  */
	private retryCount : number;

	/** @var {IMqttStatusUpdate} mqttStatusUpdate The last status update  */
	private mqttStatusUpdate : IMqttStatusUpdate;

	/**
	 * Initialise the listeners that the client attaches to
	 * @constructor
	 */
	constructor() {
		this.retryCount = 0;
		this.mqttStatusUpdate = {status: MqttStatus.POWEREDOFF, previousStatus: MqttStatus.POWEREDOFF, hostname: ''};
		this.updateStatus(MqttStatus.DISCONNECTED);
		Listeners.authenticationListener.userSignin.subscribe(this.onAuthenticated);
		Listeners.mqttListener.mqttPresenceSend.subscribe(this.sendWho);
		Listeners.mqttListener.mqttMessageSend.subscribe(this.publishToTopic);
	}

	/**
	 * When the user is authenticated start the Mqtt connection process
	 * @method onAuthenticated
	 * @private
	 * @async
	 * @param {LoggedInUser} loggedInUser The user that has logged in
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onAuthenticated = (loggedInUser: LoggedInUser): Promise<void> => {
		this.username = loggedInUser.username;
		this.password = loggedInUser.password;
		this.mqttStatusUpdate.hostname = this.username + '-' + moment().unix();
		MqttPayloadUtils.setMqttUsername(this.username);
		MqttPayloadUtils.setMqttHostname(this.mqttStatusUpdate.hostname);
		this.updateStatus(MqttStatus.AUTHENTICATING);
		this.connectStandard();
		return;
	}


	/**
	 * Initiate the standard Mqtt connection.
	 * @method connectStandard
	 * @private
	 */
	private connectStandard = () => {
		Logger.silly('MqttClient.connectStandard : Starting Mqtt');
		this.updateStatus(MqttStatus.CONNECTING);
		try {
			const url = Configuration.MQTT_HOST;
			const clientId: string = this.username + '-' + Math.random().toString(16).substr(2, 8);
			const configOptions: IClientOptions = {clientId: clientId, reconnectPeriod: 10000, username: this.username, password: this.password};
			this.mqttClient = mqtt.connect(url, configOptions);
			this.updateStatus(MqttStatus.CONNECTED);
			this.initialiseMqttEvents();
		} catch (e) {
			Logger.error('MqttClient.connectStandard : Unable to connect to IoT : ' + e);
		}
	}

	/**
	 * Initiate the listeners that will be fired from the mqtt client.
	 * @method initialiseMqttEvents
	 * @private
	 */
	private initialiseMqttEvents = (): void => {
		Logger.silly('MqttClient.initialiseMqttEvents : Initialising Mqtt event listeners.');
		this.mqttClient.on('connect', this.onConnect);
		this.mqttClient.on('message', this.onMessage);
		this.mqttClient.on('error', this.onError);
		this.mqttClient.on('reconnect', this.onReconnect);
		this.mqttClient.on('close', this.onClose);
	}

	/**
	 * Handle receipt of the request to subscribe to a topic.
	 * @method subscribeToTopic
	 * @private
	 * @async
	 * @param {string} topic The topic to subscribe to
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private subscribeToTopic = (topic: string): Promise<void> => {
		return new Promise((resolve, reject) => {
			this.mqttClient.subscribe(topic, (err: Error) => {
				if (err) {
					Logger.silly('MqttClient.subscribeToTopic : Unable to subscribe to ' + topic + ' : ' + err.message);
					reject();
				} else {
					Logger.silly('MqttClient.subscribeToTopic : Subscribed to ', topic);
					resolve();
				}
			});
		});
	}

	/**
	 * Publish a message to a topic.
	 * @method publishToTopic
	 * @private
	 * @async
	 * @param {IMqttMessage} mqttMessage The message to publish
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private publishToTopic = (mqttMessage: IMqttMessage): Promise<void> => {
		Logger.silly('MqttClient.publishToTopic : %s : %O', mqttMessage.topic, JSON.stringify(mqttMessage.mqttPayload));
		this.mqttClient.publish(mqttMessage.topic, JSON.stringify(mqttMessage.mqttPayload));
		return;
	}

	/**
	 * The method that will be called when there is a connection to the mqtt service.
	 * @method onConnect
	 * @private
	 * @async
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onConnect = async (): Promise<void> => {
		Logger.silly('MqttClient.onConnect : Mqtt Connected');
		this.updateStatus(MqttStatus.CONNECTED);
		this.onlineNotification();
		try {
			await this.subscribeToTopic(Configuration.MQTT_PRESENCE_PATH);
			await this.subscribeToTopic(Configuration.MQTT_NODE_PATH + this.mqttStatusUpdate.hostname);
			setInterval(() => {
				this.sendWho();
			}, 30000);
			return this.sendWho();
		}
		catch (error) {
			Logger.error('MqttClient.onConnect : Error connecting to Mqtt');
		}
	}

	/**
	 * The method that will be called when a message is received. The message is unpacked and passed on to the app message listener.
	 * @method onMessage
	 * @private
	 * @param {string} topic The topic that the message was received from.
	 * @param {string} message The message that was received in string form.
	 */
	private onMessage = (topic: string, message: string): void => {
		Logger.silly('MqttClient.onMessage : Received message from topic - %s', topic);
		const mqttPayload: IMqttPayload = JSON.parse(message);
		const mqttMessage: IMqttMessage = { topic: topic,
											timestamp: TimeUtils.momentAsHumanDate(moment()),
											mqttPayload : mqttPayload
											};
		Listeners.mqttListener.mqttMessageReceived.publish(mqttMessage);
	}

	/**
	 * The method that will be called when a message is received. The message is unpacked and passed on to the app message listener.
	 * @method onError
	 * @private
	 * @async
	 * @param {string} topic The topic that the message was received from.
	 * @param {string} message The message that was received in string form.
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onError = (error: Error): Promise<void> => {
		Logger.error('MqttClient.onError : Mqtt error : %o', error);
		return this.updateStatus(MqttStatus.ERROR);
	}

	/**
	 * The method that will be called when the connection to mqtt closes.
	 * @method onClose
	 * @private
	 * @async
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onClose = (): Promise<void> => {
		Logger.error('MqttClient.onClose : Mqtt Offline');
		return this.updateStatus(MqttStatus.OFFLINE);
	}

	/**
	 * The method that will be called when the connection to mqtt closes.
	 * @method onReconnect
	 * @private
	 * @async
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onReconnect = (): Promise<void> => {
		this.retryCount++;
		if (this.retryCount > Configuration.MQTT_RETRY_LIMIT) {
			this.mqttClient.end();
		} else {
			Logger.error('MqttClient.onReconnect : Mqtt Reconnected Retry: %d', this.retryCount);
			return this.updateStatus(MqttStatus.RECONNECTING);
		}
	}

	/**
	 * Display the online notification.
	 * @method onlineNotification
	 * @private
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onlineNotification = (): void => {
		// UiNotification.notificationWithClose('Reminders Online', 'You are online and connected to ReminderNet');
		if (Device && Device.cordova && Device.cordova.nativeAudio) {
			Device.cordova.nativeAudio.preloadSimple('online', 'audio/computerbeep_3.mp3')
			.then(() => {
				Device.cordova.nativeAudio.play('online');
			});
		}
		this.updateStatus(MqttStatus.ONLINE);
	}

	/**
	 * Send a who message to the mqtt service.
	 * @method sendWho
	 * @private
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private sendWho = (): Promise<void> => {
		Logger.silly('MqttClient.sendWho : Sending who request');
		return Listeners.mqttListener.mqttMessageSend.publish(MqttPayloadUtils.broadcastWho(Configuration.MQTT_PRESENCE_PATH));
	}

	/**
	 * Update the mqttStatus object with any changes.
	 * @method updateStatus
	 * @private
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private updateStatus = (status: MqttStatus, hostname?: string): Promise<void> => {
		Logger.silly('MqttClient.updateStatus : Changing status from ' + this.mqttStatusUpdate.previousStatus + ' to ' + status);
		this.mqttStatusUpdate.previousStatus = this.mqttStatusUpdate.status;
		this.mqttStatusUpdate.status = status;
		this.mqttStatusUpdate.hostname = (hostname) ? hostname : this.mqttStatusUpdate.hostname;
		return Listeners.mqttListener.mqttStatus.publish(this.mqttStatusUpdate);
	}
}