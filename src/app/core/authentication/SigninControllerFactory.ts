import { ISigninController } from './ISigninController';
import { SigninController } from './reminder/SigninController';
import { SigninControllerType } from './SigninControllerType';

/**
 * Factory class to return the required signin controller
 * @class
 */

export class SigninControllerFactory {

	/**
	 * Initialise the signin controller according to the configured type
	 * @method init
	 * @public
	 * @static
	 * @param {SigninControllerType} signinType The configured sign in type
	 * @returns {ISigninController} Returns the newly instantiated sign in controller
	 */
	public static init = (signinType: SigninControllerType): ISigninController => {
		switch (signinType) {
			case SigninControllerType.STANDARD :
				return new SigninController();
			default :
				return new SigninController();
		}
	}
}