/**
 * Create the options for sign in controllers.
 * @enum
 */

export enum SigninControllerType {
	STANDARD,
	COGNITO
}