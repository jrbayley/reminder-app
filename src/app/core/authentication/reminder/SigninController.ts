import { LoggedInUser } from '../../appData/LoggedInUser';
import { Comms } from '../../comms/Comms';
import { Configuration } from '../../config/Configuration';
import { Listeners } from '../../listener/Listeners';
import { Logger } from '../../logger/Logger';
import { ISigninController } from '../ISigninController';
import { ISigninRequest } from '../ISigninRequest';
import { IUserValidationResponse } from '../models/IUserValidationResponse';
import { SigninStage } from '../models/SigninStage';
import { SigninRequest } from '../SigninRequest';

/**
 * Sign in the user via the little reminder servers.
 * @class
 * @implements {ISigninController}
 */

export class SigninController implements ISigninController {

	/**
	 * @constructor
	 */
	constructor () {
		//
	}

	/**
	 * Process the signing in of the user
	 * @method signin
	 * @public
	 * @async
	 * @param {string} username The username to sign in with
	 * @param {string} password The password to sign in with
	 * @returns {Promise<void>} The sign in response from the server.
	 */
	public signin = async (username: string, password: string, confirmationCode?: string): Promise<void> => {
		const signinRequest: ISigninRequest = new SigninRequest(username, password).asInterface();
		if (confirmationCode) {
			signinRequest.confirmation_code = confirmationCode;
		}
		const url = Configuration.REST_HOST + Configuration.REST_SIGN_IN;
		const userValidationResponse: IUserValidationResponse = await Comms.restRequest.restPost(url, signinRequest);
		if (userValidationResponse) {
			if (userValidationResponse.success) {
				Logger.debug('SigninController.signin : Signin Success.');
				Listeners.authenticationListener.authenticationListener.publish(userValidationResponse);
			} else if (userValidationResponse.message === 'Email not verified') {
				Logger.debug('SigninController.signin : Account verification required.');
				Listeners.authenticationListener.signinStage.publish(SigninStage.VERIFY);
			} else {
				Logger.debug('SigninController.signin : Signin Invalid.');
				Listeners.authenticationListener.signinStage.publish(SigninStage.START);
			}
		} else {
			Logger.debug('SigninController.signin : Signin Failed.');
			Listeners.authenticationListener.signinStage.publish(SigninStage.FAILED);
		}
	}

	/**
	 * @method checkToSeeIfUserIsValid
	 * @public
	 * Check to see if there are valid credentials and if so carry out cognito login
	 * @param {LoggedInUser} loggedInUser The current logged in user from storage or cognito
	 * @returns {boolean} True if the credentials are not valid or not present
	 */
	public checkToSeeIfUserIsValid = (loggedInUser: LoggedInUser): boolean => {
		return loggedInUser && loggedInUser.valid();
	}
}