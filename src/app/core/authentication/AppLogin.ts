import { AppData } from '../../core/appData/AppData';
import { ILoggedInUser } from '../appData/ILoggedInUser';
import { LoggedInUser } from '../appData/LoggedInUser';
import { IUserValidationResponse } from '../authentication/models/IUserValidationResponse';
import { Configuration } from '../config/Configuration';
import { Device } from '../device/Device';
import { Listeners } from '../listener/Listeners';
import { Logger } from '../logger/Logger';
import { Biometric } from './Biometric';
import { ISigninController } from './ISigninController';
import { SigninStage } from './models/SigninStage';
import { SigninControllerFactory } from './SigninControllerFactory';

/**
 * The applogin class for handling sign in to the all and triggering UI controls
 * @class
 */

export class AppLogin {

	/** @var {Biometric} biometric the biometrics instance for using device security */
	private biometric : Biometric;

	/** @var {SigninController} signinController the external sign in handler */
	private signinController : ISigninController;

	/** @var {string} username Stored username for use in dealing with delayed responses */
	private username : string;

	/** @var {string} password Stored password for use in dealing with delayed responses */
	private password : string;

	/** @var {string} confirmtaionCode Stored confirmation code for use in dealing with delayed responses */
	private confirmtaionCode : string;

	/**
	 * Initialise the app security and login features
	 * @constructor
	 */
	constructor () {
		this.signinController = SigninControllerFactory.init(Configuration.SIGNIN_TYPE);
		this.biometric = new Biometric();
		Listeners.authenticationListener.signinStage.publish(SigninStage.START);
		Listeners.authenticationListener.userSignout.subscribe(this.onSignout);
		Listeners.deviceListener.deviceResume.subscribe(this.onResume);
		this.check();
	}

	/**
	 * Check to see if login or biometrics is to be shown.
	 * @method check
	 * @public
	 */
	public check = (): void => {
		const loggedInUser = AppData.currentUser.getLoggedInUser();
		if (loggedInUser && loggedInUser.valid()) {
			Logger.debug('AppLogin.check : Login using stored credentials.');
			const signInRequired: boolean = ! this.signinController.checkToSeeIfUserIsValid(loggedInUser);
			if (signInRequired) {
				// We have no local data so display login form
				Listeners.authenticationListener.signinStage.publish(SigninStage.START);
			} else {
				this.showBioOrAppLogin(loggedInUser);
			}
		} else {
			Listeners.authenticationListener.signinStage.publish(SigninStage.START);
		}
	}


	/**
	 * Restart login after resume.
	 * @method onResume
	 * @public
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public onResume = (): Promise<void> => {
		Logger.debug('AppLogin.onResume : Starting resume sign in.');
		const loggedInUser = AppData.currentUser.getLoggedInUser();
		if (loggedInUser && loggedInUser.valid()) {
			this.signin(loggedInUser.username, loggedInUser.password);
		} else {
			this.check();
		}
		return;
	}

	/**
	 * Check and initiate the appropriate sign in calls depending in the device and credentials
	 * @method showBioOrAppLogin
	 * @private
	 * @param {LoggedInUser} loggedInUser The current logged in user from storage or cognito
	 */
	private showBioOrAppLogin = (loggedInUser: LoggedInUser): void => {
		if (Device.localDevice.isMobile()) {
			Logger.debug('AppLogin.showBioOrAppLogin : Mobile device');
			this.showBioLogin(loggedInUser);
		} else {
			Logger.debug('AppLogin.showBioOrAppLogin : Not a mobile device')
			if (loggedInUser) {
				this.signin(loggedInUser.username, loggedInUser.password);
			} else {
				Listeners.authenticationListener.signinStage.publish(SigninStage.START);
			}
		}
	}

	/**
	 * Initialise and trigger the biometrocs if possible
	 * @method showBioLogin
	 * @private
	 * @param {LoggedInUser} loggedInUser The current logged in user from storage or cognito
	 */
	private showBioLogin = (loggedInUser: LoggedInUser): void => {
		this.biometric.isAvailable()
		.then((isAvailable: boolean) => {
			Logger.debug('AppLogin.showBioLogin : Bio : %o', isAvailable);
			if (isAvailable) {
				this.startBioLogin(loggedInUser);
			} else {
				Listeners.authenticationListener.signinStage.publish(SigninStage.START);
			}
		})
		.catch((error: Error) => {
			Logger.error('AppLogin.showBioLogin : Bio login check error : ', error.message);
			Listeners.authenticationListener.signinStage.publish(SigninStage.START);
		});
	}
	/**
	 * Initialise and trigger the biometrocs if possible
	 * @method startBioLogin
	 * @private
	 * @param {LoggedInUser} loggedInUser The current logged in user from storage or cognito
	 */
	private startBioLogin = (loggedInUser: LoggedInUser): void => {
		Listeners.authenticationListener.signinStage.publish(SigninStage.LOGIN_FROM_BIO);
		this.biometric.show()
		.then((signedIn: boolean) => {
			if (signedIn) {
				this.signin(loggedInUser.username, loggedInUser.password);
			} else {
				Listeners.authenticationListener.signinStage.publish(SigninStage.START);
			}
		})
		.catch((error: Error) => {
			Logger.error('AppLogin.startBioLogin : Bio login error : ', error.message);
			Listeners.authenticationListener.signinStage.publish(SigninStage.START);
		});
	}

	/**
	 * Start the sign in with cognito
	 * @method signin
	 * @private
	 * Start the sign in with cognito
	 * @param {string} username The username to sign in with
	 * @param {string} password The passwword to sign in with
	 * @returns {Promise<void>} Returns an empty promise
	 */
	public signin = (username: string, password: string, confirmationCode?: string): Promise<void> => {
		this.username = username;
		this.password = password;
		Listeners.authenticationListener.authenticationListener.subscribe(this.onSigninSuccess);
		return this.signinController.signin(username, password, confirmationCode);
	}


	/**
	 * If there was a signin success this method will be called.
	 * @method onSigninSuccess
	 * @private
	 * @param {string} username The username to sign in with
	 * @param {string} password The passwword to sign in with
	 * @param {IUserValidationResponse} signinResponse The signin response from the server
	 */
	private onSigninSuccess = (signinResponse: IUserValidationResponse): Promise<void> => {
		const loggedInUserData: ILoggedInUser = {
			username: this.username,
			password: this.password,
			token: signinResponse.token,
			user: signinResponse.user
		};
		const loggedInUser: LoggedInUser = new LoggedInUser(loggedInUserData);
		this.username = undefined;
		this.password = undefined;
		AppData.currentUser.setLoggedInUser(loggedInUser);
		Listeners.authenticationListener.signinStage.publish(SigninStage.COMPLETE);
		Listeners.authenticationListener.userSignin.publish(loggedInUser);
		return;
	}

	/**
	 * Sign out the user and clear the stored and UI credentials.
	 * @method onSignout
	 * @private
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onSignout = (): Promise<void> => {
		AppData.currentUser.clearLoggedInUser();
		return;
	}
}
