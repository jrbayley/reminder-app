import { LoggedInUser } from '../appData/LoggedInUser';

/**
 * @interface
 * Sign in the user.
 */

export interface ISigninController {

	/**
	 * Process the signing in of the user
	 * @method signin
	 * @public
	 * @async
	 * @param {string} username The username to sign in with
	 * @param {string} password The password to sign in with
	 * @returns {Promise<void>} The sign in response from the server.
	 */
	signin  (username: string, password: string, confirmationCode?: string): Promise<void>;

	/**
	 * @method checkToSeeIfUserIsValid
	 * @public
	 * Check to see if there are valid credentials and if so carry out cognito login
	 * @param {LoggedInUser} loggedInUser The current logged in user from storage or cognito
	 * @returns {boolean} True if the credentials are not valid or not present
	 */
	checkToSeeIfUserIsValid  (loggedInUser: LoggedInUser): boolean;
}