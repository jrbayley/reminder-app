
/**
 * @class
 * Biometric authentication utility.
 * Provides interfaces to the OS Biometric authentication components via cordova.
 */

export class Biometric {

	/**
	 * Test to see if biometric functions are available on this device.
	 * @method isAvailable
	 * @public
	 * @async
	 * @returns {Promise<boolean>} Returns a promise containing a boolean true for biometric available false for not available
	 */
	public isAvailable = (): Promise<boolean> => {
		return new Promise((resolve, _reject) => {
			Fingerprint.isAvailable(() => {
				resolve(true);
			}, (_error: string) => {
				resolve(false);
			});
		});
	}
	/**
	 * Use the deveice biometric authentication functions to authentocate the user.
	 * @method show
	 * @public
	 * @async
	 * @returns {Promise<boolean>} Returns a promise containing a boolean true for authenticated false for not authenticated
	 */
	public show = (): Promise<boolean> => {
		return new Promise((resolve, _reject) => {
			Fingerprint.show({
				description: 'Reminder Sign in',
			}, () => {
				resolve(true);
			}, (_error: string) => {
				resolve(false);
			});
		});
	}
}