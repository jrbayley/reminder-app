import { IUser } from './IUser';

/**
 * The user validation response from the server when signing in
 * @interface
 */

export interface IUserValidationResponse {
	
	/** @var {boolean} success Whether the signin was a success */
	success : boolean;

	/** @var {string} message The response message from the server */
	message : string;

	/** @var {string} username The username entered */
	username : string;

	/** @var {string} password The password entered */
	password : string;

	/** @var {string} token The JWT for reauth and to access secure server services */
	token : string;

	/** @var {IUser} user The user details for customisation */
	user : IUser | undefined;
}