/**
 * The stages of signin that the listener will use
 * @enum
 */

export enum SigninStage {
	START,
	LOGIN_FROM_FORM,
	LOGIN_FROM_BIO,
	FAILED,
	COMPLETE,
	RECOVER,
	RECOVERED,
	NEW_PASSWORD,
	REGISTER,
	VERIFY
}