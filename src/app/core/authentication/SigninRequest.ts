import { ISigninRequest } from './ISigninRequest';

/**
 * Sign in request instance
 * @class
 */

export class SigninRequest implements ISigninRequest {

	/** @var {string} username The signin username */
	public username : string;

	/** @var {string} password The signin password */
	public password : string;

	/** @var {string} confirmationCode The signin confirmation code if present */
	public confirmationCode : string;

	/**
	 * @constructor
	 * @param {string} username The sign in username
	 * @param {string} password The sign in password
	 */
	constructor (username: string, password: string, confirmationCode?: string) {
		this.username = username;
		this.password = password;
		this.confirmationCode = confirmationCode;
	}

	/**
	 * Return the signin request as an interface / json
	 * @method asInterface
	 * @public
	 * @returns {ISigninRequest} Returns a sign in request interface with the data
	 */
	public asInterface = (): ISigninRequest => {
		return {
			username: this.username,
			password: this.password,
			confirmation_code: this.confirmationCode
		};
	}
}