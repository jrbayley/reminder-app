import { AppLogin } from './AppLogin';

/**
 * Create and initialise the local authentication classes
 * @class
 */

export class Authentication {

	/** @var {AppLogin} appLogin The application login controls */
	public static appLogin : AppLogin;

	/**
	 * Initialise the local login components
	 * @method init
	 * @public
	 * @static
	 */
	public static init = (): void => {
		Authentication.appLogin = new AppLogin();
	}
}