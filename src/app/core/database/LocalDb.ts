/**
 * Methods for handling the data into and out of the localstorage
 * @class
 */

export class LocalDb {

	/**
	 * @constructor
	 */
	constructor () {
		//
	}

	/**
	 * Get an item from the localstorage
	 * @method getItem
	 * @public
	 * @param {string} key The key of the item to retrieve
	 * @returns {string} The content of the stored item
	 */
	public getItem = (key: string): string => {
		return localStorage.getItem(key) || '';
	}

	/**
	 * Store data to localstorage using the provided key.
	 * @method setItem
	 * @public
	 * @param {string} key The key of the item to store
	 * @param {string} value The string value to store to localstorage
	 */
	public setItem = (key: string, value: string): void => {
		localStorage.setItem(key, value);
	}

	/**
	 * Remove data from localstorage using the provided key.
	 * @method removeItem
	 * @public
	 * @param {string} key The key of the item to remove
	 */
	public removeItem = (key: string): void => {
		localStorage.removeItem(key);
	}

	/**
	 * Clear all local storage data.
	 * @method clearData
	 * @public
	 */
	public clearData = (): void => {
		localStorage.clear();
	}
}