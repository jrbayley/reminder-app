import { LocalDb } from './LocalDb';

/**
 * Create and initialise the local database
 * @class
 */

export class Database {

	/** @var {LocalDB} localDb The local db instance */
	public static localDb : LocalDb;

	/**
	 * Initialise the local static reference with a new LocalDB instance
	 * @method init
	 * @public
	 * @static
	 * @returns {Promise<void>} An empty promise.
	 */
	public static init = (): void => {
		Database.localDb = new LocalDb();
	}
}