import { Device } from 'framework7/utils/device';

import { F7 } from '../framework/F7';
import { BrowserInformation } from './BrowserInformation';
import { DeviceInformation } from './DeviceInformation';

/**
 * Utility class for interfacing the app to the local device info and facilities
 * @class
 */

export class LocalDevice {

	/** @var {DeviceInformation} deviceInformation Access to the device information class */
	public deviceInformation : DeviceInformation;

	/**
	 * Initialise all the device features and info
	 * @constructor
	 */
	constructor () {
		if (this.isMobile()) {
			this.deviceInformation = new DeviceInformation();
		} else {
			this.deviceInformation = new BrowserInformation()
		}
	}


	/**
	 * Fire the framework device initialisation
	 * @method device
	 * @public
	 * @returns {Device} Returns the f7 device object
	 */
	public device = (): Device => {
		return F7.framework7App.app.device;
	}

	/**
	 * Determine if this is a mobile device
	 * @method isMobile
	 * @public
	 * @returns {boolean} True for a mobile device
	 */
	public isMobile = (): boolean => {
		return (this.device().cordova && (this.device().ios || this.device().android));
	}

	/**
	 * Determine if this is a cordova device or desktop
	 * @method deviceType
	 * @public
	 * @returns {string} Returns a string of the type of device
	 */
	public deviceType = (): string => {
		if (F7.framework7App.app.device.cordova) {
			return 'cordova';
		}
		if (F7.framework7App.app.device.desktop) {
			return 'desktop';
		}
	}
}