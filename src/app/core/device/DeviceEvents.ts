import { Listeners } from '../listener/Listeners';
import { Logger } from '../logger/Logger';

/**
 * This class is used to manage any device events that may be raised
 * @class
 */

export class DeviceEvents {

	/**
	 * Initialise the device event listeners
	 * @constructor
	 */
	constructor() {
		Logger.debug('DeviceEvents : Listening for device events.');
		document.addEventListener('backbutton', this.fireBackButtonListener, false);
		document.addEventListener('pause', this.firePauseListener, false);
		document.addEventListener('resume', this.fireResumeListener, false);
	}

	/**
	 * Fire the back button listener.
	 * @method fireBackButtonListener
	 * @private
	 * @param {Event} event The button event.
	 */
	private fireBackButtonListener = (event: Event): void => {
		Listeners.deviceListener.deviceBack.publish(event);
	}

	/**
	 * Fire the device pause listener.
	 * @method firePauseListener
	 * @private
	 * @param {Event} event The pause event.
	 */
	private firePauseListener = (event: Event): void => {
		Logger.debug('DeviceEvents.firePauseListener : PAUSE.');
		Listeners.deviceListener.devicePause.publish(event);
	}

	/**
	 * Fire the device resume listener.
	 * @method fireResumeListener
	 * @private
	 * @param {Event} event The resume event.
	 */
	private fireResumeListener = (event: Event): void => {
		Logger.debug('DeviceEvents.fireResumeListener : RESUME.');
		Listeners.deviceListener.deviceResume.publish(event);
	}

}