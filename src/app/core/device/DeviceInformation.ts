/**
 * This class contains the device information at runtime
 * @class
 */

export class DeviceInformation implements CordovaDevice{

	/** @var {string} cordova If the app is cordiva the version */
	public cordova : string;

	/** @var {string} model If the detected device model */
	public model : string;

	/** @var {string} platform The detected platform */
	public platform : string;

	/** @var {string} uuid The uuid of the app / device or install */
	public uuid : string;

	/** @var {string} version The detected device version */
	public version : string;

	/** @var {string} manufacturer The detected device manufacturer */
	public manufacturer : string;

	/** @var {boolean} isVirtual Flag indicating this is a virtual device */
	public isVirtual : boolean;

	/** @var {string} serial The detected device serial number */
	public serial : string;

	/** @var {boolean} available Availability of the app flag */
	public available : boolean;

	/**
	 * Initialise the device info
	 * @constructor
	 */
	constructor() {
		this.cordova = device.cordova;
		this.model = device.model;
		this.platform = device.platform;
		this.uuid = device.uuid;
		this.version = device.version;
		this.manufacturer = device.manufacturer;
		this.isVirtual = device.isVirtual;
		this.serial = device.serial;
		this.available = device.available;
	}
}

