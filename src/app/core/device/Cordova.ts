import { Listeners } from '../listener/Listeners';
import { Logger } from '../logger/Logger';
import { DeviceEvents } from './DeviceEvents';
import { NativeAudio } from './NativeAudio';
import { Network } from './Network';

/**
 * Utility class for interfacing the app to the local cordova facilities
 *  @class
 */

export class Cordova {

	/** @var {NativeAudio} nativeAudio Access to the device audio */
	public nativeAudio : NativeAudio;

	/** @var {Network} network Access to the device audio */
	public network : Network;

	/** @var {DeviceEvents} deviceEvents Access to the device events class */
	public deviceEvents : DeviceEvents;


	/**
	 * Initialise all the device features and info
	 * @constructor
	 */
	constructor () {
		Logger.debug('Cordova : Creating cordova control');
		this.deviceEvents = new DeviceEvents();
		Listeners.deviceListener.deviceReady.subscribe(this.onCordovaReady);
		document.addEventListener('deviceready', this.readyListener, false);
	}

	/**
	 * Initialise cordova device classes
	 * @method readyListener
	 * @private
	 * @param {Event} event The event that fires this listener
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private readyListener = (event: Event): void => {
		Logger.debug('Cordova : Firing cordova onReady change');
		Listeners.deviceListener.deviceReady.publish(event);
	}

	/**
	 * Method that is called when the device init indicates it is ready
	 * @method onCordovaReady
	 * @private
	 * @returns {Promise<void>} Returns an empty promise
	 */
	private onCordovaReady = (): Promise<void> => {
		this.nativeAudio = new NativeAudio();
		this.network = new Network();
		return;
	};
}