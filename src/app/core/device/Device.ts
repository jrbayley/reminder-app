import { Cordova } from './Cordova';
import { LocalDevice } from './LocalDevice';

/**
 * Access to local device services, imformation and events
 * @class
 */

export class Device {

	/** @var {LocalDevice} localDevice information about the local device */
	public static localDevice : LocalDevice;

	/** @var {Cordova} cordova information about the local device */
	public static cordova : Cordova;


	/**
	 * Initialise the local device components if this is a mobile device
	 * @method
	 * @static
	 */
	public static init = (): void => {
		Device.localDevice = new LocalDevice();
		Device.cordova = new Cordova();
	}
}