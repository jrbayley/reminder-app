import { F7 } from '../framework/F7';

/**
 * This class contains the device information at runtime
 * @class
 */

export class BrowserInformation implements CordovaDevice{

	/** @var {string} cordova If the app is cordiva the version */
	public cordova : string;

	/** @var {string} model If the detected device model */
	public model : string;

	/** @var {string} platform The detected platform */
	public platform : string;

	/** @var {string} uuid The uuid of the app / device or install */
	public uuid : string;

	/** @var {string} version The detected device version */
	public version : string;

	/** @var {string} manufacturer The detected device manufacturer */
	public manufacturer : string;

	/** @var {boolean} isVirtual Flag indicating this is a virtual device */
	public isVirtual : boolean;

	/** @var {string} serial The detected device serial number */
	public serial : string;

	/** @var {boolean} available Availability of the app flag */
	public available : boolean;

	/**
	 * Initialise the browser info
	 * @constructor
	 */
	constructor() {
		this.cordova = (F7.framework7App.app.device.cordova) ? 'Yes' : 'No';
		this.model = this.getModel();
		this.platform = this.getPlatform();
		this.uuid = 'Unavailable';
		this.version = F7.framework7App.app.device.osVersion;
		this.manufacturer = F7.framework7App.app.device.os;
		this.isVirtual = false;
		this.serial = 'Unavailable';
		this.available = true;
	}

	/**
	 * Determine the platform string based onthe browser flags
	 * @method getPlatform
	 * @private
	 * @returns {string} The platform string
	 */
	private getPlatform = (): string => {
		if (F7.framework7App.app.device.windows) {
			return 'Windows';
		}
		if (F7.framework7App.app.device.macos) {
			return 'Mac';
		}
		if (F7.framework7App.app.device.ipad) {
			return 'iPad';
		}
		if (F7.framework7App.app.device.ipod) {
			return 'iPod';
		}
		if (F7.framework7App.app.device.iphone) {
			return 'iPhone';
		}
		if (F7.framework7App.app.device.android) {
			return 'Android';
		}
		return 'Unknown';
	}
	/**
	 * Determine the model string based onthe browser flags
	 * @method getModel
	 * @private
	 * @returns {string} The model string
	 */
	private getModel = (): string => {
		if (F7.framework7App.app.device.ios) {
			return 'iOS';
		}
		if (F7.framework7App.app.device.androidChrome) {
			return 'Android Chrome';
		}
		if (F7.framework7App.app.device.edge) {
			return 'edge';
		}
		if (F7.framework7App.app.device.ie) {
			return 'ie';
		}
		if ((<any>F7).framework7App.app.device.electron) {
			return 'electron';
		}
		return 'Unknown';
	}
}

