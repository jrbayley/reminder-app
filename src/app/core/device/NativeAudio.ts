import { Logger } from '../logger/Logger';

/**
 * Handle interfaces to the devices audio facilities
 * @class
 */

export class  NativeAudio {

	/** @var {number} volume The audio volume */
	private volume = 1;

	/** @var {number} voices The number of audio voices to use */
	private voices = 1;

	/** @var {number} delay The audio delay */
	private delay = 0;

	/**
	 * Preload the audio
	 * @constructor
	 */
	constructor() {
		this.preloadSimple('beeps', 'audio/beeps.mp3')
		.then(() => {
			this.play('beeps');
		})
		.catch((error: Error) => {
			Logger.error(error.message);
		});
	}

	/**
	 * Preload any longer audio that may need to be stopped.
	 * @method preloadComplex
	 * @public
	 * @param {string} audio the audio id
	 * @param {string} fileName the audio asset filename
	 */
	public preloadComplex = (audio: string, fileName: string): Promise<void> => {
		return new Promise((resolve, reject) => {
			return window.plugins.NativeAudio.preloadComplex(audio, fileName, this.volume, this.voices, this.delay,
			(_msg: string) => {
				resolve();
			},
			(msg: string) => {
				reject(msg);
			});
		});
	}

	/**
	 * Preload any short clips / single shots (up to five seconds) that cannot be stopped once started.
	 * @method preloadSimple
	 * @public
	 * @param {string} audio the audio id
	 * @param {string} fileName the audio asset filename
	 */
	public preloadSimple = (audio: string, fileName: string): Promise<void> => {
		return new Promise((resolve, reject) => {
			return window.plugins.NativeAudio.preloadSimple(audio, fileName,
			(_msg: string) => {
				resolve();
			},
			(msg: string) => {
				reject(msg);
			});
		});
	}

	/**
	 * Play an audio file.
	 * @method play
	 * @public
	 * @param {string} audio the audio id
	 */
	public play = (audio: string): void => {
		window.plugins.NativeAudio.play(audio);
	}

	/**
	 * Play an audio file on loop.
	 * @method loop
	 * @public
	 * @param {string} audio the audio id
	 */
	public loop = (audio: string): void => {
		window.plugins.NativeAudio.loop(audio);
	}

	/**
	 * Stop playback of an audio file.
	 * @method stop
	 * @public
	 * @param {string} audio the audio id
	 */
	public stop = (audio: string): void => {
		window.plugins.NativeAudio.stop(audio);
	}

	/**
	 * Unload an audio file from memort.
	 * @method unload
	 * @public
	 * @param {string} audio the audio id
	 */
	public unload = (audio: string): void => {
		window.plugins.NativeAudio.unload(audio);
	}
}