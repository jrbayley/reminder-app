import { Listeners } from '../listener/Listeners';
import { Logger } from '../logger/Logger';

/**
 * Handle interfaces to the devices network facilities
 * @class
 */

export class Network {

	/** @var {string} connectionType The connection type returned from the network interface */
	public connectionType : string;

	/** @var {string} online String containing the current online status (online / offline) */
	public online : string;

	/**
	 * Initialise the listeners that publish network status changes.
	 * @constructor
	 */
	constructor() {
		document.addEventListener('online', this.onOnline, false);
		document.addEventListener('offline', this.onOffline, false);
		Listeners.deviceListener.deviceNetwork.subscribe(this.onNetworkStatusChange);
		// Network status takes a while to initialise according to docs.
		if (navigator && navigator.onLine) {
			this.initialStatusCheck();
		} else {
			setTimeout(() => {
				this.initialStatusCheck();
			}, 1000);
		}
	}

	/**
	 * Run an initial status check from the navigator object to report the network state at startup
	 * @method initialStatusCheck
	 * @private
	 */
	private initialStatusCheck = (): void => {
		if (navigator && navigator.onLine) {
			Listeners.deviceListener.deviceNetwork.publish('online');
		} else {
			Listeners.deviceListener.deviceNetwork.publish('offline');
		}
	}
	/**
	 * Method that is called when the network status changes
	 * @method onNetworkStatusChange
	 * @private
	 * @async
	 * @param {string} status The status that the network is currently in at fire time.
	 * @returns {Promise<void>} An empty promise.
	 */
	private onNetworkStatusChange = (status: string): Promise<void> => {
		Logger.debug('Network.onNetworkStatusChange : Firing on Network Change : ' + status);
		Logger.debug('Network.onNetworkStatusChange : Network type : ' + navigator.connection.type);
		this.online = status;
		this.connectionType = this.checkConnection();
		return;
	};

	/**
	 * Method that is triggered when the network is online.
	 * @method onOnline
	 * @private
	 * @async
	 * @returns {Promise<void>} An empty promise.
	 */
	private onOnline = (): Promise<void> => {
		Logger.debug('Network.onOnline : Firing on Online');
		Listeners.deviceListener.deviceNetwork.publish('online');
		return;
	};


	/**
	 * Method that is triggered when the network is offline.
	 * @method onOffline
	 * @private
	 * @async
	 * @returns {Promise<void>} An empty promise.
	 */
	public onOffline = (): Promise<void> => {
		Logger.debug('Network.onOffline : Firing on Offline');
		Listeners.deviceListener.deviceNetwork.publish('offline');
		return;
	};

	/**
	 * Method that is triggered when the network is offline.
	 * @method checkConnection
	 * @private
	 * @returns {Promise<void>} An empty promise.
	 */
	public checkConnection = (): string => {
		const networkState = navigator.connection.type;
		const states = {};
		states[Connection.UNKNOWN]  = 'Unknown connection';
		states[Connection.ETHERNET] = 'Ethernet connection';
		states[Connection.WIFI]     = 'WiFi connection';
		states[Connection.CELL_2G]  = 'Cell 2G connection';
		states[Connection.CELL_3G]  = 'Cell 3G connection';
		states[Connection.CELL_4G]  = 'Cell 4G connection';
		states[Connection.CELL]     = 'Cell generic connection';
		states[Connection.NONE]     = 'No network connection';
		return states[networkState];
	}
}