import Handlebars from 'handlebars';

/**
 * Utility class for extending handlebars functionality
 * @class
 */

export class HandlebarUtils {

	/**
	 * Register the handlebars helper functions
	 * @method registerHelpers
	 * @public
	 * @static
	 */
	public static registerHelpers = (): void => {
		HandlebarUtils.registerCheckUtil();
	}
	/**
	 * Handlebars check function to return true if the element exists and is not empty
	 * @method registerCheckUtil
	 * @public
	 * @static
	 */
	private static registerCheckUtil = (): void => {
		Handlebars.registerHelper('check', (value) => {
			return (value !== null && value !== '');
		});
	}
}