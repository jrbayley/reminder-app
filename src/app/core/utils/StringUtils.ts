
/**
 *
 * Common tools for working with strings.
 * @class
 *
 */

export class StringUtils {
	
	/**
	 * Remove any characters that can cause probles with filenames replacing them with a dash.
	 * @method filenameCleanString
	 * @public
	 * @static
	 * @param {string} rawString string to be cleaned up.
	 * @returns {string} returns a string with all illegal characters removed
	 */
	public static filenameCleanString = (rawString: string): string => {
		let cleanName = rawString.replace(/\&/gi, '-');
		cleanName = cleanName.replace(/\(/gi, '-');
		cleanName = cleanName.replace(/\)/gi, '-');
		cleanName = cleanName.replace(/\s/gi, '-');
		cleanName = cleanName.replace(/\//gi, '-');
		cleanName = cleanName.replace(/\\/gi, '-');
		cleanName = cleanName.replace(/[^0-9a-z_-]/gi, '');
		return cleanName;
	}

	/**
	 * Remove any space characters and lowecase the string.
	 * @method removeSpaceAndMakeLowercase
	 * @public
	 * @static
	 * @param {string} rawString string to be cleaned up.
	 * @returns {string} returns a LOWERCASE string with space characters removed
	 */
	public static removeSpaceAndMakeLowercase = (rawString: string): string => {
		return rawString.toLowerCase().replace(/\s+/g, '');
	}

	/**
	 * Remove any space characters and lowecase the string.
	 * @method removeSpaceAndMakeUppercase
	 * @public
	 * @static
	 * @param {string} rawString string to be cleaned up.
	 * @returns {string} returns a UPPERCASE string with space characters removed
	 */
	public static removeSpaceAndMakeUppercase = (rawString: string): string => {
		return rawString.toUpperCase().replace(/\s+/g, '');
	}

	/**
	 * Remove any space characters and lowecase the string.
	 * @method postcodeString
	 * @public
	 * @static
	 * @param {string} postcode string to be cleaned up.
	 * @returns {string} returns a lowercase postcode with space characters removed
	 */
	public static postcodeString = (postcode: string): string => {
		return StringUtils.removeSpaceAndMakeUppercase(postcode);
	}


	/**
	 * Check to see if the string contains an uppercase character.
	 * @method containsUppercase
	 * @public
	 * @static
	 * @param {string} checkString the string to test against
	 * @returns {boolean} returns true if the string contains any uppercase character
	 */
	public static containsUppercase = (checkString: string): boolean => {
		return (checkString && (/[A-Z]/.test(checkString)));
	}

	/**
	 * Check to see if the string contains an lowercase character.
	 * @method containsLowercase
	 * @public
	 * @static
	 * @param {string} checkString the string to test against
	 * @returns {boolean} returns true if the string contains any lowercase character
	 */
	public static containsLowercase = (checkString: string): boolean => {
		return (checkString && (/[a-z]/.test(checkString)));
	}

	/**
	 * Check to see if the string contains a number character.
	 * @method containsNumeric
	 * @public
	 * @static
	 * @param {string} checkString the string to test against
	 * @returns {boolean} returns true if the string contains any numeric character
	 */
	public static containsNumeric = (checkString: string): boolean => {
		return (checkString && (/[0-9]/.test(checkString)));
	}

	/**
	 * Check to see if the string contains a non alpha numeric character.
	 * @method containsSpecial
	 * @public
	 * @static
	 * @param {string} checkString the string to test against
	 * @returns {boolean} returns true if the string contains any special character
	 */
	public static containsSpecial = (checkString: string): boolean => {
		return (checkString && (/[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(checkString)));
	}

	/**
	 * Check to see if the string contains a minimum of characters depending on the morethan value.
	 * @method stringLengthMoreThan
	 * @public
	 * @static
	 * @param {string} checkString the string to test against
	 * @returns {boolean} returns true if the string contains a character lenght between 8-20
	 */
	public static stringLengthMoreThan = (checkString: string, moreThan: number): boolean => {
		return (checkString && checkString.length >= moreThan);
	}

	/**
	 * Check to see if the string contains a number character.
	 * @method containsNumbersOnly
	 * @public
	 * @static
	 * @param {string} checkString the string to test against
	 * @returns {boolean} returns true if the string contains any numeric character
	 */
	public static containsNumbersOnly = (checkString: string): boolean => {
		return (checkString && (/^[0-9]+$/.test(checkString)));
	}

	/**
	 * Check to see if the string contains a number character and if not remove it .
	 * @method allowNumbersOnly
	 * @public
	 * @static
	 * @param {string} checkString the string to test against
	 * @returns {boolean} returns true if the string contains numeric characters only
	 */
	public static allowNumbersOnly = (checkString: string): string => {
		return (checkString && checkString.replace(/[^0-9]/gi, ''));
	}

	/**
	 * Check to see if the string contains a number character and if not remove it .
	 * @method allowLettersOnly
	 * @public
	 * @static
	 * @param {string} checkString the string to test against
	 * @returns {boolean} returns true if the string contains numeric characters only
	 */
	public static allowLettersOnly = (checkString: string): string => {
		return (checkString && checkString.replace(/[^a-zA-Z-]/gi, ''));
	}


	/**
	 * Check to see if the string contains an lowercase character.
	 * @method checkPasswordValidation
	 * @public
	 * @static
	 * @param {string} checkString the string to test against
	 * @returns {boolean} returns true if the string contains any lowercase character
	 */
	public static checkPasswordValidation = (checkString: string): boolean => {
		return StringUtils.containsLowercase(checkString) &&
		StringUtils.containsUppercase(checkString) &&
		StringUtils.containsNumeric(checkString) &&
		StringUtils.containsSpecial(checkString) &&
		StringUtils.stringLengthMoreThan(checkString, 8);
	}

	/**
	 * Check to see if the string contains an lowercase character.
	 * @method emailFormat
	 * @public
	 * @static
	 * @param {string} checkString the string to test against
	 * @returns {boolean} returns true if the string contains any lowercase character
	 */
	public static emailFormat = (checkString: string): boolean => {
		return (checkString && (/[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+[.]+[A-Za-z.-_]/.test(checkString)));
	}

	/**
	 * Check to see if the string contains an lowercase character.
	 * @method isValidPassword
	 * @public
	 * @static
	 * @param {string} checkString the string to test against
	 * @returns {boolean} returns true if the string contains any lowercase character
	 */
	public static isValidPassword = (checkString: string): boolean => {
		return StringUtils.checkPasswordValidation(checkString);
	}

	/**
	 * Check to see if the string contains an lowercase character.
	 * @method isValidEmail
	 * @public
	 * @static
	 * @param {string} checkString the string to test against
	 * @returns {boolean} returns true if the string contains any lowercase character
	 */
	public static isValidEmail = (checkString: string): boolean => {
		return StringUtils.emailFormat(checkString);
	}
}
