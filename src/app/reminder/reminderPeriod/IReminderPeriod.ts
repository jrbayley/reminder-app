/**
 * Interface to defined the reminder period objects
 * @interface
 */

export interface IReminderPeriod {

	/** @var {number} period The reminder period in days */
	period : number;

	/** @var {string} period_desc The reminder period description */
	period_desc : string;

	/** @var {string} period_code The reminder period code that uniquely identifies the period in the db */
	periodCode : string;
}