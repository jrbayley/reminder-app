/**
 * THe interface that defines the predicates that are used for filtering using lodash etc
 * @interface
 */

export interface ReminderPeriodPredicate {
	
	/** @var {number} period The reminder period in days */
	period? : number;

	/** @var {string} period_desc The reminder period description */
	period_desc? : string;

	/** @var {string} period_code The reminder period code that uniquely identifies the period in the db */
	periodCode? : string;
}