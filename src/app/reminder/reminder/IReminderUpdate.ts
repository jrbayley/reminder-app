/**
 * Interface for defining the reminder update object that will contain 
 * all updatable fields
 */

export interface IReminderUpdate {

	/** @var {number} reminder_id the reminder identifier */
	reminder_id : number;

	/** @var {number} user_id the reminder user identifier */
	user_id : number;

	/** @var {string} reminder_date the reminder date */
	reminder_date : string;

	/** @var {string} description the reminder description */
	description : string;

	/** @var {number} period the reminder period */
	period : number;

	/** @var {string} notes the reminder notes */
	notes : string;

	/** @var {number} parent_id the parent reminder identifier */
	parent_id : number;

	/** @var {boolean} reminder_enabled the reminder is enabled */
	reminder_enabled : boolean;

	/** @var {boolean} reminder_archived the reminder is archived */
	reminder_archived : boolean;

	/** @var {string} reminder_time the reminder time */
	reminder_time : string;


}