import { IReminder } from './IReminder';

/**
 * Type definitian for the reminder filter function
 * @typedef
 * @type
 */

export type ReminderPredicateFunction = (reminder: IReminder) => boolean;