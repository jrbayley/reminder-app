import moment from 'moment';

import { TimeUtils } from '../../core/utils/TimeUtils';
import { IReminderPlan } from './IReminderPlan';

/**
 * Presentation class for displaying reminder plans on screen.
 */

export class ReminderPlanDisplay implements IReminderPlan{

	/** @var {number} user_id the reminder user identifier */
	public user_id : number;

	/** @var {number} period the reminder period in days */
	public reminder_period : number;

	/** @var {number} reminder_id the reminder identifier */
	public reminder_id : number;

	/** @var {string} reminder_notes the reminder notes data */
	public reminder_notes : string;

	/** @var {string} reminder_repeat_period The reminder period in string format */
	public reminder_repeat_period : string;

	/** @var {string} reminder_date the reminder date */
	public reminder_date : string;

	/** @var {string} description the reminder description */
	public description : string;

	/** @var {string} sent_date the date the reminder email will be date */
	public sent_date : string;

	/** @var {string} type the reminder message type */
	public type : number;

	/** @var {string} date_human the date the reminder email will be in humanized form */
	public date_human : string;

	/** @var {string} type_desc the reminder type description text */
	public type_desc : string;

	/** @var {string} type_class the reminder type class for grouping */
	public type_class : string;

	/** @var {string} month the reminder month text */
	public month : string;

	/** @var {string} year the reminder year text */
	public year : string;

	/**
	 * @constructor
	 * @param {IReminderPlan} reminderPlanData
	 */
	constructor(reminderPlanData: IReminderPlan) {
		this.user_id = reminderPlanData.user_id;
		this.reminder_period = reminderPlanData.reminder_period;
		this.reminder_id = reminderPlanData.reminder_id;
		this.reminder_notes = reminderPlanData.reminder_notes;
		this.reminder_repeat_period = reminderPlanData.reminder_repeat_period;
		this.reminder_date = reminderPlanData.reminder_date;
		this.description = reminderPlanData.description;
		this.sent_date = reminderPlanData.sent_date;
		this.type = reminderPlanData.type;
		this.date_human = TimeUtils.momentAsHumanDate(moment(reminderPlanData.reminder_date));
		this.type_desc = this.getTypeDesc(reminderPlanData.type);
		this.type_class = this.getTypeClass(reminderPlanData.type);
		this.month = moment(reminderPlanData.reminder_date).format('MMMM');
		this.year = moment(reminderPlanData.reminder_date).format('YYYY');
	}

	/**
	 * Get the reminder type description that matches the type value
	 * @method getTypeDesc
	 * @private
	 * @param {number} typeCode The numeric type code to retrieve a description for
	 * @returns {string} The description for the selected type
	 */
	private getTypeDesc = (typeCode: number): string => {
		switch(typeCode) {
			case 0 :
				return 'Today';
			case 1 :
				return 'Tomorrow';
			case 7 :
				return 'One Week';
			case 14 :
				return 'Fortnight';
			case 28 :
				return 'One Month';
		}
	}

	/**
	 * Get the reminder type class that matches the type value
	 * @method getTypeClass
	 * @private
	 * @param {number} typeCode The numeric type code to retrieve a class for
	 * @returns {string} The class for the selected type
	 */
	private getTypeClass = (typeCode: number): string => {
		switch(typeCode) {
			case 0 :
				return 'today';
			case 1 :
				return 'tomorrow';
			case 7 :
				return 'week';
			case 14 :
				return 'fortnight';
			case 28 :
				return 'month';
		}
	}
}