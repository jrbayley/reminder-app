import moment from 'moment';

import { AppData } from '../../core/appData/AppData';
import { TimeUtils } from '../../core/utils/TimeUtils';
import { IReminder } from './IReminder';
import { IReminderDisplay } from './IReminderDisplay';

/**
 * The reminder display class for presenting reminders onto the screen.
 */

export class ReminderDisplay implements IReminderDisplay{

	/** @var {number} reminder_id the reminder identifier */
	public reminder_id : number;

	/** @var {number} user_id the reminder user identifier */
	public user_id : number;

	/** @var {string} reminder_date the reminder date */
	public reminder_date : string;

	/** @var {string} description the reminder description */
	public description : string;

	/** @var {number} period the reminder period */
	public period : number;

	/** @var {string} notes the reminder notes */
	public notes : string;

	/** @var {number} parent_id the parent reminder identifier */
	public parent_id : number;

	/** @var {boolean} reminder_enabled the reminder is enabled */
	public reminder_enabled : boolean;

	/** @var {boolean} reminder_archived the reminder is archived */
	public reminder_archived : boolean;

	/** @var {string} reminder_time the reminder time */
	public reminder_time : string;

	/** @var {boolean} reminder_deleted the reminder is deleted */
	public reminder_deleted : boolean;

	/** @var {string} date_human the reminder date in human form */
	public date_human? : string;

	/** @var {string} period_description the reminder period description */
	public period_description? : string;

	/** @var {string} due_in_desc the reminder time to date in human form */
	public due_in_desc? : string;

	/** @var {string} month the month that the reminder is for */
	public month? : string;

	/** @var {string} year the year that the reminder os for */
	public year? : string;

	/**
	 * Initialise the object with the raw data from a reminder.
	 * @constructor
	 * @param reminderData The data to initialise with
	 */
	constructor(reminderData: IReminder) {
		this.reminder_id = reminderData.reminder_id;
		this.user_id = reminderData.user_id;
		this.reminder_date = reminderData.reminder_date;
		this.description = this.decodeHtml(reminderData.description);
		this.period = reminderData.period;
		this.notes = this.decodeHtml(reminderData.notes);
		this.parent_id = reminderData.parent_id;
		this.reminder_enabled = reminderData.reminder_enabled;
		this.reminder_archived = reminderData.reminder_archived;
		this.reminder_time = reminderData.reminder_time;
		this.reminder_deleted = reminderData.reminder_deleted;
		this.date_human = TimeUtils.momentAsHumanDate(moment(reminderData.reminder_date));
		this.period_description = this.periodDesc(reminderData.period);
		this.due_in_desc = TimeUtils.calculateHowLong(moment(reminderData.reminder_date));
		this.month = moment(reminderData.reminder_date).format('MMMM');
		this.year = moment(reminderData.reminder_date).format('YYYY');
	}

	/**
	 * return the description for the period.
	 * @method periodDesc
	 * @private
	 * @param {number} period the period value from the reminder
	 * @returns {string} the period description
	 */
	private periodDesc = (period: number): string => {
		return AppData.reminderPeriods.getReminderPeriodByPeriod(period).period_desc;
	}

	/**
	 * return the string with any html elements decoded.
	 * @method decodeHtml
	 * @private
	 * @param {string} rawString the text that may contain html entities
	 * @returns {string} the string with any entities decoded.
	 */
	private decodeHtml = (rawString: string): string => {
		const ele = document.createElement('textarea');
		ele.innerHTML = rawString;
		return ele.value;
	}
}