/**
 * THe interface that defines the predicates that are used for filtering using lodash etc
 * @interface
 */

export interface ReminderPredicate {

	/** @var {boolean} reminder_enabled the optional reminder is enabled */
	reminder_enabled? : boolean;

	/** @var {boolean} reminder_archived the optional reminder is archived */
	reminder_archived? : boolean;

	/** @var {boolean} reminder_deleted the optional reminder is deleted */
	reminder_deleted? : boolean;
}