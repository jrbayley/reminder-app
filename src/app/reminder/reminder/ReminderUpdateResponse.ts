import { IReminder } from './IReminder';
import { ReminderValidResponse } from './ReminderValidResponse';

/**
 * Reminder update response object returns the response and the reminder
 * @class
 */

export interface ReminderUpdateResponse extends ReminderValidResponse{

	/** @var {IReminder} reminder the returned reminder object */
	reminder : IReminder;

	/** @var {ReminderValidResponse} result the reminder validation response object */
	result : ReminderValidResponse;
}