import moment from 'moment';

import { AppData } from '../../core/appData/AppData';
import { Logger } from '../../core/logger/Logger';
import { TimeUtils } from '../../core/utils/TimeUtils';
import { IAddReminder } from './IAddReminder';
import { IEditReminder } from './IEditReminder';
import { IReminder } from './IReminder';

/**
 * Utilities for editing reminders
 * @class
 */

export class ReminderTools {

	/**
	 * Utility method to return an edit object from a reminder
	 * @method createEditReminder
	 * @public
	 * @static
	 * @param {IReminder} reminder The reminder data
	 * @returns {IEditReminder} An edit reminder object
	 */
	public static createEditReminder = (reminder: IReminder): IEditReminder => {
		return {
			edit_reminder_id : reminder.reminder_id.toString(),
			edit_user_id : reminder.user_id.toString(),
			edit_reminder_date : moment(reminder.reminder_date).format('DD-MM-YYYY'),
			edit_reminder_description : reminder.description,
			edit_reminder_period : reminder.period.toString(),
			edit_reminder_period_desc : AppData.reminderPeriods.getReminderPeriodByPeriod(reminder.period).period_desc,
			edit_reminder_notes : reminder.notes,
			edit_parent_id : reminder.parent_id.toString(),
			edit_reminder_enabled : (reminder.reminder_enabled) ? 'yes' : '',
			edit_reminder_archived : (reminder.reminder_archived) ? 'yes' : '',
			edit_reminder_time : reminder.reminder_time,
			edit_reminder_deleted : (reminder.reminder_deleted) ? 'yes' : '',
			edit_reminder_due_in_desc : 'Due ' + TimeUtils.calculateHowLong(moment(reminder.reminder_date))
		};
	}

	/**
	 * Utility method to return an edit object from a reminder
	 * @method createReminderUpdate
	 * @public
	 * @static
	 * @param {IReminder} editReminderFromForm The reminder data
	 * @returns {IEditReminder} An edit reminder object
	 */
	public static createReminderUpdate = (editReminderFromForm: IEditReminder): IReminder => {
		return {
			reminder_id : parseInt(editReminderFromForm.edit_reminder_id),
			user_id : parseInt(editReminderFromForm.edit_user_id),
			reminder_date : TimeUtils.stringDateToMoment(editReminderFromForm.edit_reminder_date).toISOString(),
			description : editReminderFromForm.edit_reminder_description,
			period : parseInt(editReminderFromForm.edit_reminder_period),
			notes : editReminderFromForm.edit_reminder_notes,
			parent_id : parseInt(editReminderFromForm.edit_parent_id),
			reminder_enabled : (editReminderFromForm.edit_reminder_enabled[0]) ? editReminderFromForm.edit_reminder_enabled[0] === 'yes' : false,
			reminder_archived : (editReminderFromForm.edit_reminder_archived[0]) ? editReminderFromForm.edit_reminder_archived[0] === 'yes' : false,
			reminder_time : editReminderFromForm.edit_reminder_time,
			reminder_deleted : false
		};
	}

	/**
	 * Utility method to return a reminder object from add reminder form data
	 * @method createAddReminder
	 * @public
	 * @static
	 * @param {any} reminder The reminder data
	 * @returns {IAddReminder} A reminder object
	 */
	public static createAddReminder = (reminder: any): IAddReminder => {
		Logger.debug('ReminderTools.createAddReminder: Creating from %o', reminder);
		return {
			reminder_date : moment(reminder.add_reminder_date, 'DD-MM-YYYY').toISOString(),
			reminder_description : reminder.add_reminder_description,
			reminder_period : (reminder.add_reminder_period) ? reminder.add_reminder_period.toString() : 0,
			reminder_notes : reminder.add_reminder_notes,
			reminder_time : reminder.add_reminder_time,
		};
	}
}