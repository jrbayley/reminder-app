/**
 * Interface to define the reminder plan object that is used for displaying reminders
 * on screen. Contains additional humanized fields
 * @interface
 */

export class IReminderPlan {

	/** @var {number} user_id the reminder user identifier */
	user_id : number;

	/** @var {number} period the reminder period in days */
	reminder_period : number;

	/** @var {number} reminder_id the reminder identifier */
	reminder_id : number;

	/** @var {string} reminder_notes the reminder notes data */
	reminder_notes : string;

	/** @var {string} reminder_repeat_period The reminder period in string format */
	reminder_repeat_period : string;

	/** @var {string} reminder_date the reminder date */
	reminder_date : string;

	/** @var {string} description the reminder description */
	description : string;

	/** @var {string} sent_date the date the reminder email will be date */
	sent_date : string;

	/** @var {string} type the reminder message type */
	type : number;
}