import { IReminder } from './IReminder';
import { ReminderValidResponse } from './ReminderValidResponse';

/**
 * @class
 * Reminder update response object returns the response and the reminder
 */
export interface ReminderInsertResponse extends ReminderValidResponse {
	/** @var {IReminder} reminder the returned reminder object */
	reminder : IReminder;

	/** @var {ReminderValidResponse} result the reminder validation response object */
	result : ReminderValidResponse;
}