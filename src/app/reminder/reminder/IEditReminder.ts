/**
 * Interface to define the edit reminder object that is used for editing reminders in a form
 * @interface
 */

export interface IEditReminder {

	/** @var {string} edit_reminder_id the reminder identifier */
	edit_reminder_id : string;

	/** @var {string} edit_user_id the reminder user identifier */
	edit_user_id : string;

	/** @var {string} edit_reminder_date the reminder date */
	edit_reminder_date : string;

	/** @var {string} edit_reminder_description the reminder description */
	edit_reminder_description : string;

	/** @var {string} edit_reminder_period the reminder period */
	edit_reminder_period : string;

	/** @var {string} edit_reminder_period_desc the reminder period description */
	edit_reminder_period_desc : string;

	/** @var {string} edit_reminder_notes the reminder notes */
	edit_reminder_notes : string;

	/** @var {string} edit_reminder_parent_id the parent reminder identifier */
	edit_parent_id : string;

	/** @var {string} edit_reminder_enabled the reminder is enabled */
	edit_reminder_enabled : string;

	/** @var {string} edit_reminder_archived the reminder is archived */
	edit_reminder_archived : string;

	/** @var {string} edit_reminder_time the reminder time */
	edit_reminder_time : string;

	/** @var {string} edit_reminder_deleted the reminder is deleted */
	edit_reminder_deleted : string;

	/** @var {string} edit_reminder_due_in_desc the reminder is due_in_desc */
	edit_reminder_due_in_desc : string;
}