/**
 * Reminder validation response object
 * Used to return the results of validation checks
 * @class
 */

 export interface ReminderValidResponse {

	/** @var {boolean} success Shows if the reminder update was successful or not */
	success : boolean;

	/** @var {string} message Details of the response indicating any issues */
	message : string;
}