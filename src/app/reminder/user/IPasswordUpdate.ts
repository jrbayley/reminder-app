/**
 * This interface defines the message that will be sent to change the password
 */
export interface IPasswordUpdate {

	/** @var {string} currentPassword The current user password */
	currentPassword? : string;

	/** @var {string} newPassword The new user password */
	newPassword? : string;
}