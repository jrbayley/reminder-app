/**
 * This interface defines the response that will be send when updating any account data
 * @interface
 */

export interface IAccountResponse {
	
	/** @var {string} message The optional message detailing the response outcome */
	message? : string;

	/** @var {boolean} result The result of the submission true / false for sucess / failure */
	result? : boolean;
}