export interface IUser {

	/** @var {string} user_id Surrogate PK unique identifier used to link the user */
	user_id : number;

	/** @var {string} user_email Users email address */
	user_email : string; //

	/** @var {boolean} user_suspended  User account is suspended by administrators */
	user_suspended : boolean;

	/** @var {string} user_firstname The user first / christian name */
	user_firstname : string;

	/** @var {string} user_surname The user Surname / Family name */
	user_surname : string;

	/** @var {string} user_password The encrypted user password */
	user_password : string;

	/** @var {Date} user_password_expires_on User password expiry. Users cannot log in after this date. */
	user_password_expires_on : Date;

	/** @var {Date} user_created_on The timestamp of user creation / registration */
	user_created_on : Date;

	/** @var {boolean} user_locked_out Account locked out - security breach - User resettable. */
	user_locked_out : boolean;

	/** @var {Date} user_last_login The time that the user last logged in [Form login only] */
	user_last_login : Date;

	/** @var {number} user_failed_logins The number of failed login attempts */
	user_failed_logins : number;

	/** @var {string} user_forgotten_password_code Encrypted value that is sent to the user to prove they received an email */
	user_forgotten_password_code : string;

	/** @var {Date} user_forgotten_password_time The time  that the user entered the recover password system */
	user_forgotten_password_time : Date;

	/** @var {boolean} user_verified  Indicates that the user has successfully completed the email validation verifying the registration email address */
	user_verified : boolean;

	/** @var {string} user_image_filename The users image if set, */
	user_image_filename : string;
}