#!/usr/bin/env node

var fs      = require('fs');
var path    = require('path');
var moment  = require('moment');
var git     = require("nodegit");

function extractVersionFromConfigXml(rootdir) {
	var configXml = fs.readFileSync(path.join(rootdir, 'config.xml'), 'utf8');
	var versionResult = configXml.match(/<widget.*version=\"(.*?)\"/);
	if (versionResult.length != 2) {
		throw Error("Unable to reliably extract version number from config.xml");
	}
	return versionResult[1];
}

function replaceVersion(isRelease, rootdir, filePath, forceUncleanBuild) {
	return git.Repository.open(".git").then(function(repo) {
		console.log("Fetching GIT HEAD commit");
		return repo.getHeadCommit().then(function (headCommit) {
			console.log("Checking GIT repo status");
			return repo.getStatus().then(function(repoStatus) {
				var version   = extractVersionFromConfigXml(rootdir);
				var buildID   = (""+headCommit).substring(0, 10);
				var buildTime = moment().toISOString();

				if (!isRelease) {
					version = version + "+DEV";
				}

				if (repoStatus.length > 0 && !forceUncleanBuild) {
					buildID = buildID + "+local";
				}

				console.log("Injecting version strings into "+ filePath);
				console.log("Version:    "+ version);
				console.log("Build ID:   "+ buildID);
				console.log("Build Time: "+ buildTime);

				var data = fs.readFileSync(filePath, 'utf8');

				// console.log("Before ==================");
				// console.log(data);
				var result = data.
						replace(/^.*version.*$/mg,   '	public static version = \'' + version + '\';').
						replace(/^.*buildId.*$/mg,   '	public static buildID = \'' + buildID + '\';').
						replace(/^.*buildTime.*$/mg, '	public static buildTime = \'' + buildTime + '\';');
				// console.log("After ===================");
				// console.log(result);
				// console.log("=========================");

				fs.writeFileSync(filePath, result, 'utf8');

				process.stdout.write('Updated version strings in: ' + filePath + '\n');
				// console.log("Replaced in "+ filePath);

				return undefined;
			}).catch(function (e) {
				console.log("Failed to update version strings because of: "+ e);
				process.exit(1);
			});
		});
	});
}

module.exports = function(context) {
	var isRelease = !!context.opts.options && !!context.opts.options.release;
	var forceUncleanBuild = ''; // !!context.opts.options['force-build-with-unclean-repo'];
	var rootdir = context.opts.projectRoot;
	var filePath = path.join(context.opts.projectRoot, 'src', 'app', 'Version.ts');
	return replaceVersion(isRelease, rootdir, filePath, forceUncleanBuild);
};
