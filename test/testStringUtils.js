var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();

var StringUtils = require ('../www/app/core/utils/StringUtils.js');

var illegalFilename = 'filename/|£#?"';
var cleanedFilename = 'filename-';

var stringWithSpace = 'String With Space';
var stringWithoutSpaceLowercase = 'stringwithspace';
var stringWithoutSpaceUppercase = 'STRINGWITHSPACE';
var stringPostcode = 'pe38 9jd';
var stringCleanPostcode = 'PE389JD';
var stringLettersUpper = 'PEJD';
var stringLettersLower = 'pejd';
var stringNumeric = '389';
var stringValidEmailCoUk = 'test@test.co.uk';
var stringValidEmailCom = 'test@test.com';
var stringValidPassword = 'Pa55word@';


describe('String Checks', () => {
	describe('cleanes a dirty Filename string', () => {
		it('should return true when the filename is clean', () => {
			StringUtils.StringUtils.filenameCleanString(illegalFilename).should.equal(cleanedFilename);
		});
	});

	describe('Removed Spaces and lowercase', () => {
		it('should return true when the lowercase and no spaces', () => {
			StringUtils.StringUtils.removeSpaceAndMakeLowercase(stringWithSpace).should.equal(stringWithoutSpaceLowercase);
		});
	});

	describe('Removed Spaces and uppercase', () => {
		it('should return true when uppercase and no spaces', () => {
			StringUtils.StringUtils.removeSpaceAndMakeUppercase(stringWithSpace).should.equal(stringWithoutSpaceUppercase);
		});
	});

	describe('Postcode String', () => {
		it('makes a clean uppercase postcode', () => {
			StringUtils.StringUtils.postcodeString(stringPostcode).should.equal(stringCleanPostcode);
		});
	});

	describe('Contains uppercase', () => {
		it('returns true if string contains uppercase', () => {
			StringUtils.StringUtils.containsUppercase(stringWithSpace).should.equal(true);
		});

		it('returns false if string does not contain uppercase', () => {
			StringUtils.StringUtils.containsUppercase(stringWithoutSpaceLowercase).should.equal(false);
		});
	});

	describe('Contains lowercase', () => {
		it('returns true if string contains lowercase', () => {
			StringUtils.StringUtils.containsLowercase(stringWithoutSpaceLowercase).should.equal(true);
		});

		it('returns false if string does not contain lowercase', () => {
			StringUtils.StringUtils.containsLowercase(stringWithoutSpaceUppercase).should.equal(false)
		});
	});

	describe('Contains numeric', () => {
		it('returns true if string contains numeric', () => {
			StringUtils.StringUtils.containsNumeric(stringPostcode).should.equal(true)
		});

		it('returns false if string does not contain numeric', () => {
			StringUtils.StringUtils.containsNumeric(stringWithoutSpaceUppercase).should.equal(false)
		});
	});

	describe('Contains special', () => {
		it('returns true if string contains special', () => {
			StringUtils.StringUtils.containsSpecial(illegalFilename).should.equal(true)
		});

		it('returns false if string does not contain special', () => {
			StringUtils.StringUtils.containsSpecial(stringWithoutSpaceUppercase).should.equal(false)
		});
	});


	describe('Minimum Length', () => {
		it('returns true if string longer than a set value of 16', () => {
			StringUtils.StringUtils.stringLengthMoreThan(stringWithSpace, 16).should.equal(true)
		});

		it('returns false if string is not longer than 20', () => {
			StringUtils.StringUtils.stringLengthMoreThan(stringWithSpace, 20).should.equal(false)
		});
	});


	describe('Contains Numeric', () => {
		it('returns true if string contains numeric', () => {
			StringUtils.StringUtils.containsNumeric(stringPostcode).should.equal(true)
		});

		it('returns false if does not contain numeric', () => {
			StringUtils.StringUtils.containsNumeric(stringWithSpace).should.equal(false)
		});
	});

	describe('Contains Numeric only', () => {
		it('returns the value with only numeric left behind', () => {
			StringUtils.StringUtils.allowNumbersOnly(stringPostcode).should.equal(stringNumeric)
		});

	});

	describe('Contains Letters only', () => {
		it('returns the value with only letters left behind lower', () => {
			StringUtils.StringUtils.allowLettersOnly(stringPostcode).should.equal(stringLettersLower)
		});

		it('returns the value with only letters left behind upper', () => {
			StringUtils.StringUtils.allowLettersOnly(stringCleanPostcode).should.equal(stringLettersUpper)
		});
	});

	describe('Contains a valid email', () => {
		it('returns true if the email is valid', () => {
			StringUtils.StringUtils.isValidEmail(stringValidEmailCoUk).should.equal(true)
		});

		it('returns true if the email is valid', () => {
			StringUtils.StringUtils.isValidEmail(stringValidEmailCom).should.equal(true)
		});

		it('returns false if the email is invalid', () => {
			StringUtils.StringUtils.isValidEmail(stringNumeric).should.equal(false)
		});

		it('returns false if the email is invalid', () => {
			StringUtils.StringUtils.isValidEmail(stringPostcode).should.equal(false)
		});
	});

	describe('Contains a valid password', () => {
		it('returns true if the password is valid', () => {
			StringUtils.StringUtils.checkPasswordValidation(stringValidPassword).should.equal(true)
		});

		it('returns false if the password is invalid', () => {
			StringUtils.StringUtils.checkPasswordValidation(stringNumeric).should.equal(false)
		});

		it('returns false if the password is invalid', () => {
			StringUtils.StringUtils.checkPasswordValidation(stringPostcode).should.equal(false)
		});
	});

	describe('Contains a valid email', () => {
		it('returns true if the email is valid', () => {
			StringUtils.StringUtils.emailFormat(stringValidEmailCoUk).should.equal(true)
		});

		it('returns true if the email is valid', () => {
			StringUtils.StringUtils.emailFormat(stringValidEmailCom).should.equal(true)
		});

		it('returns false if the email is invalid', () => {
			StringUtils.StringUtils.emailFormat(stringNumeric).should.equal(false)
		});

		it('returns false if the email is invalid', () => {
			StringUtils.StringUtils.emailFormat(stringPostcode).should.equal(false)
		});
	});

	describe('Contains a valid password', () => {
		it('returns true if the password is valid', () => {
			StringUtils.StringUtils.isValidPassword(stringValidPassword).should.equal(true)
		});

		it('returns false if the password is invalid', () => {
			StringUtils.StringUtils.isValidPassword(stringNumeric).should.equal(false)
		});

		it('returns false if the password is invalid', () => {
			StringUtils.StringUtils.isValidPassword(stringPostcode).should.equal(false)
		});
	});
});