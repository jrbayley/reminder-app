/**
 * @interface
 * This interface is used to extend the cordova plugins with app functions.
 */

interface CordovaPlugins {
		barcodeScanner: BarcodeScanner;
		NativeAudio: NativeAudio;
		Fingerprint: Fingerprint;
}

/**
 * @interface
 * Barcode scanner methods.
 */

interface BarcodeScanner {
	scan(callback: (result: any) => void, error: (error: Error) => void, options: BarCodeScannerOptions) : string;
}
/**
 * @interface
 * Barcode scanner configuration options.
 */

interface BarCodeScannerOptions {
	preferFrontCamera: boolean; // iOS and Android
	showFlipCameraButton: boolean; // iOS and Android
	showTorchButton: boolean; // iOS and Android
	torchOn: boolean; // Android, launch with the torch switched on (if available)
	saveHistory: boolean; // Android, save scan history (default false)
	prompt: string; // Android
	resultDisplayDuration: number;// Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
	formats: string; // default: all but PDF_417 and RSS_EXPANDED
	disableAnimations: boolean; // iOS
	disableSuccessBeep: boolean; // iOS and Android
}

/**
 * @interface
 * This interface is used to detail the native audio methods.
 */

interface NativeAudio{
	preloadSimple(audio: any, fileName: string, callback: (message: string) => void, error: (error: string) => void) : void;
	preloadComplex(audio: any, fileName: string, volume: number, voices: number, delay: number, callback: (message: string) => void, error: (error: string) => void) : void;
	play(audio: any) : void;
	loop(audio: any) : void;
	stop(audio: any) : void;
	unload(audio: any) : void;
}

declare var Fingerprint: Fingerprint;

interface fingerprintOptions{
	title?: string; //Title in authentication dialogue. Default: "<APP_NAME> Biometric Sign On"
	subtitle?: string; // Subtitle in authentication dialogue. Default: null
	description?: string; // Description in authentication dialogue. Defaults:
							// iOS: "Authenticate" (iOS' evaluatePolicy() requires this field)
							// Android: null;
	fallbackButtonTitle?: string; // Title of fallback button. Defaults:
									// When disableBackup is true
									// "Cancel"
									// When disableBackup is false
									// iOS: "Use PIN"
									// Android: "Use Backup" (Because backup could be anything pin/pattern/password ..haven't figured out a reliable way to determine lock type yet source)
	disableBackup?: boolean; 	// If true remove backup option on authentication dialogue. Default: false. This is useful if you want to implement your own fallback.
	cancelButtonTitle?: string; 	// For cancel button on Android
}

interface Fingerprint {
	isAvailable(callback: () => void, error: (error: string) => void): void;
	show(options: fingerprintOptions, callback: () => void, error: (error: string) => void): void;
}


