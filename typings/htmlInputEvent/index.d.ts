
/**
 * @interface
 * This interface is used to enable correct typings when using file upload fields 
 * by adding the required additional properties.
 */

interface HTMLInputEvent extends Event {
    target: HTMLInputElement & EventTarget;
}